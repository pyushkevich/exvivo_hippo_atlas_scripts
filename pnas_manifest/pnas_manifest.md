# Penn Hippocampus Atlas 2018 Data Manifest
This dataset contains imaging data from paper "Characterizing the human hippocampus in aging and
Alzheimer�s disease using a computational atlas derived from ex vivo MRI and histology" by Adler, Wisse, et al., to appear in PNAS 2018.

## Top-Level Data Organization
* [specimens.tar](http://pennhippoatlas.projects.nitrc.org/organized/specimens.tar) archive: images for individual specimens (31G)
* [templates.tar](http://pennhippoatlas.projects.nitrc.org/organized/templates.tar) archive: average images and templates (32G)
* [histology.tar](http://pennhippoatlas.projects.nitrc.org/organized/histology.tar) archive: histology-based segmentations and reconstruction (62G)

Contents of each of these archives are detailed below

## Contents of the `specimen/XXX` Directories
Each specimen directory contains images corresponding to a single specimen. It is organized into top-level directories.

* `input/mri`: raw and preprocessed images for the specimen
    * `native`: MRI scans in native image space (i.e., as reconstructed from the MRI scanner)
        * `XXX_mri_native_raw.nii`: raw MRI image, as reconstructed from the scanner
        * `XXX_mri_native_n4clip.nii.gz`: MRI image processed with N4 and with intensity clipped to [0.1,99.9] percentile range (Sec. S1.1)
    * `manseg`: Image space in which semi-manual segmentation of the SRLM and HF were performed (Sec S1.2). For some specimens, this differs from the native image space by a crop, or axis flip. 
        * `XXX_manseg_dbseg.nii.gz`: Segmentation of the SRLM
        * `XXX_manseg_hfseg.nii.gz`: Segmentatino of the HF
        * `XXX_manseg_img.nii.gz`: MRI in the space of the segmentation
        * `XXX_manseg_to_native_affine.mat`: Affine transform between image space in which manual segmentation was performed and native image space.
    * `distcorr`: MRI scans corrected for scanner gradient miscalibration (Sec S1.1). These scans have been resliced using an affine transform that was derived by analyzing a scan of a 3D printed phantom. 
        * `XXX_distcorr_YYY.nii.gz`: corrected versions of the image, SRLM and HF segmentation (for `YYY` in `{dbseg,hfseg,img}`) 
        * `XXX_native_to_distcorr_affine.mat`: affine transform that maps from native space to the distortion-corrected space. 
* `template_build`: imaging data for the specimen generated during different stages of template building, as described in Sec. S1.3
    * These directories contain five sub-directories:
        * `mst`: Specimen mapped to the root of the minimum spanning tree, based on registration of SRLM and HF segmentations (Sec. S1.3.2, MST in Fig. S10)
        * `gshoot`: Specimen mapped to the template created using geodesic shooting, based on registratino of SRLM and HF segmentations (Sec. S1.3.3, MST+GS in Fig. S10)
        * `ncc`: Specimen mapped to the template created using group-wise intensity registration, with initialization provided by geodesic shooting (Sec. S1.3.4., MST+GS+INT in Fig. S10)
        * `foil_affine_only`: Specimen aligned to other specimens using affine registration only (Affine Only in Fig. S10). T
        * `foil_affine_ncc`: Specimen mapped to the template created using group-wise intensity registration initialized by affine registration (Affine+NCC in Fig. S10)
    * Each of these directories contains the following:
        * `XXX_distcorr_to_TTT_warp.nii.gz`: warp between the distortion corrected space (see above) and the template space for the given kind of template 
        * `reslice_XXX_to_TTT_YYY.nii.gz`: warped MRI and segmentation (for `YYY` in `{img,db,hf}`) 
* `jacobian`: Jacobian of the transformations between the "final" template (MST+GS+INT) and the subject's space (distortion corrected, i.e, `input/mri/distcorr`)
    * `XXX_jacobian_template_to_distcorr_space.nii.gz`: Jacobian image
* `histo_to_template`: mapping of histology to template, for subjects that have histology. Used to construct histology-based segmentation of the template (Sec. S1.4.5)
    * `XXX_histoseg_to_template.nii.gz`: Histological segmentation of the subject mapped into the space of the atlas (i.e., MST+GS+INT template). See Figure S12 (Subject 1 - Subject 9). 
    * `XXX_histoseg_mask_to_template.nii.gz`: Just a binary mask of the above, i.e., 1 if the voxel has a histological label, 0 otherwise. 
* `template_histo_seg`: template-based segmentation of subfields in subject's space, based on the averaging of labels of 9 subjects who have histology. Sample such segmentation is shown in Fig. S13. These data are used to generate the statistics in Table 2.
    * `XXX_histoseg_distcorr_space.nii.gz`: Segmentation of the subfields in the space of the subject's distortion corrected image (i.e., `specimen/XXX/input/mri/distcorr`)
    * `XXX_histoseg_ap_distcorr_space.nii.gz`: Same as above, but with anterior and posterior structures assigned different labels, based on the uncal apex.
* `axis_align`: MRI image and segmentations aligned with the template via only rigid or only similarity transformation. These images are used to compute hippocampus extent in Table 3 (and extents of subfields, not reported), and to define the hippocampal tail
    * These directories contain subdirectories
        * `rigid`: Images are aligned to the template using rigid transformation (rotation and translation)
        * `similarity`: Images are aligned using rigid+uniform scaling
    * Inside each directory:
        * `XXX_axisalign_img.nii.gz`: Aligned MRI scan
        * `XXX_axisalign_histoseg.nii.gz`: Histological segmentation  
        * `XXX_uncalapex.nii.gz`: Landmark designating the uncal apex
        * `XXX_tail_landmarks.nii.gz`: Landmarks used to define the hippocampal tail

## Contents of the `templates` directory
This directory contains templates and template-space data

* `template_build`: Stores templates constructed during different stages of template-building (Fig. S9)
    * This directory contains five subdirectories
        * `mst`: Specimen mapped to the root of the minimum spanning tree, based on registration of SRLM and HF segmentations (Sec. S1.3.2, MST in Fig. S10)
        * `gshoot`: Specimen mapped to the template created using geodesic shooting, based on registratino of SRLM and HF segmentations (Sec. S1.3.3, MST+GS in Fig. S10)
        * `ncc`: Specimen mapped to the template created using group-wise intensity registration, with initialization provided by geodesic shooting (Sec. S1.3.4., MST+GS+INT in Fig. S10)
        * `foil_affine_only`: Specimen aligned to other specimens using affine registration only (Affine Only in Fig. S10). T
        * `foil_affine_ncc`: Specimen mapped to the template created using group-wise intensity registration initialized by affine registration (Affine+NCC in Fig. S10)
    * Each subdirectory contains
        * `template_ZZZ_img.nii.gz`: Average intensity image (shown in Fig. S9)  
        * `template_ZZZ_hf.nii.gz`: Average hippocampus segmentation
        * `template_ZZZ_db.nii.gz`: Average dark band (SRLM) segmentation
* `final_template`: Data associated with the final template (corresponds to `ncc` above)
    * `template_img.nii.gz`: Average intensity image (identical to `template_build/ncc`)
    * `template_hf.nii.gz`: Average hippocampus segmentation (identical to `template_build/ncc`)
    * `template_db.nii.gz`: Average dark band (SRLM) segmentation (identical to `template_build/ncc`)
    * `template_histo_seg.nii.gz`: Histology-based segmentation of the template (Fig. 1, Fig. S12)
    * `template_histo_seg_ap.nii.gz`: Histology-based segmentation of the template, with partitioning into anterior and posterior subfield labels
    * `template_histo_entropy_map.nii.gz`: Entropy map (Fig. S12)
* `thickness`: Data used to generate thickness analysis. Thickness maps are computed in Sec. S1.4.6 and presented in Fig. 2. The pattern `YYY` below stands for *Dark Band, i.e., SRLM (`db`)*, *Dentate Gyrus (`dg`)*, and *Gray Matter, i.e., , CA1,CA2,CA3,subiculum (`gm`)*.  
    * `template_YYY_surface_smooth.vtk`: smoothed surface corresponding to label `YYY` in template space. These are mapped to subject space for the thickness computation
    * `subj/XXX`: Subject-specific thickness maps (in distcorr space)
        * `XXX_YYY_skeleton.vtk`: Skeleton mesh for label set YYY
        * `XXX_YYY_thickness.vtk`: Thickness for label set YYY
    * `lmer`: Mixed effects linear modeling data
        * `lmer_YYY.vtk`: These meshes contain arrays of t-statistics, chi-square statistics and FDR-adjusted p-values for the thickness analysis presented in Fig. 2
* `shape`: Principal Components and Support Vector Machine analysis results (S1.4.7, Figure 3). In all files below, ZZZ refers to the direction in shape space along which images/shapes are generated and visualized. This direction is either one of the five PCA modes (`mode_K`) or the Support Vector Machine margin direction (`svm`).
    * `momenta/ZZZ_vector_DDD.vtk`: Contains `.vtk` files with control points and initial momenta for geodesic shooting. Initial momentum corresponding to the given direction in shape space. 
    * `image_movies`: Contains 3D+time movies for the PCA modes and SVM direction (top of Fig. 3)
        * `movie_ZZZ/movie_ZZZ_img_FF.nii.gz`: Deformed template for frame `FF` along movie `ZZZ`. Timepoint 40 is the mean shape.
        * `movie_ZZZ/movie_ZZZ_seg_FF.nii.gz`: Histology-based segmentation for the same timepoint
    * `mesh_movies`: Contains 3D+time moves for surface meshes. Used to generate the bottom part of Fig. 3. 
        * `movie_ZZZ/movie_ZZZ_labelK_FF.vtk`: Deformed mesh for label `K` in frame `FF` in movie `ZZZ`.
        * `surfK.vtk`: Surface for label 'K' in the mean model.
    * `warp`: Warps corresponding to movie frames above. Each movie frame can be generated by applying the correponding warp to the template.
        * `movie_ZZZ/movie_ZZZ_FF.nii.gz`: warp for direction `ZZZ`, frame `FF`. At timepoint 40, the warp is identity.

## Contents of the `histology` directory
This directory contains outputs of the histology reconstruction pipeline and histologically-informed MRI annotation described in Sec. S1.4. 

* `blocks`: Data for the individual histology blocks
    * `XXX_block_Y`: Block `Y` in specimen `XXX`. Ordering of `Y` is from anterior to posterior.
        * `mri`: MRI for this block
            * `mri_block.nii.gz`: MRI of the block, in native space 
            * `mri_whole.nii.gz`: MRI of the whole specimen, in native space
            * `mri_block_to_whole_rigid.nii.gz`: Block MRI rigidly aligned to the whole MRI (referred to as B2W image space below).
            * `mri_block_to_whole_rigid_mask.nii.gz`: Binary mask of the block in the `B2W` image space (used for registration)
            * `mri_block_to_whole_rigid_mask_hf.nii.gz`: Binary mask of the hippocampus formation pertaining to the block in the `B2W` image space.
            * `seg_db.nii.gz`,`seg_hf.nii.gz`: Segmentations of the SRLM (dark band) and hippocampal formation in whole MRI space.
            * `whole_to_b2w_affine.mat`: Affine transformation between the whole MRI and the `B2W` image.
            * `whole_to_b2w_warp.nii.gz`: Warp between the whole MRI and the `B2W` image.
            * `reslice_whole_to_b2w_affine.nii.gz`,`reslice_whole_to_b2w_warp.nii.gz`: Whole specimen MRI resliced into `B2W` space by transformations above.
            * `ncc_stat.txt`: Normalized cross-correlation statistics for the `whole-to-B2W` registration using rigid, affine and deformable transformation models 
        * `hist/Ynn`: Data for individual histology slices in the block. `Ynn` is a code consisting of a letter (for the block, `Y` above) and number of the slice in that block.
            * `slide`: Images for the given histology slide
                * `slide_x16.png`: Slide at 16-times magnification
                * `slide_x64.png`: Slide at 64-times magnification
                * `slide_neg.nii.gz`: Color negative of the slide at 64x, in NIFTI format. 
                * `seg_transp.svg`: Segmentation of subfields in the slide. This segmentation is not used in the paper, except that it was occasionally inspected during segmentation of the block MRI.
                * `svg.png`: Segmentation above, in PNG format matched to 64x image.
                * `slide_seg.nii.gz`: Same as above, in NIFTI format
                * `reslice_mri_to_slide_hzee.nii.gz`: Block MRI resliced to the histology slide using manual registration in HistoloZee
                * `slide_mask.nii.gz`: Binary mask of the slide, based on the above MRI
            * `ann`: Hand-drawn annotations of the histology slides, used to guide block MRI segmentation
                * `ann_neg.nii.gz`: Color negative of the annotation image in its own space
                * `slide_to_ann_affine_3d.mat`,`slide_to_ann_affine.mat`: Linear transformation between the slide in its native space and the annotation image. This recovers the manual rotation of the slides performed by the person doing the annotation in Photoshop.
                * `reslice_slide_to_ann_affine.nii.gz`: Negative of the slide after rotating it into the space of the annotation image by transformation above
                * `ann_markup.nii.gz`: Difference image between `ann_neg.nii.gz` and `reslice_slide_to_ann_affine.nii.gz`. This recovers the annotations themselves.
                * `reslice_mri_to_ann_hzee.nii.gz`: MRI of the block mapped into the annotation image space.
            * `canon`: Slide images in "canonical" space. The canonical space is based on the HistoloZee reconstruction of the slides. In this space, images are in a fixed resolution and pixel size, and each image has a 3D origin based on HistoloZee ordering.
                * `canonical_space.mha`: Blank image that defined the canonical space
                * `seg_db_to_canon.mha`: MRI SRLM segmentation in canonical space
                * `seg_hf_to_canon.mha`: MRI Hippocampus segmentation in canonical space
                * `wholemri_to_canon.mha`: Whole MRI resliced into canonical space
                * `ann_to_canon.mha`: Annotation color negative image in canonical space
                * `slide_seg_to_canon.mha`: Slide segmentation (not used) in canonical space
                * `slide_to_canon.mha`: Slide color negative 
                * `mri_block_to_canon.mha`: Block MRI in canonical space
                * `mrisim_canon.mha`: Simulated MRI intensity derived from the slide RGB intensity as described Fig. S2
                * `histology_mask_canon.mha`: Mask of the histology image
            * `gw`: Intermediate images and transforms generated for this slide during groupwise registration of the slides to the block. 
                * `affine_k.mha`: affine transform from affine only registration of this slide to adjacent slides and corresponding MRI slice
                * `mrisim_k.mha`: simulated MRI intensity (`mrisim_canon.mha`) resliced after applying above transform
                * `mask_k.mha`: corresponding resliced registration mask.
                * `warp_p.mha`: deformable transform from deformable registration of this slide to adjacent slides and corresponding MRI slice
                * `mrisim_p.mha`: simulated MRI intensity (`mrisim_canon.mha`) resliced after applying above transform
                * `mask_p.mha`: corresponding resliced registration mask.
            * `mat`: Matrices extracted from HistoloZee projects
        * `canon`: Histology data reconstructed into 3D canonical space defined by the HistoloZee project.
            * `canonical_space.nii.gz`: Blank image defining the 3D canonical space
            * `block_to_canon.mat`: 3D linear transformation between the block MRI and the canonical space
            * `wholemri_to_canon.nii.gz`: Whole MRI resliced into 3D canonical space
            * `seg_hf_to_canon.nii.gz`: Whole MRI hippocampus segmentation resliced into 3D canonical space
            * `histology_to_canon.nii.gz`: Histology slides, reconstructed in 3D and aligned to the block MRI, resliced into the canonical space
            * `seg_db_to_canon.nii.gz`: Whole MRI SRLM segmentation resliced into 3D canonical space
            * `annotation_to_canon.nii.gz`: Annotation negative color image, in the 3D canonical space
            * `markup_to_canon.nii.gz`: 3D image of the slide annotations (just the marks) in 3D canonical space, after 3D reconstruction
            * `histo_seg_to_canon.nii.gz`: Histology segmentation, as an RGB image (not used in the paper)
            * `histo_labels_to_canon.nii.gz`: Histology segmentation, as a multi-label image (not used in the paper)
            * `danseg_to_canon.nii.gz`: Combined segmentation of the HF and SRLM in the 3D canonical space
        * `histo_seg_block`: Histology-guided segmentation of the block MRI
            * `XXX-blockY_wholemri_canon.nii.gz`: Whole specimen MRI resliced into canonical space above
            * `XXX-blockY_lwseg_canon.nii.gz`: Manual segmentation of selected slices in the MRI block as informed by the histology annotations.
* `whole`: Histology-guided subfield segmentations mapped into the whole MRI space for specimens and manually edited in this space
    * `wholemri_XXX`: images for specimen XXX
        * `XXX_mri_whole.nii.gz`: Whole specimen MRI for this specimen in native space
        * `XXX_combine_slices_bg_to_wholemri.nii.gz`: Binary masks of the slices in the block MRI that were manually segmented, resliced via warping into the whole MRI space
        * `XXX_lwseg_interp_masked_wholemri.nii.gz`: Interpolated subfield segmentations, before manual touch-up
        * `XXX_lwseg_interp_masked_wholemri_touchup.nii.gz`: Interpolated subfield segmentations, after manual touch-up




    

