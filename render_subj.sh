#!/bin/bash
SUBJ=${1?}
ROOT=/data/picsl/pauly/dan2015/atlas2016

for i in 1 2 3 4 5 6 8 9; 
do 
  c3d $ROOT/histolabels/axisalign/${SUBJ}/rigid/${SUBJ}_axisalign_histoseg.nii.gz \
    -thresh $i $i 1 0 -o /tmp/bin.nii.gz; 
  vtklevelset /tmp/bin.nii.gz /tmp/label${i}.vtk 0.5
done

$ROOT/scripts/vtkrender/mesh_to_png.sh \
  /tmp/label%d.vtk vtkrender/superior.pvcc 1360 720 /tmp/pic_${SUBJ}.png 1-6,8-9
