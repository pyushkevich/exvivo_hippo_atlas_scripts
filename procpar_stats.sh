ROOT=/data/picsl/pauly/dan2015/atlas2016

function scan_proc()
{
  procpar=${1?}
  key=${2?}
 
  cat $procpar | grep -A 1 "^${key} " | tail -n 1 | awk '{print $2}' 
}



function procpar_stats()
{
  id=${1?}
  nii=$(readlink -f $ROOT/preproc/$id/${id}_raw.nii.gz)

  PROCPAR1=${nii}.procpar
  unset PROCPAR2

  if [[ ! -f $PROCPAR1 ]]; then
    PROCPAR1=${nii}.procpar.1
    PROCPAR2=${nii}.procpar.2
  fi

  COIL=$(scan_proc $PROCPAR1 rfcoil | sed -e "s/\"//g") 
  TR=$(scan_proc $PROCPAR1 tr)
  TE=$(scan_proc $PROCPAR1 te)
  NT1=$(scan_proc $PROCPAR1 nt)
  NV1=$(scan_proc $PROCPAR1 nv)

  if [[ $PROCPAR2 ]]; then
    NV2=$(scan_proc $PROCPAR2 nv)
    NT2=$(scan_proc $PROCPAR2 nt)
    STITCH=Y
  else
    NV2=0
    NT2=0
    STITCH=N
  fi

  echo $id $COIL | awk \
   -v tr=$TR -v te=$TE -v nt1=$NT1 -v nt2=$NT2 -v nv1=$NV1 -v nv2=$NV2 \
   -v st=$STITCH \
   '{printf "%s,%s,%4.0f,%4.0f,%4.1f,%s\n",$1,$2,tr*1000,te*1000,(nt1*nv1+nt2*nv2)*tr/3600,st}'
}

for id in $(cat $ROOT/scripts/subj.txt); do
  procpar_stats $id
done
