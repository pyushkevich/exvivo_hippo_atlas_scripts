#!/bin/bash
#$ -S /bin/bash
set -x -e 

# The directory where we are doing this work
ROOT=/data/picsl/pauly/dan2015/atlas2016

# Read the common include file
source $ROOT/scripts/common.sh

# Whether we want to skip steps for which output already exists
unset REDO_EXISTING

# Template info
TEMPLATE_CMREP=$ROOT/atlas09_package/extras/pmatlas_cmrep.med.vtk
TEMPLATE_MASK=$ROOT/atlas09_package/pmatlas_mask.nii.gz

# Helper function - generate cm-rep warp using two cm-reps
function make_cmrep_warp()
{
  TARGET_CMREP=$1
  SOURCE_CMREP=$2
  TARGET_IMAGE=$3
  OUTPUT_WARP=$4
  FLIP=$5

  rm -rf $TMPDIR/deleteme?vec.nii.gz
  
  # Generate a cm-rep warp between the cm-rep template space 
  # and the target image space
  $CMREP_HOME/bin/cmrep_genwarp -x 5.0 0.1 $FLIP \
    $TARGET_CMREP $SOURCE_CMREP $TARGET_IMAGE \
    $TMPDIR/deleteme

  # We need to be careful to replace the 0,0,0 in the warp field by 
  # a very large number to avoid weird stuff in the segmentation. 
  $C3D_HOME/c3d $TMPDIR/deleteme?vec.nii.gz \
    -popas Z -scale -1 -popas Y -scale -1 -as X \
    -push Y -push Z \
    -foreach -dup -times -endfor -mean -thresh 0 0 1e10 0 -popas Q \
    -clear -push X -push Y -push Z \
    -foreach -push Q -add -endfor \
    -omc $OUTPUT_WARP
}

# Perform same processing on all datasets
function process_subj()
{
  # Create the directory for this subject
  ID=${1?}
  WDIR=$ROOT/work/${ID}
  mkdir -p $WDIR

  # Determine if this subject is flipped relative to the template
  FLIP=$(echo $ID | sed -e "s/.*-R$/-f/g" -e "s/.*-L$//g")

  # The raw MRI of this subject
  MRI_NATIVE=$ROOT/input/${ID}/${ID}_img.nii.gz
  HFSEG_NATIVE=$ROOT/input/${ID}/${ID}_hfseg.nii.gz
  DBSEG_NATIVE=$ROOT/input/${ID}/${ID}_dbseg.nii.gz

  # Create a filled hippo mask
  HFFILL_NATIVE=$WDIR/${ID}_hffill.nii.gz

  # Fit the cm-rep to the hippocampus mask. This is a repetition
  # of work done previously, but we repeat this step for completeness.
  # This way others can reuse our atlas building code for completely new
  # datasets, which is very cool.
  mkdir -p $WDIR/cmrep $WDIR/cmrspc

  # The expected output
  CMREP_MESH=$WDIR/cmrep/mesh/def3.med.vtk
  CMREP_FWARP=$WDIR/cmrep/template_cmrepwarp.nii.gz
  CMREP_IWARP=$WDIR/cmrep/template_cmrepinvwarp.nii.gz

  MRI_CMRSPC=$WDIR/cmrspc/${ID}_img_cmrspc.nii.gz
  HFSEG_CMRSPC=$WDIR/cmrspc/${ID}_hfseg_cmrspc.nii.gz
  HFFILL_CMRSPC=$WDIR/cmrspc/${ID}_hffill_cmrspc.nii.gz
  DBSEG_CMRSPC=$WDIR/cmrspc/${ID}_dbseg_cmrspc.nii.gz

  # Create a filled hippo mask
  if [[ $REDO_EXISTING || ! -f $HFFILL_NATIVE ]]; then
    c3d $HFSEG_NATIVE $DBSEG_NATIVE -add -thresh 1 inf 1 0 -o $HFFILL_NATIVE 
  fi

  # Perform cm-rep fitting between the ex vivo CM-REP template and the
  # current mask of the subject
  if [[ $REDO_EXISTING || ! -f $CMREP_MESH || ! -f $CMREP_FWARP \
        || ! -f $CMREP_IWARP || ! -f $MRI_CMRSPC ]]; then

    # Fit cm-rep
    $CMREP_HOME/bin/cmrep_fit \
      $ROOT/scripts/cmrep_param.txt \
      $ROOT/atlas09_package/pmatlas_cmrep.cmrep \
      $HFSEG_NATIVE \
      $WDIR/cmrep

    # Check if the cm-rep was generated
    if [[ ! -f $CMREP_MESH ]]; then
      echo "Failed to generate cm-rep mesh for $ID"
      exit -1
    fi

    # Generate a cm-rep warp between the cm-rep template space 
    # and the target image space
    make_cmrep_warp \
      $CMREP_MESH $TEMPLATE_CMREP $MRI_NATIVE $CMREP_FWARP $FLIP

    # Generate an inverse of that warp, from the image to the
    # template space
    make_cmrep_warp \
      $TEMPLATE_CMREP $CMREP_MESH $TEMPLATE_MASK $CMREP_IWARP $FLIP

    # Apply cm-rep warp to the segmentations
    $GREEDY_HOME/greedy -d 3 \
      -rf $TEMPLATE_MASK \
      -rm $MRI_NATIVE $MRI_CMRSPC \
      -ri LABEL 0.24vox \
      -rm $HFSEG_NATIVE $HFSEG_CMRSPC \
      -rm $HFFILL_NATIVE $HFFILL_CMRSPC \
      -rm $DBSEG_NATIVE $DBSEG_CMRSPC \
      -r $CMREP_IWARP

  fi

  # Run Dan's geometric parameterization code
  mkdir -p $WDIR/danparam

  # Define the outputs of Dan's code
  POT_MAP=$WDIR/danparam/${ID}_pot-extend.nii.gz
  THETA_MAP=$WDIR/danparam/${ID}_theta_extend_hfill.nii.gz
  COSPHI_MAP=$WDIR/danparam/${ID}_cos_phi_extend_hfill.nii.gz
  SINPHI_MAP=$WDIR/danparam/${ID}_sin_phi_extend_hfill.nii.gz

  # Check if we need to run this code
  if [[ $REDO_EXISTING || ! -f $POT_MAP || ! -f $THETA_MAP \
    || ! -f $COSPHI_MAP || ! -f $SINPHI_MAP ]]; then

    mkdir -p $WDIR/danparam/scratch
    $ROOT/scripts/danparam.sh \
      $HFFILL_CMRSPC $DBSEG_CMRSPC $WDIR/danparam/${ID}_%s $WDIR/danparam/scratch

  fi
}

function main()
{
  mkdir -p $ROOT/work/dump
  for id in $(cat $ROOT/scripts/subj.txt); do

    qsubp4 -N preprc_$id -cwd -V -j y -o $ROOT/work/dump \
      -l h_vmem=15.6G,s_vmem=15.2G \
      $0 process_subj $id

  done

}


# Run the selected function in this script
if [[ $1 ]]; then

  command=$1
  shift
	$command $@

else

	main
	
fi
