# README #

These are the scripts used to perform the image analysis in (Adler, Wisse, et al., 2018 PNAS), "Characterizing the human hippocampus in aging and
Alzheimer’s disease using a computational atlas derived from ex vivo MRI and histology".   

### Important Files in this Directory ###

* `preproc.sh`: Script used to organize raw MRI data, manual SRLM and HF segmentations, and apply gradient distortion correction
* `build_mst.sh`: Script used to construct MST, MST+GS, MST+GS+NCC and other templates mentioned in the paper
* `reslice_histo_segs.sh`: Script used to map histology-derived labels into the MRI atlas and to perform volumetric, thickness and shape analysis
* `archive_pnas.sh`: Script used to organize the contents of the PNAS data repository
* `snaplabels.txt`: ITK-SNAP labelfile for hippocampal subfield segmentations
* `snaplabels_ap.txt`: Same, but with partitioning into anterior/posterior subfields
* `subj.txt`: IDS of subjects included in the analysis
* `supplement.pdf`: PDF of the PNAS paper supplement

### Subdirectories ###

* `stats`: R scripts for the statistical analysis (Table 2, etc)
* `manual`: Manual inputs (minor) in the pipeline generation process (e.g., uncal apex landmark)
* `histology`: Scripts used to organize and reconstruct histology data
* `vtkrender`: Scripts used to generate images from VTK scenes
