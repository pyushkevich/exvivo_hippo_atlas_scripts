#!/bin/bash
### This function calls Dan's code to compute 
### rho, theta and phi maps on a hippocampus segmentation
set -x -e

# Input hippocampus segmentation
HFSEG=${1?}
DBSEG=${2?}

# Output filename, printf format (result_%s.nii.gz)
OUTPUT=${3?}

# Work directory - good idea to set this to TMPDIR in qsub
WORKDIR=${4?}

# Whether we want to skip steps for which output already exists
unset REDO_EXISTING

# Define various outputs that the user will get
OUT_POT=$(printf $OUTPUT "pot-extend").nii.gz
OUT_MESH_DB=$(printf $OUTPUT "darkband").vtk
OUT_MESH_HW=$(printf $OUTPUT "halfway").vtk
OUT_MESH_HF=$(printf $OUTPUT "hippocampus").vtk

# WORKDIR must be set
echo "Working in ${WORKDIR?}"

# DANCODE_HOME is the directory of the executables of Poisson streamlining
DANCODE_HOME=/data/picsl/pauly/nibtend_renewal/indd_to_atlas/danparam/code

# CMREP path
CMREP_HOME=/data/picsl/pauly/bin/cmrep_2015_04/bin

# C3D path
C3D_HOME=/data/picsl/pauly/bin

# Prepend these directories to the path
PATH=$C3D_HOME:$CMREP_HOME:$DANCODE_HOME:$PATH

# TODO: remove these vars
FIXRUNDIR=potrun
EXTEND="-extend"

# Copy the segmentations into the TMPDIR directory
c3d $HFSEG -thresh 1 inf 1 0 -type uchar -o $WORKDIR/hf_fill.nii.gz
c3d $DBSEG -thresh 1 inf 1 0 -type uchar -o $WORKDIR/db_fill.nii.gz


# Go to the temp dir
pushd $WORKDIR

# ========================
# FROM 02_create_bnd
# ========================

if [[ $REDO_EXISTING || ! -f $OUT_POT ]]; then

  # get rid of DB that is not contained within an erosion (1x1x1) of the HF mask:
  c3d hf_fill.nii.gz -dilate 0 1x1x1vox -as hf \
      db_fill.nii.gz -as db \
      -push hf -push db \
      -scale -1 -add \
      -thresh -1 -1 -1 0 \
      -push db \
      -add \
      -o db_fill_trim.nii.gz

  # Fill everything outside of HF shell and include internal DB sheet:
  c3d hf_fill.nii.gz -dilate 0 1x1x1vox -o temp_hf_eroded.nii.gz
  c3d hf_fill.nii.gz temp_hf_eroded.nii.gz -add -o temp_hf_shell.nii.gz
  c3d temp_hf_shell.nii.gz db_fill_trim.nii.gz -add -clip 0 3 -o temp.nii.gz
  c3d temp.nii.gz -replace 0 2 1 2 2 0 3 1 -o bnd.nii.gz
  c3d temp.nii.gz -replace 2 0 1 2 3 1 -o bnd-extend.nii.gz
      

  # ========================
  # Added by Paul: use distance map to create initial potential
  # ========================

  # Create a shell around the outside of the image
  c3d bnd-extend.nii.gz -thresh -inf inf 1 0 -as R -pad 3x3x3 3x3x3 0 \
    -dilate 0 1x1x1 -insert R 1 -int 0 -reslice-identity -replace 0 1 1 0 \
    -o temp_outershell.nii.gz

  # Compute the potential between hippocampus and outside
  c3d \
    temp_outershell.nii.gz -sdt -clip 0 inf -as A \
    hf_fill.nii.gz -as HF -sdt -clip 0 inf -as B \
    -add -reciprocal -as D -clear \
    -push A -scale 2 -push B -scale 3 -add -push D -times \
    -push HF -thresh 0 0 1 0 -times \
    -o temp_interp23.nii.gz

  # Compute the potential between dark band and hippocampus
  c3d \
    hf_fill.nii.gz -as HF -sdt -scale -1 -clip 0 inf -as A \
    db_fill.nii.gz -sdt -clip 0 inf -as B \
    -add -reciprocal -as D -clear \
    -push A -scale 1 -push B -scale 2 -add -push D -times \
    -replace nan 0 -clip 1 2 \
    -push HF -times \
    -o temp_interp12.nii.gz

  # Add to get the combined potential
  c3d temp_interp12.nii.gz temp_interp23.nii.gz -add -o pot-extend-init.nii.gz
   
  ### rm temp*

  # ========================
  # FROM 03_compute_pot
  # ========================

  # Create a directory for running
  mkdir -p ${FIXRUNDIR}
  rm -rf ${FIXRUNDIR}/*

  # copy boundary values to MHD format:
  # -> boundary value of 255 is special and means that the voxel is GROUNDED at 0V
  c3d bnd${EXTEND}.nii.gz -type ushort -o ${FIXRUNDIR}/bnd${EXTEND}.mhd

  # create initial potential in MHD format:
  # -> boundary value of 255 is special and means that the voxel is GROUNDED at 0V
  # !!!!!!!!!!! Comment out this line if we wish to continue from where we left off... !!!!!!!!!!!!
  ### c3d bnd${EXTEND}.nii.gz -replace 255 0 -type double -o ${FIXRUNDIR}/pot${EXTEND}.mhd
  c3d pot-extend-init.nii.gz -type double -o ${FIXRUNDIR}/pot${EXTEND}.mhd

  # create headers for gradients:
  c3d bnd${EXTEND}.nii.gz -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/gradx${EXTEND}.mhd
  c3d bnd${EXTEND}.nii.gz -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/grady${EXTEND}.mhd
  c3d bnd${EXTEND}.nii.gz -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/gradz${EXTEND}.mhd

  # 	// 0: poisson
  # 	// 1: bnd.raw (boundary values in short format) -b
  # 	// 2: img.raw (input conductivity image in double format) -i
  # 	// 3: pot_in.raw INITIAL APPROX (initial potential in double format) -p
  # 	// 4: pot_out.raw OUTPUT (output potential in double format) -o
  # 	// 5: gradx.raw; 6: grady.raw; 7: gradz.raw (output gradient in double format) -g
  # 	// 8: NX; 9: NY; 10: NZ -n
  # 	// 11: SX; 12: SY; 13: SZ (relative voxel sizes) -s
  # 	// 14: maxits -niter
  # 	// 15: rjac -r
  # 	// 16: beta -f (if beta < 0, then compute beta = average gradient value in image)
  # 	// 17: useWeightedLaplacian (flag for whether to use weighted laplacian)
  # 	// 18: doRandomWalkerSeg (flag for whether to perform RandomWalker segmentation)
  # 	// 19: seg.raw (segmentation output)
  # 	// 20: imageBoundaryValue (if > 0, then this value is placed on the edges of the image to create an "extension" boundary

  # using weighted laplacian leads to some not nice results on our hippocampi,
  # since their image characteristics are SO different!
  # should theoretically work very well though...

  useWeightedLaplacian=0
  doRandomWalkerSeg=0

  # number of iterations of SOR:
  NUM_ITERATIONS=2500

  # if > 0, then this value is added to border pixels to extend potential outwards toward border
  imageBoundaryValue=0

  if [ "${EXTEND}" == "-extend" ]; then
    imageBoundaryValue=3
  ###  NUM_ITERATIONS=15000
  fi

  NX=`c3d bnd${EXTEND}.nii.gz -info-full | grep ' dim\[1\]' | sed 's/    dim\[1\] = //'`
  NY=`c3d bnd${EXTEND}.nii.gz -info-full | grep ' dim\[2\]' | sed 's/    dim\[2\] = //'`
  NZ=`c3d bnd${EXTEND}.nii.gz -info-full | grep ' dim\[3\]' | sed 's/    dim\[3\] = //'`

  SX=`c3d bnd${EXTEND}.nii.gz -info-full | grep ' pixdim\[1\]' | sed 's/    pixdim\[1\] = //'`
  SY=`c3d bnd${EXTEND}.nii.gz -info-full | grep ' pixdim\[2\]' | sed 's/    pixdim\[2\] = //'`
  SZ=`c3d bnd${EXTEND}.nii.gz -info-full | grep ' pixdim\[3\]' | sed 's/    pixdim\[3\] = //'`

  # Note: the second argument used to be ${FIXRUNDIR}/img.raw
  #       but we removed it since it is not needed here:

  ${DANCODE_HOME}/poisson \
        ${FIXRUNDIR}/bnd${EXTEND}.raw \
        junk.raw \
        ${FIXRUNDIR}/pot${EXTEND}.raw \
        ${FIXRUNDIR}/pot${EXTEND}.raw \
        ${FIXRUNDIR}/gradx${EXTEND}.raw ${FIXRUNDIR}/grady${EXTEND}.raw ${FIXRUNDIR}/gradz${EXTEND}.raw \
        ${NX} ${NY} ${NZ} \
        1.0 1.0 1.0 \
        ${NUM_ITERATIONS} \
        0.6 \
        -0.1 \
        ${useWeightedLaplacian} \
        ${doRandomWalkerSeg} \
        junk.raw \
        ${imageBoundaryValue}
        

  # convert potential map and its gradients to NII format images:
  c3d ${FIXRUNDIR}/pot${EXTEND}.mhd -type double -o ${OUT_POT}

  c3d ${FIXRUNDIR}/gradx${EXTEND}.mhd ${FIXRUNDIR}/grady${EXTEND}.mhd ${FIXRUNDIR}/gradz${EXTEND}.mhd \
      -type double -omc ${FIXRUNDIR}/grad${EXTEND}.nii.gz

  # delete the MHD images:
  ### rm ${FIXRUNDIR}/bnd${EXTEND}.raw ${FIXRUNDIR}/bnd${EXTEND}.mhd
  ### rm ${FIXRUNDIR}/pot${EXTEND}.raw ${FIXRUNDIR}/pot${EXTEND}.mhd
  ### rm ${FIXRUNDIR}/gradx${EXTEND}.raw ${FIXRUNDIR}/grady${EXTEND}.raw ${FIXRUNDIR}/gradz${EXTEND}.raw
  ### rm ${FIXRUNDIR}/gradx${EXTEND}.mhd ${FIXRUNDIR}/grady${EXTEND}.mhd ${FIXRUNDIR}/gradz${EXTEND}.mhd


  # ========================
  # FROM 04_generate_meshes
  # ========================
  ${CMREP_HOME}/vtklevelset ${OUT_POT} ${OUT_MESH_DB} 1.01
  ${CMREP_HOME}/vtklevelset ${OUT_POT} ${OUT_MESH_HW} 1.5
  ${CMREP_HOME}/vtklevelset ${OUT_POT} ${OUT_MESH_HF} 1.99

fi

# ========================
# FROM 01_create_bnd_ellipsoid
# ========================
c3d ${OUT_POT} -thresh 0 1.5 1 0 -type ushort -o halfway_mask.nii

# Run MATLAB scripts to generate minimum volume bounding ellipsoid of hippocampus mask.
# -Ellipsoid is represented by PSD matrix A.
# -Also get eigendecomposition of A (ellipsoid axes and radii): V and D.
# -Columns of V are eigenvectors and D is diagonal matrix of inverse-squared eigenvalues.
# -center is the center of the ellipsoid in VOXEL coordinates.
# -Note that [1 1 1] is subtracted from the center to map from [1,N] (in MATLAB) to [0,N-1].
# -Re-order the radii from LARGEST to SMALLEST and re-order the eigenvectors (axes)
#  to correspond with the radii.
# -Also make sure that all axes (eigenvectors) are oriented correctly (to approximately
#  co-incide with positive IJK axes).

EXPANSION_VOX="0 0 0"

matlab -nodisplay -singleCompThread -r "\
  addpath('${DANCODE_HOME}/matlab'); \
  mask = load_untouch_nii('halfway_mask.nii'); \
  num_expansion_voxels = [${EXPANSION_VOX}]; \
  [ellipsoidMask, A, V, D, center] = createEnclosingEllipsoid(mask.img, num_expansion_voxels); \
  mask.img = ellipsoidMask; \
  save_untouch_nii(mask, 'ellipsoid.nii'); \
  save('ellipsoid_A_matrix.txt', '-ascii', 'A'); \
  temp = center' - [1 1 1]; \
  save('ellipsoid_center.txt', '-ascii', 'temp'); \
  temp = 1 ./ sqrt(diag(D)); \
  temp = temp'; \
  [temp, I] = sort(temp, 2, 'descend'); \
  save('ellipsoid_radii.txt', '-ascii', 'temp'); \
  temp = [V(:,I(1)), V(:,I(2)), V(:,I(3))]'; \
  if (temp(1,1) < 0) \
    temp(1,:) = -temp(1,:); \
  end; \
  if (temp(2,2) < 0) \
    temp(2,:) = -temp(2,:); \
  end; \
  if (temp(3,3) < 0) \
    temp(3,:) = -temp(3,:); \
  end; \
  save('ellipsoid_axes_inrows.txt', '-ascii', 'temp'); \
  quit;"


# Fill everything outside of ellipsoid:
c3d \
  ellipsoid.nii -replace 0 2 1 0 \
  halfway_mask.nii -add \
  -clip 0 2 -o bnd_ellipsoid.nii.gz


# ========================
# FROM 02_compute_pot_ellipsoid.sh
# ========================

FIXRUNDIR=run-ellipsoid
mkdir -p ${FIXRUNDIR}

# copy boundary values to MHD format:
# -> boundary value of 255 is special and means that the voxel is GROUNDED at 0V
c3d bnd_ellipsoid.nii.gz -type ushort -o ${FIXRUNDIR}/bnd.mhd

# create initial potential in MHD format:
c3d \
  bnd_ellipsoid.nii.gz -as E -thresh 1 1 1 0 -sdt -clip 0 inf -as A \
  -push E -thresh 2 2 1 0 -sdt -clip 0 inf -as B \
  -add -reciprocal -as D -clear \
  -push A -scale 2 -push B -scale 1 -add -push D -times \
  -replace nan 0 -clip 1 2 \
  -type double -o ${FIXRUNDIR}/pot-init.mhd

cat ${FIXRUNDIR}/pot-init.mhd | sed -e "s/pot-init/pot/g" > ${FIXRUNDIR}/pot.mhd

# create headers for gradients:
c3d bnd_ellipsoid.nii.gz -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/gradx.mhd
c3d bnd_ellipsoid.nii.gz -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/grady.mhd
c3d bnd_ellipsoid.nii.gz -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/gradz.mhd

# 	// 0: poisson
# 	// 1: bnd.raw (boundary values in short format) -b
# 	// 2: img.raw (input conductivity image in double format) -i
# 	// 3: pot_in.raw INITIAL APPROX (initial potential in double format) -p
# 	// 4: pot_out.raw OUTPUT (output potential in double format) -o
# 	// 5: gradx.raw; 6: grady.raw; 7: gradz.raw (output gradient in double format) -g
# 	// 8: NX; 9: NY; 10: NZ -n
# 	// 11: SX; 12: SY; 13: SZ -s
# 	// 14: maxits -niter
# 	// 15: rjac -r
# 	// 16: beta -f (if beta < 0, then compute beta = average gradient value in image)
# 	// 17: useWeightedLaplacian (flag for whether to use weighted laplacian)
# 	// 18: doRandomWalkerSeg (flag for whether to perform RandomWalker segmentation)
# 	// 19: seg.raw (segmentation output)
# 	// 20: imageBoundaryValue (if > 0, then this value is placed on the edges of the image to create an "extension" boundary


	# using weighted laplacian leads to some not nice results on our hippocampi,
	# since their image characteristics are SO different!
	# should theoretically work very well though...

	useWeightedLaplacian=0
	doRandomWalkerSeg=0

	# number of iterations of SOR:
	NUM_ITERATIONS=2500
	
	# if > 0, then this value is added to border pixels to extend potential outwards toward border
	imageBoundaryValue=0
	
	NX=`c3d bnd_ellipsoid.nii.gz -info-full | grep ' dim\[1\]' | sed 's/    dim\[1\] = //'`
	NY=`c3d bnd_ellipsoid.nii.gz -info-full | grep ' dim\[2\]' | sed 's/    dim\[2\] = //'`
	NZ=`c3d bnd_ellipsoid.nii.gz -info-full | grep ' dim\[3\]' | sed 's/    dim\[3\] = //'`

	SX=`c3d bnd_ellipsoid.nii.gz -info-full | grep ' pixdim\[1\]' | sed 's/    pixdim\[1\] = //'`
	SY=`c3d bnd_ellipsoid.nii.gz -info-full | grep ' pixdim\[2\]' | sed 's/    pixdim\[2\] = //'`
	SZ=`c3d bnd_ellipsoid.nii.gz -info-full | grep ' pixdim\[3\]' | sed 's/    pixdim\[3\] = //'`
		
	${DANCODE_HOME}/poisson \
				${FIXRUNDIR}/bnd.raw \
				junk.raw \
				${FIXRUNDIR}/pot-init.raw \
				${FIXRUNDIR}/pot.raw \
				${FIXRUNDIR}/gradx.raw ${FIXRUNDIR}/grady.raw ${FIXRUNDIR}/gradz.raw \
				${NX} ${NY} ${NZ} \
				${SX} ${SY} ${SZ} \
				${NUM_ITERATIONS} \
				0.6 \
				-0.1 \
				${useWeightedLaplacian} \
				${doRandomWalkerSeg} \
				junk.raw \
				${imageBoundaryValue}


	# We need to extend potential inwards within hippo mask and outwards from ellipsoid mask.
	# Do this using the signed distance transform.
	
	# This distance transform will be added to harmonic potential map in order to give gradient some non-zero value
	# on inside of hippo mask and outside of ellipsoid mask.
	
	# Create distance transform that goes from -1 at center of hippo mask to 0 on border.
	c3d halfway_mask.nii.gz -duplicate \
	    -sdt -multiply \
	    -stretch 0% 100% -1 0 \
	    -o temp_dist.nii.gz
	
	# Create distance transform that goes from 0 at ellipsoid border to some positive value outside.
	c3d bnd_ellipsoid.nii.gz \
	    -replace 1 0 -sdt \
	    -clip -inf 0 -scale -1 \
	    -stretch 0 50 0 1 \
	    -o temp_dist2.nii.gz
	
	# Add these two distance transforms to the potential image:
	c3d temp_dist.nii.gz temp_dist2.nii.gz -add -dup \
      ${FIXRUNDIR}/pot.mhd -copy-transform -add \
	    -type double -o ${FIXRUNDIR}/pot-sdt.mhd
	
	
	rm temp_dist.nii.gz
	rm temp_dist2.nii.gz
	
	# convert potential map and its gradients to NII format images:
	c3d ${FIXRUNDIR}/pot.mhd -type double -o ${FIXRUNDIR}/pot.nii.gz
	c3d ${FIXRUNDIR}/pot-sdt.mhd -type double -o ${FIXRUNDIR}/pot-sdt.nii.gz

	c3d ${FIXRUNDIR}/gradx.mhd ${FIXRUNDIR}/grady.mhd ${FIXRUNDIR}/gradz.mhd \
	    -type double -omc ${FIXRUNDIR}/grad.nii.gz
	
	# delete the MHD images:
	### rm ${FIXRUNDIR}/bnd.raw ${FIXRUNDIR}/bnd.mhd

	### rm ${FIXRUNDIR}/pot.raw ${FIXRUNDIR}/pot-sdt.raw
	### rm ${FIXRUNDIR}/pot.mhd ${FIXRUNDIR}/pot-sdt.mhd
		
	### rm ${FIXRUNDIR}/gradx.raw ${FIXRUNDIR}/grady.raw ${FIXRUNDIR}/gradz.raw
	### rm ${FIXRUNDIR}/gradx.mhd ${FIXRUNDIR}/grady.mhd ${FIXRUNDIR}/gradz.mhd

# ========================
# FROM 03_run_streamline_ellipsoid_eig.sh
# ========================
FIXRUNDIR=run-ellipsoid

# VTK mesh of halfway mask:
MESH=${OUT_MESH_HW}

# save out just the list of vertices to an .xyz pointcloud file:
IN_VERTICES_XYZ="halfway.xyz"
IN_FACES="halfway.faces"

# subdivide the faces of mesh triangles if they get stretched to0 wide on ellipsoid after streaming
DO_SUBDIVISION=1
OUT_FACES="halfway.faces.sub"
OUT_VERTICES_XYZ="halfway.xyz.sub"

matlab -nodisplay -singleCompThread -r "\
  addpath('${DANCODE_HOME}/matlab'); \
  mesh = vtk_mesh_read('${MESH}'); \
  verts = mesh.points; \
  faces = cell2mat(mesh.cells.polygons) - 1; \
  save('${IN_VERTICES_XYZ}', '-ascii', 'verts'); \
  fid = fopen('${IN_FACES}', 'wt'); \
  fprintf(fid, '%d %d %d\n', faces); \
  fclose(fid); \
  quit;"

# output files from streaming of ellipsoid to halfway surface:
# --> output spherical angles (theta, phi) and geodesic distance rho traveled from
#     original source mesh to ellipsoid boundary along streamlines:
OUT_ANGLES_TPR="halfway_to_spherical_angles.xyz"

# --> vertices mapped to ellipsoid following streamlining (x,y,z in MM coords):
OUT_ELLIPSOID_XYZ="halfway_to_ellipsoid.xyz"
  
# --> vertices mapped to sphere (x,y,z in MM coords):
OUT_SPHERE_XYZ="halfway_to_unit_sphere.xyz"


# create headers for gradients:
c3d ellipsoid.nii -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/gradx-sdt.mhd
c3d ellipsoid.nii -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/grady-sdt.mhd
c3d ellipsoid.nii -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/gradz-sdt.mhd

# get summary info
c3d ellipsoid.nii -info-full > temp_info.txt

NX=`cat temp_info.txt | grep ' dim\[1\]' | sed 's/    dim\[1\] = //'`
NY=`cat temp_info.txt | grep ' dim\[2\]' | sed 's/    dim\[2\] = //'`
NZ=`cat temp_info.txt | grep ' dim\[3\]' | sed 's/    dim\[3\] = //'`

SX=`cat temp_info.txt | grep ' pixdim\[1\]' | sed 's/    pixdim\[1\] = //'`
SY=`cat temp_info.txt | grep ' pixdim\[2\]' | sed 's/    pixdim\[2\] = //'`
SZ=`cat temp_info.txt | grep ' pixdim\[3\]' | sed 's/    pixdim\[3\] = //'`

SROW_X=`cat temp_info.txt | grep srow_x | sed -e 's/.* = //g'`
SROW_Y=`cat temp_info.txt | grep srow_y | sed -e 's/.* = //g'`
SROW_Z=`cat temp_info.txt | grep srow_z | sed -e 's/.* = //g'`
SROW_W="0.0  0.0  0.0  1.0"

SROW_FILE="srow.txt"
echo -e "${SROW_X}\n${SROW_Y}\n${SROW_Z}\n${SROW_W}" > ${SROW_FILE}

STEP=0.025
INTEGRATION_TYPE=2 # RK-4

# tolerance from ellipsoid boundary within which to terminate the streamlining:
SINK_TOL=0.0

DST_LO=2.0
DST_HI=3.0

# xyz voxel unit coords of ellipsoid center (starting at 0!!!):

# !!! MATLAB gives back center with voxels starting at 1 !!!
# This is already taken care of in the MATLAB script of 02_create_bnd_ellipsoid.sh
CENTER=`sed -n '1p' < "ellipsoid_center.txt"`

# eigenvectors corresponding to the three axes:
# -> these are the columns of V from [V,D] = eig(A)

# !!!!!! BE SURE TO ORDER THEM LIKE THIS!!!!

# need to map LONGEST axis of ellipsoid to X
#             MEDIUM axis to Y
#             SHORTEST axis to Z
EIGVEC1=`sed -n '1p' < "ellipsoid_axes_inrows.txt"`
EIGVEC2=`sed -n '2p' < "ellipsoid_axes_inrows.txt"`
EIGVEC3=`sed -n '3p' < "ellipsoid_axes_inrows.txt"`

# radii of the three axes (in voxel units):
# -> these are the diagonal elements of D^(-0.5)
# !!!!!!!! ORDER THESE VALUES FROM LARGEST TO SMALLEST!!!!!!
RADII=`sed -n '1p' < "ellipsoid_radii.txt"`
 
# edges between vertices separated by angle larger than this (in degrees) are split in two:
MAX_ANGLE_DEGREES=5.0

# maximum subdivision recursions:
### MAX_RECURSION_DEPTH=8  # Paul: this crashes!!!
MAX_RECURSION_DEPTH=8

# subdivide the faces?
# -> if this variable == 0, then the EDGES are subdivided
# -> if == 1, then FACES are subdivided
# subdividing faces leads to more pleasant looking mesh, but this is irrelevant for our work
SUBDIVIDE_FACES=0

# 0: stream
  # 1: inputVertices.xyz (x,y,z in in mm)
  # 2: inputFaces.ijk (indices i,j,k from 0): triangle faces of input mesh
  # 3: outputVertices.xyz (x,y,z in in mm): vertices after subdivision
  # 4: outputFaces.ijk (indices i,j,k from 0): triangle faces after subdivision
  # 5: outputAngles.xyz (vertices mapped to spherical angles theta, phi and distance rho)
  # 6: outputSphere.xyz (vertices in mm mapped to unit sphere's x,y,z coordinates)
  # 7: outputEllipsoid.xyz (vertices mapped to bounding ellipsoid's x,y,z coordinates)
  # 8: potential.raw (in double format)
  # 9-11: gradx.raw; grady.raw; gradz.raw (for output)
  # 12-14: NX; NY; NZ
  # 15-17: SX; SY; SZ
  # 18: SROW_FILE (vox2mm matrix for image)
  # 19: spacing step in integration
  # 20: input destination low potential for end
  # 21: input destination high potential for end
  # 22-24: centerX centerY centerZ (x,y,z coords of center of ellipsoid)
  # 25-27: eigvec1X eigvec1Y eigvec1Z (normalized vector of ellipsoid's 1st axis)
  # 28-30: eigvec2X eigvec2Y eigvec2Z (normalized vector of ellipsoid's 2nd axis)
  # 31-33: eigvec3X eigvec3Y eigvec3Z (normalized vector of ellipsoid's 3rd axis)
  # 34-36: radius1 radius2 radius3 (ellipsoid axis radii -- 1/sqrt(D))
    # 37: sinkTol (stopping tolerance of sink: vertices terminate when reaching within this tolerance of sink)
  # 38: integrationType (0: NN, 1: Euler, 2: RK4)
  # 39: subdivideTriangles: whether to subdivide input triangle faces that get stretched to have long sides on ellipsoid
  # 40: maxAngleDegrees: angle beyond which edges are subdivided (in degrees)
  # 41: maxRecursionDepth: maximum subdivision recursion depth

### I can only get stream to work on a Mac. So send the data to the 
### Mac and run from there
tar -czvf tomac.tgz \
  ${IN_VERTICES_XYZ} \
  ${IN_FACES} \
  ${FIXRUNDIR}/pot-sdt.raw \
  ${FIXRUNDIR}/pot-sdt.mhd \
  ${FIXRUNDIR}/gradx-sdt.raw ${FIXRUNDIR}/grady-sdt.raw ${FIXRUNDIR}/gradz-sdt.raw \
  ${FIXRUNDIR}/gradx-sdt.mhd ${FIXRUNDIR}/grady-sdt.mhd ${FIXRUNDIR}/gradz-sdt.mhd \
  $SROW_FILE

ssh picsl@10.150.13.189 mkdir -p /Users/picsl/tk/stream/data/$$
scp tomac.tgz picsl@10.150.13.189:/Users/picsl/tk/stream/data/$$

###	${DANCODE_HOME}/streamMeshEllipsoidSubdivideEigenDCEL \

ssh picsl@10.150.13.189 "\
  cd /Users/picsl/tk/stream/data/$$; \
  tar -zxvf tomac.tgz; \
  rm -rf tomac.tgz; \
  ../../stream \
      ${IN_VERTICES_XYZ} \
      ${IN_FACES} \
      ${OUT_VERTICES_XYZ} \
      ${OUT_FACES} \
      ${OUT_ANGLES_TPR} \
      ${OUT_SPHERE_XYZ} \
      ${OUT_ELLIPSOID_XYZ} \
      ${FIXRUNDIR}/pot-sdt.raw \
      ${FIXRUNDIR}/gradx-sdt.raw ${FIXRUNDIR}/grady-sdt.raw ${FIXRUNDIR}/gradz-sdt.raw \
      ${NX} ${NY} ${NZ} \
      ${SX} ${SY} ${SZ} \
      ${SROW_FILE} \
      ${STEP} \
      ${DST_LO} ${DST_HI} \
      ${CENTER} \
      ${EIGVEC1} ${EIGVEC2} ${EIGVEC3} \
      ${RADII} \
      ${SINK_TOL} \
      ${INTEGRATION_TYPE} \
      ${DO_SUBDIVISION} \
      ${MAX_ANGLE_DEGREES} \
      ${MAX_RECURSION_DEPTH} \
      ${SUBDIVIDE_FACES} ; \
  tar -czvf frommac.tgz \
    ${OUT_VERTICES_XYZ} \
    ${OUT_FACES} \
    ${OUT_ANGLES_TPR} \
    ${OUT_SPHERE_XYZ} \
    ${OUT_ELLIPSOID_XYZ} \
    ${FIXRUNDIR}/pot-sdt.raw \
    ${FIXRUNDIR}/pot-sdt.mhd \
    ${FIXRUNDIR}/gradx-sdt.raw ${FIXRUNDIR}/grady-sdt.raw ${FIXRUNDIR}/gradz-sdt.raw \
    ${FIXRUNDIR}/gradx-sdt.mhd ${FIXRUNDIR}/grady-sdt.mhd ${FIXRUNDIR}/gradz-sdt.mhd"

# Copy the data back from the mac
scp picsl@10.150.13.189:/Users/picsl/tk/stream/data/$$/frommac.tgz .
ssh picsl@10.150.13.189 "rm -rf /Users/picsl/tk/stream/data/$$"
tar -zxvf frommac.tgz

# Remove the tarballs and weird files
rm -rf *.tgz ._* 
    
# add spherical angles to original input mesh.
# also create new meshes of the target unit sphere and ellipsoid.
# normalize the geodesic distance RHO to range [0,1].

MESH_WITH_ANGLES="halfway_with_angles.vtk"
MESH_SPHERE="halfway_to_unit_sphere.vtk"
MESH_ELLIPSOID="halfway_to_ellipsoid.vtk"
  
matlab -nodisplay -singleCompThread -r "\
  addpath('${DANCODE_HOME}/matlab'); \
  mesh = vtk_mesh_read('${MESH}'); \
  \
  vertices = load('${OUT_VERTICES_XYZ}'); \
  mesh.points = vertices; \
  mesh = vtk_del_point_data(mesh, 'normals'); \
  \
  faces = load('${OUT_FACES}'); \
  faces = faces(:,1:3)' + 1; \
  new_polygons = cell(1,size(faces,2)); \
  for i = 1:size(faces,2) \
    new_polygons{i} = faces(:,i);  \
  end; \
  mesh = rmfield(mesh, 'cells'); \
  mesh.cells.polygons = new_polygons; \
  \
  spherical_angles = load('${OUT_ANGLES_TPR}'); \
  spherical_angles(abs(spherical_angles) < 1.0e-16) = 0.0; \
  spherical_angles(isnan(spherical_angles)) = 0.0; \
  \
  rhoRange = max(spherical_angles(:,3)) - min(spherical_angles(:,3)); \
  spherical_angles(:,3) = (spherical_angles(:,3) - min(spherical_angles(:,3))) / rhoRange; \
  \
  ellipsoid_xyz = load('${OUT_ELLIPSOID_XYZ}'); \
  ellipsoid_xyz(abs(ellipsoid_xyz) < 1.0e-16) = 0.0; \
  ellipsoid_xyz(isnan(ellipsoid_xyz)) = 0.0; \
  \
  sphere_xyz = load('${OUT_SPHERE_XYZ}'); \
  sphere_xyz(abs(sphere_xyz) < 1.0e-16) = 0.0; \
  sphere_xyz(isnan(sphere_xyz)) = 0.0; \
  \
  mesh = vtk_add_point_data(mesh, 'theta', spherical_angles(:,1), true); \
  mesh = vtk_add_point_data(mesh, 'phi', spherical_angles(:,2), true); \
  mesh = vtk_add_point_data(mesh, 'rho', spherical_angles(:,3), true); \
  vtk_mesh_write('${MESH_WITH_ANGLES}', mesh); \
  \
  mesh.points = ellipsoid_xyz; \
  vtk_mesh_write('${MESH_ELLIPSOID}', mesh); \
  \
  mesh.points = sphere_xyz; \
  vtk_mesh_write('${MESH_SPHERE}', mesh); \
  \
  quit;"


# convert potential map and its gradients to NII format images:
c3d ${FIXRUNDIR}/gradx-sdt.mhd ${FIXRUNDIR}/grady-sdt.mhd ${FIXRUNDIR}/gradz-sdt.mhd \
    -type double -omc ${FIXRUNDIR}/grad-sdt.nii.gz

# delete MHD images:
### rm ${FIXRUNDIR}/gradx${EXTEND}-sdt.raw ${FIXRUNDIR}/grady${EXTEND}-sdt.raw ${FIXRUNDIR}/gradz${EXTEND}-sdt.raw
### rm ${FIXRUNDIR}/gradx${EXTEND}-sdt.mhd ${FIXRUNDIR}/grady${EXTEND}-sdt.mhd ${FIXRUNDIR}/gradz${EXTEND}-sdt.mhd

### rm ${FIXRUNDIR}/pot${EXTEND}-sdt.mhd
### rm ${FIXRUNDIR}/pot${EXTEND}-sdt.raw

#SKIP3

# ========================
# FROM 01_streamline_vol_fill_intersect_mesh.sh
# ========================

FIXRUNDIR=run-volfill
mkdir -p ${FIXRUNDIR}

# input potential file:
# -> computed after cm-rep warping of boundaries
IN_POTENTIAL=$OUT_POT

# files containing lists of vertex coords and spherical angles:
# -> C++ code streamline.cpp needs to read these files instead of mesh
IN_VERTICES="halfway.xyz.sub"
IN_ANGLES="halfway_to_spherical_angles.xyz"

# list of face triangles of mesh:
IN_FACES="halfway.faces.sub"


# create headers for gradients:
c3d ${IN_POTENTIAL} -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/gradx-extend.mhd
c3d ${IN_POTENTIAL} -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/grady-extend.mhd
c3d ${IN_POTENTIAL} -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/gradz-extend.mhd

# create headers for filled volumes of spherical angles:
c3d ${IN_POTENTIAL} -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/volfill_theta-extend.mhd
c3d ${IN_POTENTIAL} -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/volfill_phi-extend.mhd
c3d ${IN_POTENTIAL} -thresh -inf inf 0 0 -type double -o ${FIXRUNDIR}/volfill_rho-extend.mhd

# get summary info (this is redundant really)
c3d ${IN_POTENTIAL} -info-full > temp_info.txt

NX=`cat temp_info.txt | grep ' dim\[1\]' | sed 's/    dim\[1\] = //'`
NY=`cat temp_info.txt | grep ' dim\[2\]' | sed 's/    dim\[2\] = //'`
NZ=`cat temp_info.txt | grep ' dim\[3\]' | sed 's/    dim\[3\] = //'`

SX=`cat temp_info.txt | grep ' pixdim\[1\]' | sed 's/    pixdim\[1\] = //'`
SY=`cat temp_info.txt | grep ' pixdim\[2\]' | sed 's/    pixdim\[2\] = //'`
SZ=`cat temp_info.txt | grep ' pixdim\[3\]' | sed 's/    pixdim\[3\] = //'`

SROW_X=`cat temp_info.txt | grep srow_x | sed -e 's/.* = //g'`
SROW_Y=`cat temp_info.txt | grep srow_y | sed -e 's/.* = //g'`
SROW_Z=`cat temp_info.txt | grep srow_z | sed -e 's/.* = //g'`
SROW_W="0.0  0.0  0.0  1.0"

SROW_FILE="srow.txt"
echo -e "${SROW_X}\n${SROW_Y}\n${SROW_Z}\n${SROW_W}" > ${SROW_FILE}

# use a large step, since there are 12M voxels!
STEP=0.05
INTEGRATION_TYPE=2 # RK-4

# values between which to begin searching for intersections of streamline rays with mesh:
# -> this allows for some cushion before costly ray-mesh intersection tests
DST_LO=0.9 # inner halfway surface
DST_HI=2.1 # outer halfway surface


# run streamlines from all voxels within these bounds:
SRC_LO=1.0;
SRC_HI=3.0;

# precision of hitting the boundary:
EPSILON=0.0

# maximum number of integration steps along streamline:
# -> if this number is exceeded, then integration halts (this stops infinite loop
#    in the case of bad gradients or unreachable sink)
MAX_INTEGRATIONS=10000



# We need to extend potential inwards within dark band mask:
# Do this using the signed distance transform.

# This distance transform will be added to harmonic potential map in order to give gradient some non-zero value
# on inside of DB mask.

# Create distance transform that goes from -1 at center of DB mask to 0 on border.
#	c3d ${FIXDIR}/${FIX}_db_mask.nii.gz -duplicate -sdt -multiply -stretch 0% 100% -1 0 -o ${FIXDIR}/temp_dist.nii.gz

# Add this distance transform to the potential image:
#	c3d ${FIXDIR}/temp_dist.nii.gz ${FIXRUNDIR}/pot${EXTEND}.mhd -add -type double -o ${FIXRUNDIR}/pot${EXTEND}-sdt.mhd

#	rm ${FIXDIR}/temp_dist.nii.gz


# create header for mask of voxels that were hit by streamlines:
c3d ${IN_POTENTIAL} -thresh -inf inf 0 0 -type uchar -o ${FIXRUNDIR}/hitmask-extend.mhd

# create copy of potential in MHD format
c3d ${IN_POTENTIAL} -type double -o ${FIXRUNDIR}/pot-extend.mhd


# Trace streamlines from each image voxel towards mesh:
# 0: streamlinesVolFillToMesh
    # 1: inputVertices.xyz (x,y,z in mm from inputMesh.vtk)
    # 2: inputSphericalAngles.xyz (spherical angles theta, phi, rho from inputMesh_with_angles.vtk)
  # 3: inputFaces.ijk (mesh triangle faces: i j k indices -- starting at 0 -- per line)
    # 4: potential.raw (in double format)
    # 5: volFillTheta.raw : output volume filled with theta (in double format)
    # 6: volFillPhi.raw : output volume filled with phi (in double format)
    # 7: volFillRho.raw : output volume filled with rho (in double format)
    # 8-10: gradx.raw; grady.raw; gradz.raw (for output)
    # 11-13: NX; NY; NZ
    # 14-16: SX; SY; SZ
    # 17: SROW_FILE (vox2mm matrix for image)
    # 18: spacing step in integration
    # 19: input destination low potential for ending streamlining (e.g. 1.49)
    # 20: input destination high potential for ending streamlining (e.g. 1.51)
    # 21: integrationType (0: NN, 1: Euler, 2: RK4)
  # 22: epsilon (how close to allow us to get to destination potential)
  # 23: maxIntegrations (maximum number of integration steps along streamline)
  # 24: hitmask.raw (mask of voxels that generated streamlines that intersected the mesh)
  # 25: input source low potential for starting streamlining
  # 26: input source high potential for starting streamlining

#	${DANCODE_HOME}/streamlinesVolFillToMesh \
${DANCODE_HOME}/volfill \
    ${IN_VERTICES} \
    ${IN_ANGLES} \
    ${IN_FACES} \
    ${FIXRUNDIR}/pot-extend.raw \
    ${FIXRUNDIR}/volfill_theta-extend.raw \
    ${FIXRUNDIR}/volfill_phi-extend.raw \
    ${FIXRUNDIR}/volfill_rho-extend.raw \
    ${FIXRUNDIR}/gradx-extend.raw ${FIXRUNDIR}/grady-extend.raw ${FIXRUNDIR}/gradz-extend.raw \
    ${NX} ${NY} ${NZ} \
    ${SX} ${SY} ${SZ} \
    ${SROW_FILE} \
    ${STEP} \
    ${DST_LO} ${DST_HI} \
    ${INTEGRATION_TYPE} \
    ${EPSILON} \
    ${MAX_INTEGRATIONS} \
    ${FIXRUNDIR}/hitmask-extend.raw \
    ${SRC_LO} ${SRC_HI}
    
# Convert to outputs
c3d ${FIXRUNDIR}/volfill_theta-extend.mhd -o $(printf $OUTPUT "theta_extend").nii.gz
c3d ${FIXRUNDIR}/volfill_phi-extend.mhd -o $(printf $OUTPUT "phi_extend").nii.gz
c3d ${FIXRUNDIR}/volfill_rho-extend.mhd -o $(printf $OUTPUT "rho_extend").nii.gz

# remove temporary potential:
###rm ${FIXRUNDIR}/pot${EXTEND}.mhd ${FIXRUNDIR}/pot${EXTEND}.raw

# remove the gradients and boundaries:
###rm ${FIXRUNDIR}/gradx${EXTEND}.mhd ${FIXRUNDIR}/grady${EXTEND}.mhd ${FIXRUNDIR}/gradz${EXTEND}.mhd
###rm ${FIXRUNDIR}/gradx${EXTEND}.raw ${FIXRUNDIR}/grady${EXTEND}.raw ${FIXRUNDIR}/gradz${EXTEND}.raw


# -----------------------------------------------
# ---- FIX HOLES IN VOLFILL OUTPUT --------------
# -----------------------------------------------

FIXRUNDIR=run-holefill
mkdir -p $FIXRUNDIR

# Generate the inputs to the interpolation code
MYMAP=$(printf $OUTPUT "theta_extend").nii.gz
c3d $MYMAP \
  -shift 4 -scale 1000 \
  run-volfill/hitmask-extend.mhd -thresh 1 inf 1 0 -times \
  -type ushort -o $FIXRUNDIR/input_short.mhd \
  -type double -o $FIXRUNDIR/pot.mhd \
  -scale 0 \
  -o $FIXRUNDIR/gradx.mhd \
  -o $FIXRUNDIR/grady.mhd \
  -o $FIXRUNDIR/gradz.mhd

# get summary info (this is redundant really)
c3d ${MYMAP} -info-full > temp_info.txt
NX=`cat temp_info.txt | grep ' dim\[1\]' | sed 's/    dim\[1\] = //'`
NY=`cat temp_info.txt | grep ' dim\[2\]' | sed 's/    dim\[2\] = //'`
NZ=`cat temp_info.txt | grep ' dim\[3\]' | sed 's/    dim\[3\] = //'`

${DANCODE_HOME}/poisson \
  $FIXRUNDIR/input_short.raw \
  junk.raw \
  $FIXRUNDIR/pot.raw \
  $FIXRUNDIR/pot.raw \
  $FIXRUNDIR/gradx.raw $FIXRUNDIR/grady.raw $FIXRUNDIR/gradz.raw \
  $NX $NY $NZ 1.0 1.0 1.0 \
  500 0.6 -0.1 0 0 \
  $FIXRUNDIR/junk.raw \
  0

# Process the potential back
c3d $FIXRUNDIR/pot.mhd -scale 0.001 -shift -4 \
  -o $(printf $OUTPUT "theta_extend_hfill").nii.gz


# Generate the inputs to the interpolation code
MYMAP=$(printf $OUTPUT "phi_extend").nii.gz
c3d $MYMAP \
  -shift 4 -scale 1000 \
  run-volfill/hitmask-extend.mhd -thresh 1 inf 1 0 -times \
  -type ushort -o $FIXRUNDIR/input_short.mhd \
  -type double -o $FIXRUNDIR/pot.mhd \
  -scale 0 \
  -o $FIXRUNDIR/gradx.mhd \
  -o $FIXRUNDIR/grady.mhd \
  -o $FIXRUNDIR/gradz.mhd

# get summary info (this is redundant really)
c3d ${MYMAP} -info-full > temp_info.txt
NX=`cat temp_info.txt | grep ' dim\[1\]' | sed 's/    dim\[1\] = //'`
NY=`cat temp_info.txt | grep ' dim\[2\]' | sed 's/    dim\[2\] = //'`
NZ=`cat temp_info.txt | grep ' dim\[3\]' | sed 's/    dim\[3\] = //'`

${DANCODE_HOME}/poisson \
  $FIXRUNDIR/input_short.raw \
  junk.raw \
  $FIXRUNDIR/pot.raw \
  $FIXRUNDIR/pot.raw \
  $FIXRUNDIR/gradx.raw $FIXRUNDIR/grady.raw $FIXRUNDIR/gradz.raw \
  $NX $NY $NZ 1.0 1.0 1.0 \
  500 0.6 -0.1 0 0 \
  $FIXRUNDIR/junk.raw \
  0

# Process the potential back
c3d $FIXRUNDIR/pot.mhd -scale 0.001 -shift -4 \
  -o $(printf $OUTPUT "phi_extend_hfill").nii.gz


# Compute sin and cos of theta
c3d $(printf $OUTPUT "phi_extend_hfill").nii.gz \
  -cos -o $(printf $OUTPUT "cos_phi_extend_hfill").nii.gz

c3d $(printf $OUTPUT "phi_extend_hfill").nii.gz \
  -sin -o $(printf $OUTPUT "sin_phi_extend_hfill").nii.gz

# Go back
popd
