#!/bin/bash
#$ -S /bin/bash
set -x -e

# The directory where we are doing this work
ROOT=/data/picsl/pauly/dan2015/atlas2016

# The work directory
WDIR=$ROOT/mst

# Read the common include file
source $ROOT/scripts/common.sh

# List of all the ids to run
IDS=$(cat $ROOT/scripts/subj.txt)

# Global parameters - reused by script
GSHOOT_NITER=5
GSHOOT_NITER_NCC=5
NCC_NITER=6

# Path to the old atlas from 2009
OLD_ATLAS_HF=$ROOT/atlas09_package/pmatlas_mask.nii.gz

# The side of the old atlas
OLD_ATLAS_SIDE="L"

# Function : return the input image, hfseg and dbseg for a subject
# use with read a b c <<<$(fn_input_unwarped_images $id)
function fn_input_unwarped_images()
{
  local ID=${1?}
  
  # Template is being built from images that have been unwarped to correct
  # for scanner gradient scaling
  local IM=$ROOT/preproc/$ID/unwarp/${ID}_unwarp_img.nii.gz
  local HF=$ROOT/preproc/$ID/unwarp/${ID}_unwarp_hfseg.nii.gz
  local DB=$ROOT/preproc/$ID/unwarp/${ID}_unwarp_dbseg.nii.gz

  echo $IM $HF $DB
}

# Private function used by reslice_subj() and reslice_subj_segsonly()
function reslice_subj_internal()
{
  # Mode flags
  local DO_IMG=${1?}
  local DO_SEG=${2?}

  # Subject's ID
  local ID=${3?}

  # Reference space
  local REFSPACE=${4?}
  local OUTPATTERN=${5?}

  # The requested warp chain
  shift 5
  local WARPCHAIN="$@"

  # Subject's preproc directory
  local PDIR=$ROOT/preproc/${ID}

  # Subject's RAW MRI scan
  local RAW_IMG=$PDIR/${ID}_raw_n4clip.nii.gz

  # Subject's source segmentation of DB and HF
  local HFSEG=$PDIR/${ID}_danseg_hfseg.nii.gz
  local DBSEG=$PDIR/${ID}_danseg_dbseg.nii.gz

  # Subject's unwarp and dan/raw matrices
  local UNWARP_MAT=$PDIR/unwarp/${ID}_unwarp.mat
  local SEG_TO_RAW_MAT=$PDIR/${ID}_danseg_to_raw.mat

  # Warp chain from raw image space to template space
  local CHAIN_RAW_TO_TEMPLATE="$WARPCHAIN $UNWARP_MAT"

  # Warp chain from input image space (where segmentation drawn) to template
  local CHAIN_INPUT_TO_TEMPLATE="$CHAIN_RAW_TO_TEMPLATE $SEG_TO_RAW_MAT"

  # Reslice the image
  if [[ $DO_IMG -eq 1 ]]; then
    $GREEDY_HOME/greedy -d 3 \
      -rf $REFSPACE \
      -rm $RAW_IMG $(echo $OUTPATTERN | sed -e "s/XXX/img/g") \
      -r $CHAIN_RAW_TO_TEMPLATE
  fi

  # Reslice the segmentations
  if [[ $DO_SEG -eq 1 ]]; then
    $GREEDY_HOME/greedy -d 3 \
      -rf $REFSPACE \
      -ri LABEL 0.24vox \
      -rm $HFSEG $(echo $OUTPATTERN | sed -e "s/XXX/hf/g") \
      -rm $DBSEG $(echo $OUTPATTERN | sed -e "s/XXX/db/g") \
      -r $CHAIN_INPUT_TO_TEMPLATE
  fi
}


# This function reslices the subject's RAW image data and native space
# segmentations into the provided reference image space using the chain
# of transforms between the reference space and the subject's UNWARPED
# space.
# 
# Usage:
#   reslice_subj ID reference output_pattern warp_chain
# 
# Output pattern must include string XXX which will be replaced by 
# img, hf and db respectively.
function reslice_subj()
{
  reslice_subj_internal 1 1 $@
}

# Same as above but segmentations only
function reslice_subj_segsonly()
{
  reslice_subj_internal 0 1 $@
}


# Function : perform 'smooth' registration between pair of scans in order
# to evaluate similarity
function fn_pairwise_rough()
{
  # The ids of the two subjects
  IDREF=${1?}
  IDMOV=${2?}

  # The images being used
  read IMGREF HFREF DBREF <<<$(fn_input_unwarped_images $IDREF)
  read IMGMOV HFMOV DBMOV <<<$(fn_input_unwarped_images $IDMOV)

  # The tag for this pair
  TAG="ref_${IDREF}_mov_${IDMOV}"

  # Registration will be performed on the label masks of the two images
  # using a very high smoothness term
  WORK=$WDIR/pairwise/${TAG}
  mkdir -p $WORK

  # warp file
  mat_moments=$work/${tag}_moments.mat
  mat_affine=$work/${tag}_affine.mat
  WARP=$WORK/${TAG}_warp.nii.gz

  # Resliced moving segmentations
  RSLPATTERN=$WORK/${TAG}_reslice_XXXseg.nii.gz
  HFRSL=$WORK/${TAG}_reslice_hfseg.nii.gz
  DBRSL=$WORK/${TAG}_reslice_dbseg.nii.gz

  # Make symlinks
  for fn in $HFREF $HFMOV $DBREF $DBMOV; do
    ln -sf $fn $WORK/
  done

  # Do we need to flip space between these two segmentations?
  SIDE_REF=$(echo $IDREF | awk -F '-' '{print $NF}')
  SIDE_MOV=$(echo $IDMOV | awk -F '-' '{print $NF}')

  if [[ $SIDE_REF == $SIDE_MOV ]]; then DET="1"; else DET="-1"; fi

  # This line of greedy inputs and weights will be reused
  GREEDY_INPUTS="-w 1 -i $HFREF $HFMOV -w 5 -i $DBREF $DBMOV"

  # Perform moments of intertia matching between the two masks
  $GREEDY_HOME/greedy -d 3 -threads $NSLOTS \
    $GREEDY_INPUTS \
    -moments -det $DET \
    -o $MAT_MOMENTS

  # Perform affine matching between the two masks
  $GREEDY_HOME/greedy -d 3 -threads $NSLOTS \
    $GREEDY_INPUTS \
    -a -ia $MAT_MOMENTS \
    -n 100x100 \
    -o $MAT_AFFINE
  
  # Run greedy between these two images
  $GREEDY_HOME/greedy -d 3 -threads $NSLOTS \
    $GREEDY_INPUTS \
    -it $MAT_AFFINE \
    -n 50x50x20x0 \
    -s 2.0mm 0.1mm -e 0.5 \
    -o $WARP

  # Reslice the segmentations from raw space
  reslice_subj_segsonly $IDMOV $HFREF $RSLPATTERN $WARP $MAT_AFFINE

  # Compute the overlaps
  $C3D_HOME/c3d \
    $HFREF $DBREF -add \
    $HFRSL $DBRSL -add \
    -label-overlap | tee $WORK/${TAG}_overlap.txt

  # Write just the adjacency value
  cat $WORK/${TAG}_overlap.txt | awk 'NR==3 {print $3}' > $WORK/${TAG}_adj.txt
}

# TODO: THIS FUNCTION NEEDS TO BE UPDATED!!!!
# This function is the alternative to the MST method - it directly
# registers everyone to the root of the MST, without taking paths 
# through the tree. The point is to show that the MST is giving us
# some advantage
fn_register_direct()
{
  # The index of the subject being registered using the MST
  idx=${1?}

  # The ID of the subject
  ID=$(cat $ROOT/scripts/subj.txt | head -n $idx | tail -n 1)

  # The path of nodes leading up to the root node (from current to the root)
  # Modification: drop all the elements of GPATH except first and last
  GPATH_FULL=($(cat $WDIR/paths.txt | head -n $idx | tail -n 1))
  N_FULL=${#GPATH_FULL[*]}
  GPATH=(${GPATH_FULL[0]} ${GPATH_FULL[$((N_FULL-1))]})

  # The native space images and registrations
  read IMG_NAT HF_NAT DB_NAT <<<$(fn_input_unwarped_images $ID)

  # This is the current chain of registrations
  WARPCHAIN="$ROOT/work/${ID}/cmrep/template_cmrepinvwarp.nii.gz"

  # Current resliced moving images
  IMGMOV=$ROOT/work/${ID}/cmrspc/${ID}_img_cmrspc.nii.gz
  HFMOV=$ROOT/work/${ID}/cmrspc/${ID}_hffill_cmrspc.nii.gz
  DBMOV=$ROOT/work/${ID}/cmrspc/${ID}_dbseg_cmrspc.nii.gz

  # Loop. The first element of GPATH is the image itself
  for ((j=1;j<${#GPATH[*]};++j)); do

    # The next image in the path
    idxref=${GPATH[j]}
    IDREF=$(cat $ROOT/scripts/subj.txt | head -n $idxref | tail -n 1)

    # The target images
    IMGREF=$ROOT/work/${IDREF}/cmrspc/${IDREF}_img_cmrspc.nii.gz
    HFREF=$ROOT/work/${IDREF}/cmrspc/${IDREF}_hffill_cmrspc.nii.gz
    DBREF=$ROOT/work/${IDREF}/cmrspc/${IDREF}_dbseg_cmrspc.nii.gz

    # Create directory for this registration
    WORK=$WDIR/pdirect/${ID}/step_${j}
    mkdir -p $WORK

    # The output warp
    WARP=$WORK/warp.nii.gz

    # Create links for the relevant images
    ln -sf $IMGMOV $WORK/img_moving.nii.gz
    ln -sf $HFMOV $WORK/hf_moving.nii.gz
    ln -sf $DBMOV $WORK/db_moving.nii.gz

    ln -sf $IMGREF $WORK/img_ref.nii.gz
    ln -sf $HFREF $WORK/hf_ref.nii.gz
    ln -sf $DBREF $WORK/db_ref.nii.gz

    # Perform registration
    $GREEDY_HOME/greedy -d 3 \
      -w 1 -i $HFREF $HFMOV \
      -w 5 -i $DBREF $DBMOV \
      -threads $NSLOTS -n 100x50x40x20 \
      -s 0.6mm 0.1mm -e 0.5 -float \
      -o $WARP

    # Update the warp chain and the reslices
    WARPCHAIN="$WARP $WARPCHAIN"
    IMGMOV=$WORK/reslice_${ID}_to_${IDREF}_img.nii.gz
    HFMOV=$WORK/reslice_${ID}_to_${IDREF}_hf.nii.gz
    DBMOV=$WORK/reslice_${ID}_to_${IDREF}_db.nii.gz

    # Apply the deformations to the data
    $GREEDY_HOME/greedy -d 3 \
      -rf $HFREF \
      -rm $IMG_NAT $IMGMOV \
      -ri LABEL 0.24vox \
      -rm $HF_NAT $HFMOV \
      -rm $DB_NAT $DBMOV \
      -r $WARPCHAIN

  done

  # Create links to the "final" moving images
  mkdir -p $WDIR/pdirect/${ID}/final
  for fn in $IMGMOV $HFMOV $DBMOV; do
    ln -sf $fn $WDIR/pdirect/${ID}/final/
  done
}

function fn_register_root_to_oldatlas()
{
  # We would like the MST root to be correctly aligned with the old atlas so 
  # it's not strangely rotated.
  local ROOTIDX=$(cat $WDIR/paths.txt | awk 'NF==1 {print $1}')
  local ROOTID=$(cat $ROOT/scripts/subj.txt | head -n $ROOTIDX | tail -n 1)

  # The output directory
  local WORK=$WDIR/root_to_oldatlas

  # The input hippo segmentation
  local ROOT_IMG ROOT_HF ROOT_DB
  read ROOT_IMG ROOT_HF ROOT_DB <<<$(fn_input_unwarped_images $ROOTID)

  # The matrices output
  local MAT_MOMENTS=$WORK/root_to_atlas_moments.mat
  local MAT_RIGID=$WORK/root_to_atlas_rigid.mat

  # Do the registration
  mkdir -p $WORK

  # Perform moments of intertia matching between the two masks
  $GREEDY_HOME/greedy -d 3 -threads $NSLOTS \
    -i $OLD_ATLAS_HF $ROOT_HF \
    -moments -o $MAT_MOMENTS

  # Perform affine matching between the two masks
  $GREEDY_HOME/greedy -d 3 -threads $NSLOTS \
    -i $OLD_ATLAS_HF $ROOT_HF \
    -a -dof 6 -ia $MAT_MOMENTS \
    -n 100x100 \
    -o $MAT_RIGID
}

function fn_register_mst()
{
  # The index of the subject being registered using the MST
  idx=${1?}

  # The ID of the subject
  ID=$(cat $ROOT/scripts/subj.txt | head -n $idx | tail -n 1)

  # The path of nodes leading up to the root node (from current to the root)
  GPATH=($(cat $WDIR/paths.txt | head -n $idx | tail -n 1))

  # The native space images and registrations
  read IMGNAT HFNAT DBNAT <<<$(fn_input_unwarped_images $ID)

  # We start with the warp chain as an empty list
  WARPCHAIN=""

  # The moving images at the start are the same as the native images
  IMGMOV=$IMGNAT
  HFMOV=$HFNAT
  DBMOV=$DBNAT

  # The id in whose space the moving image is - at the beginning this is the 
  # same as the subject's ID
  IDMOV=$ID

  # Loop. The first element of GPATH is the image itself
  for ((j=1;j<${#GPATH[*]};++j)); do

    # The next image in the path
    idxref=${GPATH[j]}
    IDREF=$(cat $ROOT/scripts/subj.txt | head -n $idxref | tail -n 1)

    # The target images
    read IMGREF HFREF DBREF <<<$(fn_input_unwarped_images $IDREF)

    # Create directory for this registration
    WORK=$WDIR/paths/${ID}/step_${j}
    mkdir -p $WORK

    # We already know the affine between these two images
    AFFINE_INIT=$WDIR/pairwise/ref_${IDREF}_mov_${IDMOV}/ref_${IDREF}_mov_${IDMOV}_affine.mat

    # The output warp
    AFFINE=$WORK/affine.mat
    WARP=$WORK/warp.nii.gz

    # Create links for the relevant images
    ln -sf $IMGMOV $WORK/img_moving.nii.gz
    ln -sf $HFMOV $WORK/hf_moving.nii.gz
    ln -sf $DBMOV $WORK/db_moving.nii.gz

    ln -sf $IMGREF $WORK/img_ref.nii.gz
    ln -sf $HFREF $WORK/hf_ref.nii.gz
    ln -sf $DBREF $WORK/db_ref.nii.gz

    ln -sf $AFFINE_INIT $WORK/affine_init.mat

    # Perform affine registration between the moving and fixed 
    $GREEDY_HOME/greedy -d 3 \
      -w 1000 -i $HFREF $HFMOV \
      -w 5000 -i $DBREF $DBMOV \
      -threads $NSLOTS -n 100x100 \
      -ia $AFFINE_INIT -a -o $AFFINE

    # Perform registration - use intensity along with boundary strength
    $GREEDY_HOME/greedy -d 3 \
      -w 1000 -i $HFREF $HFMOV \
      -w 5000 -i $DBREF $DBMOV \
      -w 0.00006 -i $IMGREF $IMGMOV \
      -threads $NSLOTS -n 100x50x40x20 \
      -s 0.6mm 0.1mm -e 0.5 -float \
      -it $AFFINE \
      -o $WARP

    # Update the warp chain and the reslices
    WARPCHAIN="$WARP $AFFINE $WARPCHAIN"
    RESLICE_PTRN=$WORK/reslice_${ID}_to_${IDREF}_XXX.nii.gz
    IMGMOV=$WORK/reslice_${ID}_to_${IDREF}_img.nii.gz
    HFMOV=$WORK/reslice_${ID}_to_${IDREF}_hf.nii.gz
    DBMOV=$WORK/reslice_${ID}_to_${IDREF}_db.nii.gz
    IDMOV=${IDREF}

    # Apply the deformations to the data - going all the way to the raw IMG and SEG
    reslice_subj $ID $HFREF $RESLICE_PTRN $WARPCHAIN

  done

  # In the final stage, we prepend the warpchain by the rigid trasform to the 
  # old atlas reference space, so that the atlases live in similar space and 
  # so that the orientation of the hippocampus is reasonable
  WORK=$WDIR/paths/${ID}/final

  # The final reference space comes from the old hippocampus atlas
  WARPCHAIN="$WDIR/root_to_oldatlas/root_to_atlas_rigid.mat $WARPCHAIN"
  RESLICE_PTRN=$WORK/reslice_${ID}_to_${IDREF}_XXX.nii.gz

  # Create links to the "final" moving images
  mkdir -p $WORK
  reslice_subj $ID $OLD_ATLAS_HF $RESLICE_PTRN $WARPCHAIN

  # Save the warpchain to a text file for easy reuse
  echo $WARPCHAIN > $WORK/chain_unwarp_to_final.txt
}

function fn_print_adjacency()
{
  # Create the adjacency matrix
  N=$(cat $ROOT/scripts/subj.txt | wc -l)
  for ((i=1;i<=$N;i++)); do
    IDi=$(cat $ROOT/scripts/subj.txt | head -n $i | tail -n 1)
    for ((j=1;j<=$N;j++)); do
      if [[ $i -ne $j ]]; then
        IDj=$(cat $ROOT/scripts/subj.txt | head -n $j | tail -n 1)
        TAG="ref_${IDi}_mov_${IDj}"
        if [[ -f $WDIR/pairwise/$TAG/${TAG}_adj.txt ]]; then
          echo $i $j $(cat $WDIR/pairwise/$TAG/${TAG}_adj.txt)
        fi
      fi
    done
  done > $WDIR/adj.txt

  # Use R to compute paths
  Rscript $ROOT/scripts/compute_mst.R $WDIR/adj.txt > $WDIR/paths.txt


}

function fn_all_pairwise()
{
  # Launch all jobs
  for ID1 in $IDS; do
    for ID2 in $IDS; do
      if [[ $ID1 != $ID2 ]]; then
        qsubp2 -cwd -o $WDIR/dump -j y -N "pw_${ID1}_${ID2}" \
          $0 fn_pairwise_rough $ID1 $ID2
      fi
    done
  done

  # Wait for completion
  qsub -cwd -o $WDIR/dump -j y -hold_jid "pw_*" -sync y -b y sleep 1
}

function fn_all_paths()
{
  func=${1?}

  # Launch all jobs
  N=$(cat $ROOT/scripts/subj.txt | wc -l)
  for ((i=1;i<=$N;i++)); do
    qsubp4 -cwd -o $WDIR/dump -j y -N "path_$i" \
          $0 $func $i
  done


  # Wait for completion
  qsub -cwd -o $WDIR/dump -j y -hold_jid "path_*" -sync y -b y sleep 1
}

function fn_paths_average()
{
  # Create the mean image and mean DB images
  mkdir -p $WDIR/template_mst
  for what in img db hf; do
    c3d $WDIR/paths/*/final/reslice_*${what}.nii.gz \
      -mean -o $WDIR/template_mst/template_mst_${what}.nii.gz
  done
}

function fn_paths_shooting_average()
{
  # Create the mean image and mean DB images
  mkdir -p $WDIR/template_gshoot
  for what in img db hf; do
    c3d $WDIR/gshoot/*/iter_$((GSHOOT_NITER-1))/reslice_*${what}.nii.gz \
      -mean -o $WDIR/template_gshoot/template_gshoot_${what}.nii.gz
  done
}

function fn_pdirect_average()
{
  # Create the mean image and mean DB images
  mkdir -p $WDIR/template_direct
  for what in img db hf; do
    c3d $WDIR/pdirect/*/final/reslice_*${what}.nii.gz \
      -mean -o $WDIR/template_direct/template_direct_${what}.nii.gz
  done
}

function fn_pair_paths_overlap()
{
  ID1=${1?}
  ID2=${2?}

  WORK=$WDIR/template_mst/overlap/pairs
  mkdir -p $WORK

  # Compute Dice
  for what in db hf; do
    $C3D_HOME/c3d \
      $WDIR/paths/${ID1}/final/*_${what}*.nii.gz \
      $WDIR/paths/${ID2}/final/*_${what}*.nii.gz \
      -overlap 1 > $WORK/overlap_${what}_${ID1}_${ID2}.txt
  done

  # Compute NCC
  c3d \
    $WDIR/paths/${ID1}/final/*_img*.nii.gz \
    $WDIR/paths/${ID2}/final/*_img*.nii.gz \
    -ncc 4x4x4 -replace -NaN 0 NaN 0 -Inf 0 Inf 0 \
    $WDIR/template_mst/template_mst_hf.nii.gz -thresh 0.5 inf 1 0 \
    -lstat | awk 'NR==3 {print $2}' \
    > $WORK/ncc_${ID1}_${ID2}.txt
  
}

function fn_pair_gshoot_overlap()
{
  ID1=${1?}
  ID2=${2?}

  ITERDIR=iter_$((GSHOOT_NITER-1))

  WORK=$WDIR/template_gshoot/overlap/pairs
  mkdir -p $WORK

  # Compute Dice
  for what in db hf; do
    $C3D_HOME/c3d \
      $WDIR/gshoot/${ID1}/$ITERDIR/*_${what}*.nii.gz \
      $WDIR/gshoot/${ID2}/$ITERDIR/*_${what}*.nii.gz \
      -overlap 1 > $WORK/overlap_${what}_${ID1}_${ID2}.txt
  done

  # Compute NCC
  c3d \
    $WDIR/gshoot/${ID1}/$ITERDIR/*_img*.nii.gz \
    $WDIR/gshoot/${ID2}/$ITERDIR/*_img*.nii.gz \
    -ncc 4x4x4 -replace -NaN 0 NaN 0 -Inf 0 Inf 0 \
    $WDIR/template_gshoot/template_gshoot_hf.nii.gz -thresh 0.5 inf 1 0 \
    -lstat | awk 'NR==3 {print $2}' \
    > $WORK/ncc_${ID1}_${ID2}.txt
}

function fn_pair_pdirect_overlap()
{
  ID1=${1?}
  ID2=${2?}

  WORK=$WDIR/template_direct/overlap/pairs
  mkdir -p $WORK

  # Compute Dice
  for what in db hf; do
    $C3D_HOME/c3d \
      $WDIR/pdirect/${ID1}/final/*_${what}*.nii.gz \
      $WDIR/pdirect/${ID2}/final/*_${what}*.nii.gz \
      -overlap 1 > $WORK/overlap_${what}_${ID1}_${ID2}.txt
  done

  # Compute NCC
  c3d \
    $WDIR/pdirect/${ID1}/final/*_img*.nii.gz \
    $WDIR/pdirect/${ID2}/final/*_img*.nii.gz \
    -ncc 4x4x4 -replace -NaN 0 NaN 0 -Inf 0 Inf 0 \
    $WDIR/template_direct/template_direct_hf.nii.gz -thresh 0.5 inf 1 0 \
    -lstat | awk 'NR==3 {print $2}' \
    > $WORK/ncc_${ID1}_${ID2}.txt
  
}

function fn_all_pair_paths_overlap()
{
  func=${1?}

  N=$(cat $ROOT/scripts/subj.txt | wc -l)
  for ((i=1;i<=$N;i++)); do
    IDi=$(cat $ROOT/scripts/subj.txt | head -n $i | tail -n 1)
    for ((j=$((i+1));j<=$N;j++)); do
      if [[ $i -ne $j ]]; then
        IDj=$(cat $ROOT/scripts/subj.txt | head -n $j | tail -n 1)
        qsub -cwd -o $WDIR/dump -j y -N "ovl_${IDi}_${IDj}" \
          $0 $func $IDi $IDj
      fi
    done
  done

  # Wait for completion
  qsub -cwd -o $WDIR/dump -j y -hold_jid "ovl_*" -sync y -b y sleep 1
}

# use geodesic shooting to "fix up" correspondences - this is done iteratively
# as the template landmarks are updated towards the mean shape
function fn_shooting_correction_subj_iter()
{
  # The index of the subject being registered using the MST
  idx=${1?}

  # The iteration - 0 is the initial iteration, involves extra work 
  iter=${2?}

  # The path to the landmarks 
  LANDMARKS=${3?}

  # Which mode are we running shooting in? 
  # MST - after building the initial MST
  # NCC - after building the image-based template
  MODE=${4?}

  # The ID of the subject
  ID=$(cat $ROOT/scripts/subj.txt | head -n $idx | tail -n 1)

  # Output directory - depends on the mode
  if [[ $MODE == 'MST' ]]; then
    WORK=$WDIR/gshoot/${ID}
  elif [[ $MODE == 'NCC' ]]; then
    WORK=$WDIR/gshoot_ncc/${ID}
  fi

  WITER=$WORK/iter_${iter}
  mkdir -p $WITER $WITER/movie

  # The native space images and registrations
  read IMGNAT HFNAT DBNAT <<<$(fn_input_unwarped_images $ID)

  # Reference space (root node in cm-rep space)
  REFSPACE=$WORK/refspace.nii.gz

  # Result meshes
  TARGET=$WORK/shooting_target_native.vtk
  LM_PROCRUSTES=$WITER/shooting_target_procrustes.vtk
  LM_PROCRUSTES_MAT=$WITER/target_to_root_procrustes.mat
  SHOOTING_WARP=$WITER/shooting_warp.nii.gz

  # Output pattern
  RESLICE_PTRN=$WITER/reslice_${ID}_shooting_to_template_XXX.nii.gz

  # Mesh containing the momenta
  MOMENTA=$WITER/shooting_momenta.vtk

  # Target-related stuff in the WORK directory that is only done in the first iter
  if [[ $iter -eq 0 ]]; then

    # Create refspace - the funny wildcard here makes sure that the reference
    # space is also picked up for the root subject in the MST
    REFSPACE_SRC=$(ls $WDIR/paths/${ID}/final/*_hf*.nii.gz)
    $C3D_HOME/c3d $REFSPACE_SRC -pad 0x20x20 0x20x20 0 -o $REFSPACE

    # Compute the target landmarks for this subject
    if [[ $MODE == 'MST' ]]; then

      # Get the warp chain from file
      WARPCHAIN=$(cat $WDIR/paths/${ID}/final/chain_unwarp_to_final.txt)

      # Apply the warp chain to the landmark mesh in template space, creating
      # the target locations for the geodesic shooting registration
      # -- this code works when the WARPCHAIN is empty (MST root)
      $GREEDY_HOME/greedy -d 3 \
        -rf $REFSPACE \
        -rs $LANDMARKS $TARGET \
        -r $WARPCHAIN

    elif [[ $MODE == 'NCC' ]]; then

      # The output of the segmentation-guided shooting iteration
      GSDIR=$WDIR/gshoot/${ID}/iter_$((GSHOOT_NITER-1))

      # The final NCC warp
      NCCWARP=$WDIR/template_ncc/iter$((NCC_NITER-1))/warp_${ID}.nii.gz

      # Build up the warp chain - all the 'shape' warps
      WARPCHAIN="$NCCWARP $GSDIR/shooting_warp.nii.gz $GSDIR/target_to_root_procrustes.mat,-1"

      # Apply the warp chain to the landmark mesh in template space, creating
      # the target locations for the geodesic shooting registration
      $GREEDY_HOME/greedy -d 3 \
        -rf $REFSPACE \
        -rs $LANDMARKS $TARGET \
        -r $WARPCHAIN

    fi

  fi

  # Landmarks in reference space
  ln -sf $LANDMARKS $WITER/landmarks.vtk

  # Bring the target mesh back near the root mesh using procrustes alignment
  vtkprocrustes $TARGET $LANDMARKS $LM_PROCRUSTES_MAT

  # Apply procrustes to the landmarks. Warp mesh is more reliable than greedy around
  # image edges, so we use it here. We had a situation in 65240-R where some landmarks
  # that were close to the image border were mapped badly (to a far-off location) which
  # screwed up the geodesic (and might have impacted the rest of the template too...
  warpmesh $TARGET $LM_PROCRUSTES $LM_PROCRUSTES_MAT

  # Offending code!
  ##-- $GREEDY_HOME/greedy -d 3 \
  ##--   -rf $HFNAT \
  ##--   -rs $TARGET $LM_PROCRUSTES \
  ##--   -r $LM_PROCRUSTES_MAT

  # Perform geodesic shooting between the procrustes landmarks and the
  # warped landmarks - this is going to allow us to interpolate the correspondence
  # found by the MST to the rest of the images
  if [[ ! -f $MOMENTA || ! -f $SHOOTING_WARP ]]; then 

    # Let's try a GPU version!
    ### time qsub -j y -o $WDIR/dump -sync y -b y -cwd -V -N "lmshoot_$ID" -q gpu.q \
    ###   lmshoot_cuda -d 3 \
    ###     -m $LANDMARKS $LM_PROCRUSTES \
    ###     -o $MOMENTA \
    ###     -s 2.0 -l 5000 -n 40 -i 240 0 -f -O $WITER/movie/movie%04d.vtk

    time lmshoot -d 3 \
      -m $LANDMARKS $LM_PROCRUSTES \
      -o $MOMENTA \
      -s 2.0 -l 5000 -n 40 -i 240 0 -f -O $WITER/movie/movie%04d.vtk

    # Convert the shooting result into a warp
    lmtowarp -d 3 -n 40 -r $REFSPACE \
      -m $MOMENTA -o $SHOOTING_WARP \
      -s 2.0

  fi

  # Warp the native space image into the template
  reslice_subj $ID $REFSPACE $RESLICE_PTRN $SHOOTING_WARP $LM_PROCRUSTES_MAT,-1
}

# Peform shooting-based shape averaging
function fn_shooting_shape_avg()
{
  # The iteration of the loop
  iter=${1?}

  # The source landmarks
  SRC_LANDMARKS=${2?}

  # The mode - NCC or MST
  MODE=${3?}

  # The work dir
  if [[ $MODE == 'MST' ]]; then
    SHOOT_DIR=$WDIR/gshoot
  elif [[ $MODE == 'NCC' ]]; then
    SHOOT_DIR=$WDIR/gshoot_ncc
  fi

  WORK=$SHOOT_DIR/shape_avg/iter_${iter}

  # The reference space for shooting
  REFSPACE=$WDIR/template_mst/template_mst_hf.nii.gz

  # The result landmarks - after shape averating
  SHAVG_LANDMARKS_NOPROC=$WORK/shavg_landmarks_noprocrustes.vtk
  SHAVG_LANDMARKS=$WORK/shavg_landmarks.vtk

  # Create the work dir
  mkdir -p $WORK
  
  # Average the momentum maps from the previous iteration
  avgmesharr \
    $SHOOT_DIR/*/iter_${iter}/shooting_momenta.vtk \
    InitialMomentum $SRC_LANDMARKS $WORK/average_momenta.vtk

  # Perform the shooting and generate warp
  lmtowarp \
    -d 3 -n 40 -r $REFSPACE \
    -m $WORK/average_momenta.vtk \
    -o $WORK/average_momenta_warp.nii.gz -s 2.0

  # Apply the warp to the landmarks to bring them to shape-averaging position
  $GREEDY_HOME/greedy \
    -d 3 -rs $SRC_LANDMARKS $SHAVG_LANDMARKS_NOPROC \
    -rf $REFSPACE -r $WORK/average_momenta_warp.nii.gz

  # This transformation of the landmarks can cause shrinkage of the template. This 
  # is not at all what we want in the template, we actually want the template to 
  # keep its size during this iterative process. The way to correct this is to perform
  # procrustes between the source lanmarks and the new shape average
  vtkprocrustes $SRC_LANDMARKS $SHAVG_LANDMARKS_NOPROC $WORK/residual_procrustes.mat \
    | grep RMS_ | tee $WORK/procrustes_metric.txt

  # Applying the inverse of this procrustes to the SHAVG_LANDMARKS_NOPROC gives the new
  # template landmarks which are shape averaged but still the same size as the original
  # template. 
  $GREEDY_HOME/greedy \
    -d 3 -rs $SRC_LANDMARKS $SHAVG_LANDMARKS \
    -rf $REFSPACE \
    -r $WORK/average_momenta_warp.nii.gz $WORK/residual_procrustes.mat,-1
}

function fn_shooting_init_landmarks()
{
  # Determine what is the input
  MODE=${1?}
  if [[ $MODE == 'MST' ]]; then
    IMG_HF=$WDIR/template_mst/template_mst_hf.nii.gz
    IMG_DB=$WDIR/template_mst/template_mst_db.nii.gz
    WORK=$WDIR/gshoot/template
  elif [[ $MODE == 'NCC' ]]; then
    IMG_HF=$(ls $WDIR/template_ncc/iter*/template_hf.nii.gz | tail -n 1)
    IMG_DB=$(ls $WDIR/template_ncc/iter*/template_db.nii.gz | tail -n 1)
    WORK=$WDIR/gshoot_ncc/template
  fi

  # Samples
  SAMHF=$WORK/work/samples_hf.ply
  SAMDB=$WORK/work/samples_db.ply
  TEMPLATE=$WORK/root_landmarks.vtk

  # Create output directories
  mkdir -p $WORK/work

  # Extract the level set surfaces
  vtklevelset $IMG_HF $WORK/work/mesh_hf.stl 0.5
  vtklevelset $IMG_DB $WORK/work/mesh_db.stl 0.5

  # Perform point sampling
  mesh_poisson_sample $WORK/work/mesh_hf.stl $SAMHF 500

  mesh_poisson_sample $WORK/work/mesh_db.stl $SAMDB 500

  # Combine the two PLY objects into a VTK mesh
  NVDB=$(cat $SAMDB | grep 'element vertex' | awk '{print $3}')
  NVHF=$(cat $SAMHF | grep 'element vertex' | awk '{print $3}')
  NV=$((NVDB + NVHF))

  # Write the header of the VTK file
  echo "# vtk DataFile Version 4.0" > $TEMPLATE
  echo "vtk output" >> $TEMPLATE
  echo "ASCII" >> $TEMPLATE
  echo "DATASET POLYDATA" >> $TEMPLATE
  echo "POINTS ${NV} float" >> $TEMPLATE

  # Write the point coordinates
  grep -A $NVHF end_header $SAMHF | tail -n $NVHF >> $TEMPLATE
  grep -A $NVDB end_header $SAMDB | tail -n $NVDB >> $TEMPLATE
}

function fn_shooting_correction_iterative()
{
  MODE=${1?}

  START_ITER=0

  # The shooting dir
  if [[ $MODE == 'MST' ]]; then
    SHOOT_DIR=$WDIR/gshoot
    END_ITER=$GSHOOT_NITER
  elif [[ $MODE == 'NCC' ]]; then
    SHOOT_DIR=$WDIR/gshoot_ncc
    END_ITER=$GSHOOT_NITER_NCC
  fi

  # Initial landmarks - sampled from template segmentation
  INIT_LANDMARKS=$SHOOT_DIR/template/root_landmarks.vtk

  # Loop
  for ((iter=$START_ITER;iter<$END_ITER;iter++)); do

    # Path to the landmarks at this iteration
    if [[ $iter -eq 0 ]]; then

      # Compute the initial landmarks
      qsub -cwd -o $WDIR/dump -j y -sync y -N "shootinit" \
            $0 fn_shooting_init_landmarks $MODE

      LANDMARKS=$INIT_LANDMARKS

    else

      LANDMARKS=$SHOOT_DIR/shape_avg/iter_$((iter-1))/shavg_landmarks.vtk

    fi

    # Launch all jobs
    N=$(cat $ROOT/scripts/subj.txt | wc -l)
    for ((i=1;i<=$N;i++)); do
      qsub -cwd -o $WDIR/dump -j y -N "shoot_$i" \
            $0 fn_shooting_correction_subj_iter $i $iter $LANDMARKS $MODE
    done

    # Wait for completion
    qsub -cwd -o $WDIR/dump -j y -hold_jid "shoot_*" -sync y -b y sleep 1

    # Launch the averaging job
    qsubp4 -cwd -o $WDIR/dump -j y -N "avshoot_$i" \
          $0 fn_shooting_shape_avg $iter $LANDMARKS $MODE

    # Wait for completion
    qsub -cwd -o $WDIR/dump -j y -hold_jid "avshoot_*" -sync y -b y sleep 1

  done
}

function fn_shooting_correction_all_paths()
{
  # Launch all jobs
  N=$(cat $ROOT/scripts/subj.txt | wc -l)
  for ((i=1;i<=$N;i++)); do
    qsub -cwd -o $WDIR/dump -j y -N "shoot_$i" \
          $0 fn_shooting_correction_subj $i
  done

  # Wait for completion
  qsub -cwd -o $WDIR/dump -j y -hold_jid "shoot_*" -sync y -b y sleep 1
}

# average images from iteration iter
function fn_template_mst_ncc_average()
{
  # Iteration
	iter=$1 

  # Modality
  mod=$2

  # Directory where to average
  WORK=$WDIR/template_ncc/iter${iter}
  mkdir -p $WORK

  # List images to average
  if [[ $iter -eq 0 ]]; then

    # The inputs are in the paths directories
    INPUTS=$(ls $WDIR/gshoot/*/iter_$((GSHOOT_NITER-1))/reslice*${mod}*.nii.gz)

  else

    # The inputs are outputs from last iteration
    INPUTS=$(ls $WDIR/template_ncc/iter$((iter-1))/reslice*${mod}*.nii.gz)

  fi

  # Type of averaging to perform
  AVGMODE=0
  # This seems to make things worse
  if [[ $mod == "img" ]]; then AVGMODE=1; fi

  # Do the averaging
  ${ANTSDIR}/AverageImages 3 $WORK/template_${mod}.nii.gz $AVGMODE ${INPUTS}

  # Scale template by 100 - why?
  if [[ $mod == "img" ]]; then 
    c3d $WORK/template_${mod}.nii.gz -scale 100 -o $WORK/template_${mod}.nii.gz
  fi

  # Compute the mask
  if [[ $mod == "hf" ]]; then
    c3d $WORK/template_hf.nii.gz -thresh 0.5 inf 1 0 -dilate 1 10x10x10vox -o $WORK/template_mask.nii.gz
  fi
}

# Determine if the subject with given index needs flipping when doing procrustes 
# alignment with the MST root
function fn_flip_to_root()
{
  local idx=${1?}
  local ID=$(cat $ROOT/scripts/subj.txt | head -n $idx | tail -n 1)
  local SIDE=$(echo $ID | awk -F '-' '{print $NF}')
  local flip=""

  if [[ $SIDE != $OLD_ATLAS_SIDE ]]; then flip="-f"; fi

  echo $flip
}

# Prepare for affine-then-intensity template population build
function fn_prepare_affine_template_ncc()
{
  idx=${1?}

  # The ID of the subject
  ID=$(cat $ROOT/scripts/subj.txt | head -n $idx | tail -n 1)

  # The native space images and registrations
  read IMG_NAT HF_NAT DB_NAT <<<$(fn_input_unwarped_images $ID)

  # The iteration path for gshoot
  GSDIR=$WDIR/gshoot/${ID}/iter_$((GSHOOT_NITER-1))

  # The fixed and moving landmarks for gshoot
  GSL_FIXED=$GSDIR/landmarks.vtk
  GSL_MOVING=$GSDIR/../shooting_target_native.vtk

  # Do we need a flip?
  flip=$(fn_flip_to_root $idx)

  # Directory for the work
  WORK=$WDIR/template_aff_ncc/input/${ID}

  # The affine matrix
  AFFPROC=$WORK/proc_affine_${ID}.mat

  # The reference image where to do registrations
  REFSPACE=$WDIR/template_gshoot/template_gshoot_hf.nii.gz

  mkdir -p $WORK

  # Extract the affine component of the geodesic shooting
  vtkprocrustes $flip $GSL_FIXED $GSL_MOVING $AFFPROC

  # Apply transformation to the images
  reslice_subj $ID $REFSPACE $WORK/reslice_to_template_${ID}_XXX.nii.gz $AFFPROC
}


function fn_prepare_affine_template_ncc_all()
{
  # Launch all jobs
  N=$(cat $ROOT/scripts/subj.txt | wc -l)
  for ((i=1;i<=$N;i++)); do
    qsubp4 -cwd -o $WDIR/dump -j y -N "affprep_$i" \
          $0 fn_prepare_affine_template_ncc $i
  done

  # Wait for completion
  qsub -cwd -o $WDIR/dump -j y -hold_jid "affprep_*" -sync y -b y sleep 1
}

# average images from iteration iter
function fn_template_ncc_average()
{
  # Iteration
	iter=${1?} 

  # Modality
  mod=${2?}

  # Which template are we building (ncc or aff_ncc)
  mode=${3?}

  # Directory where to average
  WORK=$WDIR/template_${mode}/iter${iter}
  mkdir -p $WORK

  # List images to average
  if [[ $iter -eq 0 ]]; then

    if [[ $mode == "ncc" ]]; then
      # The inputs are in the paths directories
      INPUTS=$(ls $WDIR/gshoot/*/iter_$((GSHOOT_NITER-1))/reslice*${mod}*.nii.gz)
    elif [[ $mode == "aff_ncc" ]]; then
      INPUTS=$(ls $WDIR/template_${mode}/input/*/reslice*${mod}*.nii.gz)
    fi

  else

    # The inputs are outputs from last iteration
    INPUTS=$(ls $WDIR/template_${mode}/iter$((iter-1))/reslice*${mod}*.nii.gz)

  fi

  # Type of averaging to perform
  AVGMODE=0
  # This seems to make things worse
  if [[ $mod == "img" ]]; then AVGMODE=1; fi

  # Do the averaging
  ${ANTSDIR}/AverageImages 3 $WORK/template_${mod}.nii.gz $AVGMODE ${INPUTS}

  # Scale template by 100 - why?
  if [[ $mod == "img" ]]; then 
    c3d $WORK/template_${mod}.nii.gz -scale 100 -o $WORK/template_${mod}.nii.gz
  fi

  # Compute the mask
  if [[ $mod == "hf" ]]; then
    c3d $WORK/template_hf.nii.gz -thresh 0.5 inf 1 0 -dilate 1 10x10x10vox -o $WORK/template_mask.nii.gz
  fi
}


# This is the registration part of the groupwise intensity registration script. 
# It can be run on full MST/GS output or on the affine part of it, the latter 
# being a simulation of what conventional template building would produce on
# this dataset
function fn_template_ncc_register()
{
  # The index of the subject being registered using the MST
  idx=${1?}
  iter=${2?}

  # The mode for this function (ncc or aff_ncc)
  mode=${3?}

  # The work directory
  WORK=$WDIR/template_${mode}/iter${iter}

  # The ID of the subject
  ID=$(cat $ROOT/scripts/subj.txt | head -n $idx | tail -n 1)

  # The native space images and registrations
  read IMG_NAT HF_NAT DB_NAT <<<$(fn_input_unwarped_images $ID)

  # The iteration path for gshoot
  GSDIR=$WDIR/gshoot/${ID}/iter_$((GSHOOT_NITER-1))

  # Build up the warp chain - all the 'shape' warps
  if [[ $mode == "ncc" ]]; then
    WARPCHAIN="$GSDIR/shooting_warp.nii.gz $GSDIR/target_to_root_procrustes.mat,-1"
    IMGMOV=$(ls $GSDIR/reslice_*img.nii.gz)
  elif [[ $mode == "aff_ncc" ]]; then
    WARPCHAIN="$WDIR/template_${mode}/input/$ID/proc_affine_${ID}.mat"
    IMGMOV=$(ls $WDIR/template_${mode}/input/$ID/reslice_*img.nii.gz)
  fi

  # Template
  TEMPLATE_IMG=$WORK/template_img.nii.gz

  # Pattern for reslicing
  RESLICE_PTRN=$WORK/reslice_to_template_${ID}_XXX.nii.gz

  # Create the working directory
  mkdir -p $WORK

  # Symlink the moving image in this directory
  ln -sf $IMGMOV $WORK/moving_${ID}_img.nii.gz

  # The output warp
  WARP=$WORK/warp_${ID}.nii.gz

  # Perform registration with NCC
  $GREEDY_HOME/greedy -d 3 \
    -w 1 -m NCC 2x2x2 -i $TEMPLATE_IMG $IMGMOV \
    -gm $WORK/template_mask.nii.gz \
    -n 100x100x50 -s 0.6mm 0.2mm -e 0.5 \
    -threads $NSLOTS \
    -o $WARP

  # Apply registration at this iteration
  reslice_subj $ID $TEMPLATE_IMG $RESLICE_PTRN $WARP $WARPCHAIN
}

function fn_template_ncc_loop
{
  # Which template are we building (ncc or aff_ncc)
  mode=${1?}

  # List of subjects
  N=$(cat $ROOT/scripts/subj.txt | wc -l)

  # Iterations - number, starting
  SITER=0

  # Perform the loop
	for ((iter=$SITER; iter<${NCC_NITER}; iter++));
	do
    
    # Create a working directory for this iteration
    WORK=$WDIR/template_${mode}/iter${iter}
    mkdir -p $WORK/dump

    # Compute current average for each modality
    for mod in img db hf; do
      qsubp4 -cwd -o $WORK/dump -j y -N "average_${iter}_${mod}" \
        $0 fn_template_ncc_average $iter $mod $mode
    done

		# Wait for completion
		qsub -cwd -o $WORK/dump -j y -hold_jid "average_${stage}*" -sync y -b y sleep 1

		# register all images to the average and place in new folder for iter+1:
    for ((i=1;i<=$N;i++)); do

			# submit ANTS job:
      qsubp2 -cwd -o $WORK/dump -j y -N "norm_${iter}_${i}" \
        $0 fn_template_ncc_register ${i} ${iter} ${mode}
		
    done
		
		# Wait for completion
		qsub -cwd -o $WORK/dump -j y -hold_jid "norm_${stage}*" -sync y -b y sleep 1

	done
}


# Final geodesic shooting - in order to perform shape PCA and what not
# I think that shape averaging at this stage is unnecessary, but the code
# allows for it
function fn_shooting_final_shape_stats()
{
  # The index of the subject being registered using the MST
  idx=${1?}

  iter=${2?}

  # The path to the landmarks 
  LANDMARKS=${3?}

  # The ID of the subject
  ID=$(cat $ROOT/scripts/subj.txt | head -n $idx | tail -n 1)

  # Output directory
  WORK=$WDIR/shapestats/${ID}
  WITER=$WORK/iter_${iter}
  mkdir -p $WITER $WITER/movie

  # The native space images and registrations
  read IMGNAT HFNAT DBNAT <<<$(fn_input_unwarped_images $ID)

  # Reference space (root node in cm-rep space)
  REFSPACE=$WORK/refspace.nii.gz

  # Result meshes
  TARGET=$WORK/shooting_target_native.vtk
  LM_PROCRUSTES=$WITER/shooting_target_procrustes.vtk
  LM_PROCRUSTES_MAT=$WITER/target_to_root_procrustes.mat
  SHOOTING_WARP=$WITER/shooting_warp.nii.gz
  RESLICE_PTRN=$WITER/reslice_${ID}_shooting_to_template_XXX.nii.gz

  # Mesh containing the momenta
  MOMENTA=$WITER/shooting_momenta.vtk

  # Do we need a flip?
  flip=$(fn_flip_to_root $idx)

  # Target-related stuff in the WORK directory that is only done in the first iter
  if [[ $iter -eq 0 ]]; then

    # The output of the segmentation-guided shooting iteration
    GSDIR=$WDIR/gshoot/${ID}/iter_$((GSHOOT_NITER-1))

    # The final NCC warp
    NCCWARP=$WDIR/template_ncc/iter_$((NCC_NITER-1))/warp_${ID}.nii.gz

    # Build up the warp chain - all the 'shape' warps
    WARPCHAIN="$NCCWARP $GSDIR/shooting_warp.nii.gz $GSDIR/target_to_root_procrustes.mat,-1"

    # Create refspace
    REFSPACE_SRC=$(ls $WDIR/paths/${ID}/final/*hf.nii.gz)
    $C3D_HOME/c3d $REFSPACE_SRC -pad 0x20x20 0x20x20 0 -o $REFSPACE

    # Apply the warp chain to the landmark mesh in template space, creating
    # the target locations for the geodesic shooting registration
    $GREEDY_HOME/greedy -d 3 \
      -rf $REFSPACE \
      -rs $LANDMARKS $TARGET \
      -r $WARPCHAIN

  fi

  # Landmarks in reference space
  ln -sf $LANDMARKS $WITER/landmarks.vtk

  # Bring the target mesh back near the root mesh using procrustes alignment
  vtkprocrustes $flip $TARGET $LANDMARKS $LM_PROCRUSTES_MAT

  # Apply procrustes to the landmarks
  $GREEDY_HOME/greedy -d 3 \
    -rf $HFNAT \
    -rs $TARGET $LM_PROCRUSTES \
    -r $LM_PROCRUSTES_MAT

  # Perform geodesic shooting between the procrustes landmarks and the
  # warped landmarks - this is going to allow us to interpolate the correspondence
  # found by the MST to the rest of the images
  if [[ ! -f $MOMENTA || ! -f $SHOOTING_WARP ]]; then 

    time lmshoot -d 3 \
      -m $LANDMARKS $LM_PROCRUSTES \
      -o $MOMENTA \
      -s 2.0 -l 5000 -n 40 -i 240 0 -f -O $WITER/movie/movie%04d.vtk

    # Convert the shooting result into a warp
    lmtowarp -d 3 -n 40 -r $REFSPACE \
      -m $MOMENTA -o $SHOOTING_WARP \
      -s 2.0

  fi

  # Warp the native space image into the template
  reslice_subj $ID $REFSPACE $RESLICE_PTRN $SHOOTING_WARP $LM_PROCRUSTES_MAT,-1
}

function fn_template_ncc_pair_overlap()
{
  ID1=${1?}
  ID2=${2?}
  iter=${3?}
  mode=${4?}

  WORK=$WDIR/template_${mode}/iter${iter}

  mkdir -p $WORK/overlap/pairs

  # Compute Dice
  for what in db hf; do
    $C3D_HOME/c3d \
      $WORK/reslice_to_template_${ID1}_${what}.nii.gz \
      $WORK/reslice_to_template_${ID2}_${what}.nii.gz \
      -overlap 1 > $WORK/overlap/pairs/overlap_${what}_${ID1}_${ID2}.txt
  done

  # Compute NCC
  c3d \
    $WORK/reslice_to_template_${ID1}_img.nii.gz \
    $WORK/reslice_to_template_${ID2}_img.nii.gz \
    -ncc 4x4x4 -replace -NaN 0 NaN 0 -Inf 0 Inf 0 \
    $WORK/template_hf.nii.gz -thresh 0.5 inf 1 0 \
    -lstat | awk 'NR==3 {print $2}' \
    > $WORK/overlap/pairs/ncc_${ID1}_${ID2}.txt
  
}

function fn_template_ncc_all_pairs_overlap
{
  iter=${1?}
  mode=${2?}

  N=$(cat $ROOT/scripts/subj.txt | wc -l)
  for ((i=1;i<=$N;i++)); do
    IDi=$(cat $ROOT/scripts/subj.txt | head -n $i | tail -n 1)
    for ((j=$((i+1));j<=$N;j++)); do
      if [[ $i -ne $j ]]; then
        IDj=$(cat $ROOT/scripts/subj.txt | head -n $j | tail -n 1)
        qsub -cwd -o $WDIR/dump -j y -N "ovl_${IDi}_${IDj}" \
          $0 fn_template_ncc_pair_overlap $IDi $IDj $iter $mode
      fi
    done
  done

  # Wait for completion
  qsub -cwd -o $WDIR/dump -j y -hold_jid "ovl_*" -sync y -b y sleep 1
}

function summarize_template_stats()
{
  WORK=${1?}

  cat $WORK/overlap_hf_* | awk -F ',' '{print $5}' > $TMPDIR/hf.txt
  cat $WORK/overlap_db_* | awk -F ',' '{print $5}' > $TMPDIR/db.txt
  cat $WORK/ncc_*.txt > $TMPDIR/ncc.txt

  echo "OVL_HF OVL_DB NCC" > $WORK/pairwise_stats.txt
  paste $TMPDIR/hf.txt $TMPDIR/db.txt $TMPDIR/ncc.txt >> $WORK/pairwise_stats.txt
}

# Compute forward and backward transforms to raw space and unwarp space for
# each subject. This is done relative to the last iteration of geodesic shooting
# i.e., the best template that we have been able to build.
function finalize_subject()
{
  ID=${1?}

  # Subject's preproc directory
  PDIR=$ROOT/preproc/${ID}

  # Subject's unwarped-space data
  read IMGUW HFUW DBUW <<<$(fn_input_unwarped_images $ID)

  # Subject's RAW MRI scan
  RAW_IMG=$PDIR/${ID}_raw_n4clip.nii.gz

  # Subject's source segmentation of DB and HF
  HFSEG=$PDIR/${ID}_danseg_hfseg.nii.gz
  DBSEG=$PDIR/${ID}_danseg_dbseg.nii.gz

  # Subject's unwarp and dan/raw matrices
  UNWARP_MAT=$PDIR/unwarp/${ID}_unwarp.mat
  SEG_TO_RAW_MAT=$PDIR/${ID}_danseg_to_raw.mat

  # Subject's geodesic shooting transform from unwarp space to MST template space
  GSDIR=$WDIR/gshoot/${ID}/iter_$((GSHOOT_NITER-1))
  GSWARP=$GSDIR/shooting_warp.nii.gz
  GSPROC=$GSDIR/target_to_root_procrustes.mat

  # Subject's warp from shape-normalized space to NCC template
  NCCDIR=$WDIR/template_ncc/iter$((NCC_NITER-1))
  NCCWARP=$NCCDIR/warp_${ID}.nii.gz

  # Directory for the final output
  WORK=$WDIR/final/${ID}

  # Location of inverse warps - can be in TMPDIR
  NCCWARP_INV=$WORK/${ID}_ncc_invwarp.nii.gz
  GSWARP_INV=$WORK/${ID}_gshoot_invwarp.nii.gz
  
  # Warp chain from unwarped image space to template space
  CHAIN_UNWARPED_TO_TEMPLATE="$NCCWARP $GSWARP $GSPROC,-1"
  CHAIN_TEMPLATE_TO_UNWARPED="$GSPROC $GSWARP_INV $NCCWARP_INV"

  # Warp chain from raw image space to template space
  CHAIN_RAW_TO_TEMPLATE="$CHAIN_UNWARPED_TO_TEMPLATE $UNWARP_MAT"

  # Warp chain from input image space (where segmentation drawn) to template
  CHAIN_INPUT_TO_TEMPLATE="$CHAIN_RAW_TO_TEMPLATE $SEG_TO_RAW_MAT"

  # Make the directory
  mkdir -p $WORK

  # Link the forward warps and other important data for completeness
  ln -sf $NCCWARP $WORK/${ID}_ncc_warp.nii.gz
  ln -sf $GSWARP $WORK/${ID}_gshoot_warp.nii.gz
  ln -sf $IMGUW $WORK/${ID}_unwarp_img.nii.gz
  ln -sf $NCCDIR/template_img.nii.gz $WORK/template_ncc.nii.gz

  # Write the chains to file
  echo $CHAIN_UNWARPED_TO_TEMPLATE > $WORK/chain_unwarped_to_template.txt
  echo $CHAIN_TEMPLATE_TO_UNWARPED > $WORK/chain_template_to_unwarped.txt
  echo $CHAIN_RAW_TO_TEMPLATE > $WORK/chain_raw_to_template.txt
  echo $CHAIN_INPUT_TO_TEMPLATE > $WORK/chain_input_to_template.txt

  # Invert the geodesic shooting and NCC warps
  $GREEDY_HOME/greedy -d 3 \
    -iw $NCCWARP $NCCWARP_INV \
    -invexp 4 -wp 0.0001
  
  $GREEDY_HOME/greedy -d 3 \
    -iw $GSWARP $GSWARP_INV \
    -invexp 4 -wp 0.0001

  # Create the best possible resampled intensity image (one interpolation less)
  $GREEDY_HOME/greedy -d 3 \
    -rf $WORK/template_ncc.nii.gz \
    -rm $RAW_IMG $WORK/${ID}_reslice_rawimg_to_template.nii.gz \
    -r $CHAIN_RAW_TO_TEMPLATE

  # Create the best possible HF and DB masks
  $GREEDY_HOME/greedy -d 3 \
    -ri LABEL 0.24vox \
    -rf $WORK/template_ncc.nii.gz \
    -rm $HFSEG $WORK/${ID}_reslice_hfseg_to_template.nii.gz \
    -rm $DBSEG $WORK/${ID}_reslice_dbseg_to_template.nii.gz \
    -r $CHAIN_INPUT_TO_TEMPLATE
}

function finalize_all()
{
  for id in $(cat $ROOT/scripts/subj.txt); do
    qsubp2 -cwd -V -o $WDIR/dump -j y -N "final_${id}" \
      $0 finalize_subject $id
  done

  qsub -cwd -o $WDIR/dump -j y -hold_jid "final_*" -sync y -b y sleep 1
}

function template_screenshots()
{
  local mode=${1?}
  local WORK=$WDIR/final/templates/template_${mode}
  local CROPSPACE=$ROOT/scripts/manual/cropspace.nii.gz 

  mkdir -p $WORK/reslice $WORK/slices/template/

  # Build up the list of images to reslice - use links
  for id in $(cat $ROOT/scripts/subj.txt); do

    local RESLICE
    case "$mode" in 
      ncc)
        RESLICE=$WDIR/template_ncc/iter$((NCC_NITER-1))/reslice_to_template_${id}_img.nii.gz
        ;;
      aff_ncc)
        RESLICE=$WDIR/template_aff_ncc/iter$((NCC_NITER-1))/reslice_to_template_${id}_img.nii.gz
        ;;
      aff_only)
        RESLICE=$WDIR/template_aff_ncc/iter0/moving_${id}_img.nii.gz
        ;;
      mst)
        RESLICE=$(ls $WDIR/paths/${id}/final/reslice_${id}_*img.nii.gz)
        ;;
      gshoot)
        RESLICE=$(ls $WDIR/gshoot/${id}/iter_$((GSHOOT_NITER-1))/reslice*img.nii.gz)
        ;;
    esac

    ln -sf $RESLICE $WORK/reslice/reslice_${id}.nii.gz

  done

  # Create templates from the images by averaging
  ${ANTSDIR}/AverageImages 3 $WORK/template_${mode}.nii.gz 0 $WORK/reslice/*.nii.gz

  c3d \
    $CROPSPACE $WORK/template_${mode}.nii.gz \
    -reslice-identity -o $WORK/crop_template_${mode}.nii.gz

  # Generate slices for the template
  c3d \
    $WORK/crop_template_${mode}.nii.gz \
    -stretch 0 850 0 255 -clip 0 255 -popas X \
    -type uchar \
    -push X -slice x 189 -flip x -flip y -o $WORK/slices/coronal_head_${mode}_template.png \
    -push X -slice y 73  -flip x -flip y -o $WORK/slices/sagittal_${mode}_template.png \
    -push X -slice z 81  -flip x -flip y -o $WORK/slices/axial_${mode}_template.png

  # Generate slices for individual images
  for id in $(cat $ROOT/scripts/subj.txt); do

    c3d \
      $CROPSPACE $WORK/reslice/reslice_${id}.nii.gz -reslice-identity \
      -stretch 0 850 0 255 -clip 0 255 -popas X \
      -type uchar \
      -push X -slice x 189 -flip x -flip y -o $WORK/slices/coronal_head_${mode}_${id}.png \
      -push X -slice y 73  -flip x -flip y -o $WORK/slices/sagittal_${mode}_${id}.png \
      -push X -slice z 81  -flip x -flip y -o $WORK/slices/axial_${mode}_${id}.png

  done
}

function misc_final_stats()
{
  local TEMPLATE_PATHS="
    /data/picsl/pauly/dan2015/atlas2016/mst/template_mst/overlap/pairs \
    /data/picsl/pauly/dan2015/atlas2016/mst/template_gshoot/overlap/pairs \
    /data/picsl/pauly/dan2015/atlas2016/mst/template_ncc/iter5/overlap/pairs \
    /data/picsl/pauly/dan2015/atlas2016/mst/template_aff_ncc/iter5/overlap/pairs"

  for P in $TEMPLATE_PATHS; do
    summarize_template_stats $P
  done

  # Launch all jobs
  for mode in aff_ncc aff_only ncc mst gshoot; do
    qsubp2 -cwd -o $WDIR/dump -j y -N "slice_${mode}" \
          $0 template_screenshots $mode
  done

  # Wait for completion
  qsub -cwd -o $WDIR/dump -j y -hold_jid "slice_*" -sync y -b y sleep 1
}

function main()
{
  mkdir -p $WDIR/dump

  # Compute the MST
  ### fn_all_pairwise
  ### fn_print_adjacency
  
  # Register MST root to old reference atlas so we 
  ### qsubp4 -cwd -o $WDIR/dump -j y -N "root_to_oldatlas" -sync y $0 fn_register_root_to_oldatlas

  # Perform registration along MST paths and compute template and template-space overlaps
  ### fn_all_paths fn_register_mst
  ### qsubp4 -cwd -o $WDIR/dump -j y -N "avg_path" -sync y $0 fn_paths_average
  ### fn_all_pair_paths_overlap fn_pair_paths_overlap

  # Apply the geodesic shooting correction and reevaluate the overlaps and such
  ### fn_shooting_correction_iterative MST
  ### qsubp4 -cwd -o $WDIR/dump -j y -N "avg_path" -sync y $0 fn_paths_shooting_average
  ### fn_all_pair_paths_overlap fn_pair_gshoot_overlap

  # Compute alternative registrations using the first and last point only 
  # -- this is to show that MST adds value -- and it seems to quite a bit!
  ### fn_all_paths fn_register_direct
  ### qsubp4 -cwd -o $WDIR/dump -j y -N "avg_path" -sync y $0 fn_pdirect_average
  ### fn_all_pair_paths_overlap fn_pair_pdirect_overlap

  # Build a template in intensity space - consecutive averaging and registration
  ### fn_template_ncc_loop ncc
  ### fn_template_ncc_all_pairs_overlap $((NCC_NITER-1)) ncc

  # Build a comparison template using intensity groupwise registration after only 
  # affine alignment - no MST/GS
  ### fn_prepare_affine_template_ncc_all
  ### fn_template_ncc_loop aff_ncc
  ### fn_template_ncc_all_pairs_overlap $((NCC_NITER-1)) aff_ncc


  ### fn_shooting_correction_iterative NCC

  ### finalize_all

  # Compute all the summary stats
  qsubp4 -cwd -o $WDIR/dump -j y -N "mist_stats" -sync y $0 misc_final_stats
}

if [[ $1 ]]; then

  command=$1
  shift
	$command $@

else

  main
	
fi
