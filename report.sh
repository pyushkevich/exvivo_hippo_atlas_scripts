#!/bin/bash

# The directory where we are doing this work
ROOT=/data/picsl/pauly/dan2015/atlas2016

# Include file
source $ROOT/scripts/common.sh

# Which template?
TEMP=${1?}

function avg()
{
  iter=${1?}
  metric=${2?}
  prepost=${3?}
  feat=${4?}

  cat $ROOT/work/template_${TEMP}/iter_${iter}/stats/${metric}_${prepost}_${feat}* \
    | awk '$1==1 {k+=$2; n++} END {print k/n}'
}

function mean_jac()
{
  iter=${1?}
  mode=${2?}

  if [[ $mode == 'jac_min' ]]; then
    for id in $(cat $ROOT/scripts/subj.txt); do 
      cat $(ls $ROOT/work/template_${TEMP}/dump/norm_${TEMP}_$((i-1))_${id}.o* | tail -n 1) \
        | grep DetJac | grep 'LEVEL *3'; 
    done | awk '{a+=$7;n++} END {print a/n}'
  else
    for id in $(cat $ROOT/scripts/subj.txt); do 
      cat $(ls $ROOT/work/template_${TEMP}/dump/norm_${TEMP}_$((i-1))_${id}.o* | tail -n 1) \
        | grep DetJac | grep 'LEVEL *3'; 
    done | awk '{a+=$9; n++} END {print a/n}'
  fi
}
  

# Print the metrics 
for mod in pot theta cos_phi sin_phi img jac_min jac_max; do

  if [[ $mod == 'jac_min' || $mod == 'jac_max' ]]; then

    VAL[0]=1
    for ((i=1;i<=6;i++)); do
      VAL[$i]=$(mean_jac $((i-1)) $mod)
    done

  else

    if [[ $mod == 'img' ]]; then 
      metric="ncc"
    else
      metric="ssd"
    fi

    VAL[0]=$(avg 0 $metric pre $mod)

    for ((i=1;i<=6;i++)); do
      VAL[$i]=$(avg $((i-1)) $metric post $mod)
    done

  fi


  printf "%10s   %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n" $mod ${VAL[*]}

done
