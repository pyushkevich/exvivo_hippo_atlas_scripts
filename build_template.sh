#!/bin/bash 
#$ -S /bin/bash
set -x -e
echo "ok"

# The directory where we are doing this work
ROOT=/data/picsl/pauly/dan2015/atlas2016

# Include file
source $ROOT/scripts/common.sh

# List of all the ids to run
IDS=$(cat $ROOT/scripts/subj.txt)

# Location of the data
TDIR_PAR=$ROOT/work/template_par
TDIR_NCC=$ROOT/work/template_ncc
      
# Set the modalities
n=5
mods=(pot theta sin_phi cos_phi img)

# Get the variables for stage
function get_stage_vars()
{
  local stage=${1?}

  if [[ $stage == "par" ]]; then
    TDIR=$TDIR_PAR
  else
    TDIR=$TDIR_NCC
  fi
}


# This function configures the moving and fixed arrays for a subject
function get_mods_for_id()
{
  local id stage i SUBJDIR

  # The provided id
  id=${1?}
  stage=${2?}

  # Get the stage vars
  get_stage_vars $stage

  # The location of preproc output
  SUBJDIR=$ROOT/work/${id}
  unset MOVING
  unset WARPCHAIN

  # The original space for each channel
  ORIG=(
    $SUBJDIR/danparam/${id}_pot-extend.nii.gz
    $SUBJDIR/danparam/${id}_theta_extend_hfill.nii.gz
    $SUBJDIR/danparam/${id}_sin_phi_extend_hfill.nii.gz
    $SUBJDIR/danparam/${id}_cos_phi_extend_hfill.nii.gz
    $ROOT/input/${id}/${id}_img.nii.gz)

  # Warp chain for each channel
  WARPCHAIN=(
    ""
    ""
    ""
    ""
    "$SUBJDIR/cmrep/template_cmrepinvwarp.nii.gz")

  # Template directory
  if [[ $stage == "par" ]]; then

    # Names of the moving images
    for ((i=0; i<n; i++)); do
      if [[ ${mods[i]} == "img" ]]; then
        MOVING[i]=$SUBJDIR/cmrspc/${id}_img_cmrspc.nii.gz
      else
        MOVING[i]=${ORIG[i]}
      fi
    done

  else

    # The moving images are the result of the previous stage
    for ((i=0; i<n; i++)); do
      MOVING[i]=$TDIR_PAR/reslice_${id}_${mods[i]}.nii.gz
      WARPCHAIN[i]="$TDIR_PAR/twarp_${id}.nii.gz ${WARPCHAIN[i]}"
    done
  fi

  # Moving and resliced images
  unset FIXED RESLICE_PAR RESLICE_NCC
  for ((i=0; i<n; i++)); do
    FIXED[i]=$TDIR/template_${mods[i]}.nii.gz
    RESLICE[i]=$TDIR/reslice_${id}_${mods[i]}.nii.gz
  done

  # Report
  for ((i=0; i<n; i++)); do
    echo "ORIG[${mods[i]}] = ${ORIG[i]}"
    echo "MOVING[${mods[i]}] = ${MOVING[i]}"
    echo "FIXED[${mods[i]}] = ${FIXED[i]}"
    echo "WARPCHAIN[${mods[i]}] = ${WARPCHAIN[i]}"
    echo "RESLICE[${mods[i]}] = ${RESLICE[i]}"
  done
 
}
    
function greedy_iter()
{
  id=$1
  stage=$2

  # Get the moving/fixed/reslice variables
  get_mods_for_id $id $stage

  # Make directories
  mkdir -p $TDIR/stats

  # Parameters for the registration
  if [[ $stage == "par" ]]; then
    wgt=(1 0 0 0 0)
    iter=200x100x50x40
    metric=""
    GREEDY_PARAM="-s 0.6mm 0.1mm -e 0.5"
  else
    wgt=(0 0 0 0 1)
    iter=100x100x50
    metric="-m NCC 2x2x2"
    GREEDY_PARAM="-s 0.6mm 0.1mm -e 0.5"
  fi

  # Create the command line for the registration
  CMDL=""
  for ((i=0; i<n; i++)); do

    # If the weight of a modality is zero, exclude it instead
    if [[ ${wgt[i]} -gt  0 ]]; then
      CMDL="$CMDL -w ${wgt[i]} -i ${FIXED[i]} ${MOVING[i]}"
    fi
  done

  # The warp file - output of greedy
  WARP=$TDIR/twarp_${id}.nii.gz

  # Delete the warp if it exists
  rm -rf $WARP

  # Peform the fast registration
  $GREEDY_HOME/greedy \
    -d 3 -threads $NSLOTS $GREEDY_PARAM $CMDL -n $iter $metric \
    -gm $TDIR/template_mask.nii.gz \
    -o $WARP

  # Transform all the images into the template space
  for ((i=0; i<n; i++)); do

    # Reslice the image
    $GREEDY_HOME/greedy \
      -d 3 -rf ${FIXED[i]} \
      -rm ${ORIG[i]} ${RESLICE[i]} \
      -r $WARP ${WARPCHAIN[i]}

    # Measure overlap and NCC in the hippocampal region
    c3d ${FIXED[i]} ${MOVING[i]} \
      -scale -1 -add -dup -times ${FIXED[0]} -thresh 0 2 1 0 -lstat \
      > $TDIR/stats/ssd_pre_${mods[i]}_${id}.txt

    c3d ${FIXED[i]} ${RESLICE[i]} \
      -scale -1 -add -dup -times ${FIXED[0]} -thresh 0 2 1 0 -lstat \
      > $TDIR/stats/ssd_post_${mods[i]}_${id}.txt

    if [[ ${mods[i]} == "img" ]]; then

      c3d ${FIXED[i]} ${MOVING[i]} \
        -ncc 4x4x4 ${FIXED[0]} -thresh 0 2 1 0 -lstat \
        > $TDIR/stats/ncc_pre_${mods[i]}_${id}.txt

      c3d ${FIXED[i]} ${RESLICE[i]} \
        -ncc 4x4x4 ${FIXED[0]} -thresh 0 2 1 0 -lstat \
        > $TDIR/stats/ncc_post_${mods[i]}_${id}.txt

    fi
      
  done
}

# average images from iteration iter
function average()
{
  # Iteration
	iter=$1 

  # Modality
  i=$2

  # Stage
  stage=$3

  # Get stage variables
  get_stage_vars $stage

  # Directory where to average
  AVGDIR=$TDIR/iter_${iter}

  # Compute the images to average
  INPUTS=""
  for id in ${IDS}; do

    # Get the moving/resliced for this subject
    get_mods_for_id $id $stage
    echo "MY MOVING = ${MOVING[i]}"

    if [[ $iter -eq 0 ]]; then
      INPUTS="$INPUTS ${MOVING[i]}"
    else
      INPUTS="$INPUTS ${RESLICE[i]}"
    fi
  done

  # Type of averaging to perform
  AVGMODE=0
  if [[ ${mods[i]} == "img" ]]; then AVGMODE=1; fi

  # Do the averaging
  ${ANTSDIR}/AverageImages 3 $AVGDIR/template_${mods[i]}.nii.gz $AVGMODE ${INPUTS}

  # Scale template by 100 - why?
  if [[ ${mods[i]} == "img" ]]; then 
    c3d $AVGDIR/template_${mods[i]}.nii.gz -scale 100 -o $AVGDIR/template_${mods[i]}.nii.gz
  fi

  # Copy into the template working directory
  cp -av $AVGDIR/template_${mods[i]}.nii.gz $TDIR/

  # Compute the mask
  if [[ ${mods[i]} == "pot" ]]; then
    c3d $TDIR/template_pot.nii.gz -thresh 0 2.5 1 0 -dilate 1 10x10x10vox -o $TDIR/template_mask.nii.gz
  fi
}

function main_loop
{
  # Which stage to run in?
  stage=${1?}

  # Which directory
  get_stage_vars $stage

  # Iterations
  NITER=5
  SITER=0

  # Create dump directory
  mkdir -p $TDIR/dump

  # Perform the loop
	for ((iter=$SITER; iter<=$NITER; iter++));
	do
    
    # Make a directory to hold the current average
    AVGDIR=$TDIR/iter_${iter}
    mkdir -p $AVGDIR
    rm -rf $AVGDIR/*

    # Compute current average for each modality
    for (( i=0; i<n; i++)); do
      qsubp4 -cwd -o $TDIR/dump -j y -N "average_${stage}_${iter}_${i}" \
        $0 average $iter $i $stage
    done

		# Wait for completion
		qsub -cwd -o $TDIR/dump -j y -hold_jid "average_${stage}*" -sync y -b y sleep 1

    # Make sure that the templates exist
    for ((i=0;i<n;i++)); do
      if [[ ! -f $AVGDIR/template_${mods[i]}.nii.gz ]]; then 
        echo "Failed to create $AVGDIR/template_${mods[i]}.nii.gz"
        exit -1
      fi
    done

		# register all images to the average and place in new folder for iter+1:
		for ID in ${IDS}; do

			# submit ANTS job:
      qsubp2 -cwd -o $TDIR/dump -j y -N "norm_${stage}_${iter}_${ID}" \
        $0 greedy_iter ${ID} ${stage} 
		
    done
		
		# Wait for completion
		qsub -cwd -o $TDIR/dump -j y -hold_jid "norm_${stage}*" -sync y -b y sleep 1

    # Move the ssd files into the right directory
    mkdir -p $TDIR/iter_${iter}/stats
    mv $TDIR/stats/* $TDIR/iter_${iter}/stats/

	done
}

if [[ $1 ]]; then

  command=$1
  shift
	$command $@

else

	#main_loop par
  main_loop ncc
	
fi
