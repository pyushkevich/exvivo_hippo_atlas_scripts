from vtk import *
import sys
import xml.etree.ElementTree as ET
import time

# Enable off-screen rendering with MESA
factGraphics = vtkGraphicsFactory()
factGraphics.SetUseMesaClasses(1)

def show_mesh(pattern, label, color, ren):
	# Read the image
	reader=vtkPolyDataReader()
	file = (pattern) % (label)
	reader.SetFileName(file)
	reader.Update()
	print reader.GetOutput().GetNumberOfPoints()
	
	# Create an actor
	mapper=vtkPolyDataMapper()
	mapper.SetInputConnection(reader.GetOutputPort())
	actor=vtkActor()
	actor.SetMapper(mapper)
	
	# Set the color
	actor.GetProperty().SetColor(color[0], color[1], color[2])
	ren.AddActor(actor)

def pvcc_get_val(root,name):
	val=0.0
	for prop in root.findall(".//Property"):
		if prop.get("name") == name:
			val=float(prop[0].get("value"));
			break
	print (("XML for %s is %f ") % (name,val))
	return val

def pvcc_get_arr3(root,name):
	vec = [0.0, 0.0, 0.0]
	for prop in root.findall(".//Property"):
		if prop.get("name") == name:
			elt0=prop[0].get("value");
			elt1=prop[1].get("value");
			elt2=prop[2].get("value");
			vec = [float(elt0), float(elt1), float(elt2)]
			break

	print (("XML for %s is %f %f %f") % (name,vec[0],vec[1],vec[2]))
	return vec

def sshot(renwin,file):
	renwin.Modified()
	renwin.Render()
	winfil=vtkWindowToImageFilter()
	winfil.SetInput(renwin)
	png=vtkPNGWriter()
	png.SetInputConnection(winfil.GetOutputPort())
	png.SetFileName(file)
	png.Write()

def parse_range(x):
	result = []
	for part in x.split(','):
		if '-' in part:
			a, b = part.split('-')
			a, b = int(a), int(b)
			result.extend(range(a, b + 1))
		else:
			a = int(part)
			result.append(a)
	return result


# Read arguments
mesh_pattern = sys.argv[1];
pvcc_file = sys.argv[2];
width = int(sys.argv[3]);
height = int(sys.argv[4]);
output = sys.argv[5];
labels = parse_range(sys.argv[6]);

print labels

# Create a rendering window and renderer
ren = vtkRenderer()
renWin = vtkRenderWindow()
renWin.DoubleBufferOn()
renWin.OffScreenRenderingOn();
renWin.AddRenderer(ren)
renWin.SetSize(width,height)
ren.SetBackground(82/255.,87/255.,110/255.)

# Add the meshes
colors=[
    [1,0,0],[0,1,0],[0,0,1],[1,1,0],[140./255,98./255,38./255],[1,0,1],
    [0,0,0],[212./255,175./255,111./255],[0,1,1]];
for l in labels:
  show_mesh(mesh_pattern, l, colors[l-1], ren)

# Read camera from PVCC file
tree = ET.parse(pvcc_file);
root = tree.getroot()

# Position camera
camera = ren.GetActiveCamera();
vu = pvcc_get_arr3(root, 'CameraViewUp');
camera.SetViewUp(vu[0], vu[1], vu[2]);
fp = pvcc_get_arr3(root, 'CameraFocalPoint');
camera.SetFocalPoint(fp[0],fp[1],fp[2]);
va = pvcc_get_val(root,'CameraViewAngle');
camera.SetViewAngle(va);
cp = pvcc_get_arr3(root, 'CameraPosition');
camera.SetPosition(cp[0],cp[1],cp[2]);
ren.ResetCameraClippingRange();

sshot(renWin,output);
