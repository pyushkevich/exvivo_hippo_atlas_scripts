#!/usr/bin/pvpython
 
from paraview.simple import *
import xml.etree.ElementTree as ET
 
import time
import sys

def pvcc_get_val(root,name):
  elt0=root.findall(".//Property[@name='%s']/Element[@index='0']" % name)[0].get("value");
  return float(elt0);

def pvcc_get_arr3(root,name):
  elt0=root.findall(".//Property[@name='%s']/Element[@index='0']" % name)[0].get("value");
  elt1=root.findall(".//Property[@name='%s']/Element[@index='1']" % name)[0].get("value");
  elt2=root.findall(".//Property[@name='%s']/Element[@index='2']" % name)[0].get("value");
  return [float(elt0), float(elt1), float(elt2)]

def show_mesh(pattern, label, color):
  #read a vtp
  reader = LegacyVTKReader(FileNames=[pattern % label]);
  #set surface color
  dp = GetDisplayProperties()
  dp.DiffuseColor = color; #blue
  dp.Representation = "Surface";

# Read arguments
mesh_pattern = sys.argv[1];
pvcc_file = sys.argv[2];
width = int(sys.argv[3]);
height = int(sys.argv[4]);
output = sys.argv[5];

# Read camera from PVCC file
tree = ET.parse(pvcc_file);
root = tree.getroot()

#test = root.findall(".//*[@name='CameraPosition']");
test = root.findall(".//Property[@name='CameraPosition']/Element[@index='0']");
print(test);
print(test[0].get("value"));
print(pvcc_get_arr3(root,'CameraPosition'))

#position camera
view = CreateRenderView()
view.CameraViewUp = pvcc_get_arr3(root, 'CameraViewUp');
view.CameraFocalPoint = pvcc_get_arr3(root, 'CameraFocalPoint');
view.CameraViewAngle = pvcc_get_val(root,'CameraViewAngle');
view.CameraPosition = pvcc_get_arr3(root, 'CameraPosition');
 
#set the background color
view.Background = [82./255.,87./255.,110./255.]  #white
 
#set image size
view.ViewSize = [width, height] #[width, height]
 
#read a vtp
#reader = LegacyVTKReader(FileNames=["movie_svm_label9_36.vtk"]);
 
#draw the object
#Show()
 
#set surface color
#dp = GetDisplayProperties()
#dp.DiffuseColor = [0, 1, 1] #blue
#dp.Representation = "Surface"

show_mesh(mesh_pattern,1,[1, 0, 0]);
show_mesh(mesh_pattern,2,[0, 1, 0]);
show_mesh(mesh_pattern,3,[0, 0, 1]);
show_mesh(mesh_pattern,4,[1, 1, 0]);
show_mesh(mesh_pattern,5,[140./255, 98./255, 38./255]);
show_mesh(mesh_pattern,6,[1, 0, 1]);
show_mesh(mesh_pattern,9,[0, 1, 1]);
 
Show();
Render()
 
#save screenshot
WriteImage(output);
