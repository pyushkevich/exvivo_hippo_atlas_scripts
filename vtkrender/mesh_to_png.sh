#!/bin/bash
set -x -e
export LD_LIBRARY_PATH=/home/pauly/lib:$LD_LIBRARY_PATH

/data/picsl-build/pauly/buildbot/Nightly/vtk/v6.3.0/gcc64rel/bin/vtkpython \
  /data/picsl/pauly/dan2015/atlas2016/scripts/vtkrender/pics.py $@
