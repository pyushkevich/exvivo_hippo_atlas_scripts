#!/bin/bash
#$ -S /bin/bash

# PATH to CMREP commands
CMREP_HOME=/data/picsl/pauly/bin/cmrep_2015_04
export LD_LIBRARY_PATH=$CMREP_HOME/lib:$HOME/lib:$LD_LIBRARY_PATH

# PATH to C3D
C3D_HOME=/data/picsl/pauly/bin

# PATH to Greedy
GREEDY_HOME=/data/picsl/pauly/bin

# Path to Dan's code
DANCODE_HOME=/data/picsl/pauly/nibtend_renewal/indd_to_atlas/danparam

# Path to ANTS
ANTSDIR=/share/apps/ANTs/2014-06-23/build/bin

# Path to R
RHOME=/data/picsl/pauly/Rbuild/install
