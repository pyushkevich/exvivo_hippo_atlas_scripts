#!/bin/bash
SOURCE=/data/picsl/lwisse/IND/histo_segmentations
ROOT=/data/picsl/pauly/dan2015/histology

function getid()
{
  local fn=$1
  local NUM=$(echo $fn | awk -F '/' '{print $7}')
  local BLK=$(echo $fn | awk -F '/' '{print $8}')
  local ID=$(ls $ROOT/data | grep "${NUM}.*block${BLK}")
  echo $ID
}

for fn in $(ls $SOURCE/*/*/canon/wholemri_to_canon_LW4.nii.gz); do
  ID=$(getid $fn)
  cp -av $fn $ROOT/lwseg_atlas/input/${ID}_lwseg.nii.gz
  echo $ID
done

for fn in $(ls $SOURCE/*/*/canon/wholemri_to_canon_LW4.nii); do
  ID=$(getid $fn)
  gzip -c $fn > $ROOT/lwseg_atlas/input/${ID}_lwseg.nii.gz
  echo $ID
done

