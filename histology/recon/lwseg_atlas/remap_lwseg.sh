#!/bin/bash
set -x -e

# Where the work is being done
ROOT=/data/picsl/pauly/dan2015/histology



function runme()
{
  ID=${1?}
  BLOCK=${2?}
  PAD_PRE=${3?}
  PAD_POST=${4?}

  IGNORE_WARP=${5}

  BID=${ID}-block${BLOCK}

  # Histology directory for block
  BDIR=$ROOT/data/${BID}

  # These are the inputs
  LW=$ROOT/lwseg_atlas/input/${BID}_lwseg.nii.gz
  CANON_MRI=$BDIR/canon/wholemri_to_canon.nii.gz

  # Work directory for this block
  WORK=$ROOT/lwseg_atlas/work/${BID}
  mkdir -p $WORK

  # Intermediate/temporary files
  SLICES=$WORK/slices.nii.gz
  SLICESBG=$WORK/slices_bg.nii.gz
  INV_MRI_WARP=$WORK/inv_mri_warp.nii.gz
  INSIDE_REGION=$WORK/mask_inside_region.nii.gz

  # Go from 3D canon space to Laura's space - that will be a bazillion warps
  M_BLOCK_TO_CANON=$BDIR/canon/block_to_canon.mat
  M_BLOCK_TO_WHOLE_MANUAL=$BDIR/mri/block_to_whole_rigid_manual.mat
  M_WHOLE_TO_B2W_AFFINE=$BDIR/mri/whole_to_b2w_affine.mat
  
  # Link original segmentation and whole MRI block
  ln -sf $LW $WORK/${BID}_lwseg_canon.nii.gz
  ln -sf $CANON_MRI $WORK/${BID}_wholemri_canon.nii.gz

  # Invert the MRI warp
  if [[ ! -f $WORK/inv_mri_warp.nii.gz ]]; then
    greedy -d 3 -iw $BDIR/mri/whole_to_b2w_warp.nii.gz $INV_MRI_WARP 
  fi

  # Split into masks, ignore dark band, add end-caps
  CMD_PAD_PRE=0x0x0; if [[ $PAD_PRE -gt 0 ]]; then CMD_PAD_PRE=0x0x4; fi
  CMD_PAD_POST=0x0x0; if [[ $PAD_POST -gt 0 ]]; then CMD_PAD_POST=0x0x4; fi

  # Extract just the slices with optional label-9 endcaps
  c3d $LW -as X -thresh 1 inf 1 0 -dilate 1 200x200x0 -push X -times -o $SLICES
  c3d $LW -as X -thresh 1 inf 1 0 -dilate 1 200x200x0 -push X -thresh 1 inf 1 0 -times \
    -as Y -dilate 1 600x600x0 -push Y -scale -1 -add \
    -replace 1 9 $SLICES -add \
    -trim -0vox -pad $CMD_PAD_PRE $CMD_PAD_POST 9 -o $SLICESBG

  # Create a mask between the first and last slice - what should be interpolated
  c3d $LW -thresh 1 inf 1 0 -dilate 1 10x10x0 -trim 0mm -thresh -inf inf 1 0 \
    -pad 2x2x2 2x2x2 0 -o $INSIDE_REGION

  # Warp chain
  if [[ $IGNORE_WARP ]]; then
    WARPCHAIN="$M_BLOCK_TO_WHOLE_MANUAL $M_BLOCK_TO_CANON,-1"
  else
    WARPCHAIN="$M_WHOLE_TO_B2W_AFFINE,-1 $INV_MRI_WARP $M_BLOCK_TO_WHOLE_MANUAL $M_BLOCK_TO_CANON,-1"
  fi

  # Warp into target spaces
  greedy -d 3 \
    -rf $BDIR/mri/mri_whole.nii.gz \
    -ri LABEL 0.2vox \
    -rm $SLICESBG $WORK/slices_bg_to_wholemri.nii.gz \
    -rm $INSIDE_REGION $WORK/inside_mask_to_wholemri.nii.gz \
    -r $WARPCHAIN
}

function combine_blocks()
{
  ID=${1?}
  SPACE=wholemri

  # Combine the slices from all the blocks
  WORK=$ROOT/lwseg_atlas/work/${SPACE}_${ID}
  mkdir -p $WORK

  # Link to whole-brain MRI
  ln -sf $(ls $ROOT/data/${ID}*/mri/mri_whole.nii.gz | head -n 1) $WORK/${ID}_mri_whole.nii.gz

  # Combined slices
  COMB_SLICES=$WORK/${ID}_combine_slices_bg_to_${SPACE}.nii.gz
  RESULT=$WORK/${ID}_lwseg_interp_${SPACE}.nii.gz
  MASK=$WORK/${ID}_block_mask_${SPACE}.nii.gz
  RESULT_MASKED=$WORK/${ID}_lwseg_interp_masked_${SPACE}.nii.gz

  # Add segmentations
  c3d $ROOT/lwseg_atlas/work/${ID}*/slices_bg_to_${SPACE}.nii.gz \
    -foreach -thresh 1 inf 1 0 -endfor -accum -add -endaccum -thresh 1 1 1 0 \
    -popas MASK \
    work/${ID}*/slices_bg_to_${SPACE}.nii.gz \
    -accum -add -endaccum -push MASK -times \
    -o $COMB_SLICES

  # Interpolate using distance transform
  c3d -verbose $COMB_SLICES \
    -replace 0 100 -split \
    -foreach -sdt -clip 0 inf -smooth-fast 0.2mm -scale -1 -endfor \
    -scale 0 -shift -1e10 -merge \
    -replace 9 0 -o $RESULT

  # Combine block binary masks to create a mask of covered regions
  c3d $ROOT/lwseg_atlas/work/${ID}*/inside_mask_to_wholemri.nii.gz \
    -accum -add -endaccum -thresh 1 1 1 0 \
    -o $MASK \
    $RESULT -times -o $RESULT_MASKED

}

function main()
{
  mkdir -p dump

  # List of all the blocks to process with whether this is first or last
  TAGS=$(cat tags.txt)

  # List of all IDS to process
  IDS=$(cat tags.txt | sed -e "s/_.*//g")

  for tag in $TAGS; do

    read ID BLOCK PRE POST <<<$(echo $tag | sed -e "s/_/ /g")

    qsubp4 -j y -N "bl_${ID}_${BLOCK}" -cwd -V -o dump $0 runme $ID $BLOCK $PRE $POST

  done
  qsub -j y -o dump -b y -hold_jid "bl_*" -sync y /bin/sleep 1

  for id in $IDS; do 
    for space in invivo wholemri; do
      qsubp4 -j y -N "comb_${id}_${space}"  -cwd -V -o dump $0 combine_blocks $id $space
    done
  done

}

if [[ $1 ]]; then
  CMD=$1
  shift
  $CMD $@
else
  main
fi


