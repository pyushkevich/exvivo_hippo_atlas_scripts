#!/bin/bash
#$ -S /bin/bash

# Which block to process
BLOCK=${1?}

# For each slice, automatically generate the mask
pushd $BLOCK
SLICES="$(ls hist/*/ann/ann_neg.nii.gz | awk -F '/' '{print $2}')"
popd

# Path
PATH=/data/picsl/pauly/bin:$PATH

# Location of the remap script
SCRIPT_DIR=/data/picsl/pauly/dan2015/histology

# Define temp dir
if [[ ! $TMPDIR ]]; then
  TMPDIR=/tmp
fi

# Compute masks and MRI-looking histology slices
function remap_intensity()
{
  for SLICE in $SLICES; do

    # go to the slice directory
    pushd $BLOCK/hist/$SLICE

    # Automatic mask generation
    c2d \
      -mcs slide/slide_neg.nii.gz -as R -rf-apply $SCRIPT_DIR/slide_rf.dat -vote \
      -stretch 0 1 1 0 -as R -pad 5x5 5x5 0 -dilate 0 20x20 -dilate 1 20x20 \
      -insert R 1 -int 0 -reslice-identity \
      -type uchar -o slide/slide_mask.nii.gz 

    # Reslice mask to canonical space
    greedy -d 3 \
      -rf canon/canonical_space.mha \
      -ri NN -rm slide/slide_mask.nii.gz canon/histology_mask_canon.mha \
      -r mat/mri.mat mat/mri_to_slide_x64.mat,-1


    # Create a temporary location to place NIFTIs, since R does not support MHA
    mkdir -p tmp
    for file in histology_mask_canon slide_to_canon wholemri_to_canon; do
      c2d -mcs canon/${file}.mha -omc tmp/${file}.nii.gz
    done

    ls -l tmp/
    
    # R-based intensity normalization
    Rscript $SCRIPT_DIR/intensity_remap.R

    # Convert the result to a MHA
    c3d canon/canonical_space.mha tmp/mrisim.nii.gz \
      -copy-transform \
      -compress -o canon/mrisim_canon.mha

    # Remove tmp
    rm -rf tmp

    popd

  done
}

# Get the previous slice
function prev_slice()
{
  local S
  for S in $SLICES; do
    echo $S
  done | grep -B 1 $1 | grep -v $1 | cat
}

function next_slice()
{
  local S
  for S in $SLICES; do
    echo $S
  done | grep -A 1 $1 | grep -v $1 | cat
}

# Compute NCC for a pair of images
function ncc()
{
  local IM1=$1
  local IM2=$2
  local MASK=$3

  c2d $IM1 -as R -dup $IM2 -copy-transform \
    -ncc 4x4 -replace NaN 0 -Inf 0 Inf 0 \
    -push R $MASK -copy-transform \
    -lstat | tail -n 1 | awk '{print $2}'
}

# Peform registration in a Laplacian-type loop
function groupwise_affine()
{
  mkdir -p $BLOCK/gw

  # Prepare by copying the data to a different location
  for SLICE in $SLICES; do

    WD=$BLOCK/hist/$SLICE
    mkdir -p $WD/gw

    cp -av $WD/canon/mrisim_canon.mha $WD/gw/mrisim_k.mha
    cp -av $WD/canon/histology_mask_canon.mha $WD/gw/mask_k.mha

    echo "1 0 0" > $WD/gw/affine_k.mat
    echo "0 1 0" >> $WD/gw/affine_k.mat
    echo "0 0 1" >> $WD/gw/affine_k.mat

  done

  # Iterate repeatedly
  for ((it=0;it<5;it++)); do

    # Shuffle the slice order
    SHUFFLE=$(
      for SLICE in $SLICES; do
        echo $SLICE
      done | shuf)

    # Iterate 
    for SLICE in $SHUFFLE; do

      WD=$BLOCK/hist/$SLICE

      # Get next and previous slices
      PREV=$(prev_slice $SLICE)
      NEXT=$(next_slice $SLICE)

      # Images involved 
      MRI=$WD/canon/wholemri_to_canon.mha
      SIM_RAW=$WD/canon/mrisim_canon.mha
      MASK_RAW=$WD/canon/histology_mask_canon.mha

      # Build up the command line
      CMD="-i $MRI $SIM_RAW "

      if [[ $PREV ]]; then
        CMD="$CMD -i $BLOCK/hist/$PREV/gw/mrisim_k.mha $SIM_RAW"
      fi

      if [[ $NEXT ]]; then
        CMD="$CMD -i $BLOCK/hist/$NEXT/gw/mrisim_k.mha $SIM_RAW"
      fi

      # Do greedy affine
      greedy -d 2 \
        $CMD \
        -a -n 100x100x40x0 -m NCC 4x4 -gm $WD/gw/mask_k.mha \
        -o $WD/gw/affine_k.mat -ia $WD/gw/affine_k.mat

      # Apply registration to slice
      greedy -d 2 \
        -rf $MRI \
        -rm $SIM_RAW $WD/gw/mrisim_k.mha \
        -ri LABEL 0.2vox -rm $MASK_RAW $WD/gw/mask_k.mha \
        -r $WD/gw/affine_k.mat

      # Compute the NCC with MRI
      echo "MRI $(ncc $MRI $WD/gw/mrisim_k.mha $WD/gw/mask_k.mha)" > $WD/gw/ncc_k.txt

      if [[ $PREV ]]; then
        echo "PREV $(ncc $BLOCK/hist/$PREV/gw/mrisim_k.mha \
          $WD/gw/mrisim_k.mha $WD/gw/mask_k.mha)" >> $WD/gw/ncc_k.txt
      fi

      if [[ $NEXT ]]; then
        echo "NEXT $(ncc $BLOCK/hist/$NEXT/gw/mrisim_k.mha \
          $WD/gw/mrisim_k.mha $WD/gw/mask_k.mha)" >> $WD/gw/ncc_k.txt
      fi

    done

    # Print out the NCC for this iteration
    cat $BLOCK/hist/*/gw/ncc_k.txt > $BLOCK/gw/ncc_iter_${it}.txt

  done
}

function groupwise_warp()
{
  # Prepare by copying the data to a different location
  for SLICE in $SLICES; do

    WD=$BLOCK/hist/$SLICE

    cp -av $WD/gw/mrisim_k.mha $WD/gw/mrisim_p.mha
    cp -av $WD/gw/mask_k.mha $WD/gw/mask_p.mha

  done

  # Iterate repeatedly
  for ((it=0;it<5;it++)); do

    # Shuffle the slice order
    SHUFFLE=$(
      for SLICE in $SLICES; do
        echo $SLICE
      done | shuf)

    # Iterate 
    for SLICE in $SHUFFLE; do

      WD=$BLOCK/hist/$SLICE

      # Get next and previous slices
      PREV=$(prev_slice $SLICE)
      NEXT=$(next_slice $SLICE)

      # Images involved 
      MRI=$WD/canon/wholemri_to_canon.mha
      SIM_RAW=$WD/canon/mrisim_canon.mha
      MASK_RAW=$WD/canon/histology_mask_canon.mha

      # Build up the command line
      CMD="-i $MRI $SIM_RAW "

      if [[ $PREV ]]; then
        CMD="$CMD -i $BLOCK/hist/$PREV/gw/mrisim_p.mha $SIM_RAW"
      fi

      if [[ $NEXT ]]; then
        CMD="$CMD -i $BLOCK/hist/$NEXT/gw/mrisim_p.mha $SIM_RAW"
      fi

      # Do greedy affine
      greedy -d 2 \
        $CMD \
        -n 80x80x80x40x0 -m NCC 4x4 -s 30vox 0.7vox -gm $WD/gw/mask_p.mha \
        -o $WD/gw/warp_p.mha -it $WD/gw/affine_k.mat

      # Apply registration to slice
      greedy -d 2 \
        -rf $MRI \
        -rm $SIM_RAW $WD/gw/mrisim_p.mha \
        -ri LABEL 0.2vox -rm $MASK_RAW $WD/gw/mask_p.mha \
        -r $WD/gw/warp_p.mha $WD/gw/affine_k.mat

      # Compute the NCC with MRI
      echo "MRI $(ncc $MRI $WD/gw/mrisim_p.mha $WD/gw/mask_p.mha)" > $WD/gw/ncc_p.txt

      if [[ $PREV ]]; then
        echo "PREV $(ncc $BLOCK/hist/$PREV/gw/mrisim_p.mha \
          $WD/gw/mrisim_p.mha $WD/gw/mask_p.mha)" >> $WD/gw/ncc_p.txt
      fi

      if [[ $NEXT ]]; then
        echo "NEXT $(ncc $BLOCK/hist/$NEXT/gw/mrisim_p.mha \
          $WD/gw/mrisim_p.mha $WD/gw/mask_p.mha)" >> $WD/gw/ncc_p.txt
      fi

    done

    # Print out the NCC for this iteration
    cat $BLOCK/hist/*/gw/ncc_p.txt > $BLOCK/gw/ncc_warp_iter_${it}.txt

  done
}

function prepare_seg()
{
  # Iterate 
  for SLICE in $SLICES; do

    WD=$BLOCK/hist/$SLICE

    # Create the working file locations
    LDIR_ROOT=$TMPDIR/ldir
    LDIR=$LDIR_ROOT/$SLICE

    mkdir -p $WD
    mkdir -p $LDIR

    # Images involved 
    MRI=$WD/canon/wholemri_to_canon.mha

    # Histology slide in canonical space
    HIST_CAN=$WD/canon/slide_to_canon.mha
    ANN_CAN=$WD/canon/ann_to_canon.mha
    SVG_CAN=$WD/canon/slide_seg_to_canon.mha

    # Apply transform to the histology image, make it the right color
    c3d \
      -mcs $HIST_CAN \
      -foreach -stretch 0 255 255 0 -endfor \
      -oo $TMPDIR/slide_pos_%02d.mha

    # Apply transform to the annotation image in order to extract markup
    c3d \
      -mcs $ANN_CAN \
      -foreach -stretch 0 255 255 0 -endfor \
      -oo $TMPDIR/ann_pos_%02d.mha

    # If an SVG image exists, apply transformation to it also, and if it does not 
    # create a blank white image to transform
    if [[ -f $SVG_CAN ]]; then
      c3d -mcs $SVG_CAN -type ushort -oo $TMPDIR/slide_svg_%02d.mha
    else
      c3d -mcs $HIST_CAN -foreach -thresh -inf inf 65535 0 -endfor \
      -type ushort -oo $TMPDIR/slide_svg_%02d.mha
    fi

    # Unfortunately we are interpolating twice here, because no way to combine 2D and 3D
    # transforms that are involved between slice space and canonical space. 
    greedy -d 2 \
      -rf $MRI \
      -rm $TMPDIR/slide_pos_00.mha $LDIR/reslice_slide_to_wholemri_warp_00.mha \
      -rm $TMPDIR/slide_pos_01.mha $LDIR/reslice_slide_to_wholemri_warp_01.mha \
      -rm $TMPDIR/slide_pos_02.mha $LDIR/reslice_slide_to_wholemri_warp_02.mha \
      -rm $TMPDIR/slide_svg_00.mha $LDIR/reslice_svg_to_wholemri_warp_00.mha \
      -rm $TMPDIR/slide_svg_01.mha $LDIR/reslice_svg_to_wholemri_warp_01.mha \
      -rm $TMPDIR/slide_svg_02.mha $LDIR/reslice_svg_to_wholemri_warp_02.mha \
      -rm $TMPDIR/ann_pos_00.mha $LDIR/reslice_ann_to_wholemri_warp_00.mha \
      -rm $TMPDIR/ann_pos_01.mha $LDIR/reslice_ann_to_wholemri_warp_01.mha \
      -rm $TMPDIR/ann_pos_02.mha $LDIR/reslice_ann_to_wholemri_warp_02.mha \
      -r \
        $WD/gw/warp_p.mha \
        $WD/gw/affine_k.mat

    # Compute the markup
    c3d \
      $LDIR/reslice_slide_to_wholemri_warp_00.mha \
      $LDIR/reslice_ann_to_wholemri_warp_00.mha \
      -scale -1 -add -dup -times \
      $LDIR/reslice_slide_to_wholemri_warp_01.mha \
      $LDIR/reslice_ann_to_wholemri_warp_01.mha \
      -scale -1 -add -dup -times \
      $LDIR/reslice_slide_to_wholemri_warp_02.mha \
      $LDIR/reslice_ann_to_wholemri_warp_02.mha \
      -scale -1 -add -dup -times \
      -add -add -scale 0.33333 -sqrt \
      -o $LDIR/markup.mha

    # Project these images into canonical space
    for img in \
      $LDIR/reslice_slide_to_wholemri_warp_00.mha \
      $LDIR/reslice_slide_to_wholemri_warp_01.mha \
      $LDIR/reslice_slide_to_wholemri_warp_02.mha \
      $LDIR/reslice_svg_to_wholemri_warp_00.mha \
      $LDIR/reslice_svg_to_wholemri_warp_01.mha \
      $LDIR/reslice_svg_to_wholemri_warp_02.mha \
      $LDIR/reslice_ann_to_wholemri_warp_00.mha \
      $LDIR/reslice_ann_to_wholemri_warp_00.mha \
      $LDIR/reslice_ann_to_wholemri_warp_01.mha \
      $LDIR/reslice_ann_to_wholemri_warp_02.mha \
      $LDIR/markup.mha; do

      c3d $MRI $img -copy-transform -o $img

    done

  done

  # The canonical space 
  CAN3D=$BLOCK/canon/canonical_space.nii.gz

  # Smart tiling of the images - allows for gaps
  for CMP in 00 01 02; do

    c3d -verbose $CAN3D -popas R \
      $LDIR_ROOT/*/reslice_slide_to_wholemri_warp_${CMP}.mha \
      -foreach -insert R 1 -reslice-identity -dup -thresh 0 0 1 0 \
      -push R -times -add -as R -clear -endfor \
      -push R -o $TMPDIR/comp_slide_${CMP}.nii.gz

    c3d -verbose $CAN3D -popas R \
      $LDIR_ROOT/*/reslice_svg_to_wholemri_warp_${CMP}.mha \
      -foreach -insert R 1 -reslice-identity -dup -thresh 0 0 1 0 \
      -push R -times -add -as R -clear -endfor \
      -push R -o $TMPDIR/comp_svg_${CMP}.nii.gz

    c3d -verbose $CAN3D -popas R \
      $LDIR_ROOT/*/reslice_ann_to_wholemri_warp_${CMP}.mha \
      -foreach -insert R 1 -reslice-identity -dup -thresh 0 0 1 0 \
      -push R -times -add -as R -clear -endfor \
      -push R -o $TMPDIR/comp_ann_${CMP}.nii.gz

  done

  # Combine components
  c3d $TMPDIR/comp_slide_00.nii.gz $TMPDIR/comp_slide_01.nii.gz $TMPDIR/comp_slide_02.nii.gz \
    -type uchar -omc $BLOCK/canon/histology_to_canon.nii.gz

  c3d $TMPDIR/comp_svg_00.nii.gz $TMPDIR/comp_svg_01.nii.gz $TMPDIR/comp_svg_02.nii.gz \
    -type ushort -omc $BLOCK/canon/histo_seg_to_canon.nii.gz

  c3d $TMPDIR/comp_ann_00.nii.gz $TMPDIR/comp_ann_01.nii.gz $TMPDIR/comp_ann_02.nii.gz \
    -type uchar -omc $BLOCK/canon/annotation_to_canon.nii.gz

  # Do this for the markup too
  c3d -verbose $CAN3D -popas R \
    $LDIR_ROOT/*/markup.mha \
    -foreach -insert R 1 -reslice-identity -dup -thresh 0 0 1 0 \
    -push R -times -add -as R -clear -endfor \
    -push R -o $BLOCK/canon/markup_to_canon.nii.gz

  # Generate a segmentation image with proper labels
  c3d \
    $BLOCK/canon/seg_db_to_canon.nii.gz -scale 2 \
    $BLOCK/canon/seg_hf_to_canon.nii.gz \
    -add -replace 1 10 2 5 3 5 \
    -type uchar -o $BLOCK/canon/danseg_to_canon.nii.gz  

  # Convert the segmentation to a label image
  c3d -mcs $BLOCK/canon/histo_seg_to_canon.nii.gz \
    -rgb2hsv -pop -thresh 0.1 inf 1 0 -popas M -o /tmp/hue.nii.gz -popas H \
    -push M -stretch 0 1 1 0 \
    -push H -shift 0 -dup -times -scale -1 -exp -push M -times \
    -push H -shift -110.6 -dup -times -scale -1 -exp -push M -times \
    -push H -shift -208.2 -dup -times -scale -1 -exp -push M -times \
    -push H -shift -56.98 -dup -times -scale -1 -exp -push M -times \
    -push H -shift -180  -dup -times -scale -1 -exp -push M -times \
    -push H -shift -300.7 -dup -times -scale -1 -exp -push M -times \
    -vote -type char -o $BLOCK/canon/histo_labels_to_canon.nii.gz

}

#remap_intensity
#groupwise_affine
#groupwise_warp
prepare_seg

#
