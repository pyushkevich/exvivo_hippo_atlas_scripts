#!/bin/bash

#IDS=$(ls data)
IDS=$(ls data | grep 5767-L)

mkdir -p dump

for id in $IDS; do

  qsubp4 -o dump -N "hist_$id" -cwd -V -j y \
    ./autoproc.sh data/$id

done
