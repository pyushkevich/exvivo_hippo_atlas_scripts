#!/bin/bash
#$ -S /bin/bash
set -x -e

function combine_blocks()
{
  ID=${1?}
  WORK=./combine/$ID

  mkdir -p $WORK

  # List blocks
  BLOCKS=($(ls data | grep $ID | sed -e "s/.*-block//g" | sort -u))
  FIRSTB=${BLOCKS[0]}

  # From the first block, take the MRI
  cp -av data/${ID}-block${FIRSTB}/mri/mri_whole.nii.gz $WORK/

  # For each block, warp histology into the target space
  for B in ${BLOCKS[*]}; do

    BDIR=data/${ID}-block${B}

    # Go from 3D canon space to Laura's space - that will be a bazillion warps
    M_BLOCK_TO_CANON=$BDIR/canon/block_to_canon.mat
    M_BLOCK_TO_WHOLE_MANUAL=$BDIR/mri/block_to_whole_rigid_manual.mat
    M_WHOLE_TO_B2W_AFFINE=$BDIR/mri/whole_to_b2w_affine.mat
    INV_MRI_WARP=$WORK/inv_mri_warp.nii.gz

    # This is the block MRI
    BLOCK_MRI=$BDIR/mri/mri_block.nii.gz

    # Create inverse warp
    if [[ ! -f $INV_MRI_WARP ]]; then
      greedy -d 3 -iw $BDIR/mri/whole_to_b2w_warp.nii.gz $INV_MRI_WARP 
    fi
<<'SKIP'
    # Apply a blur to the histology data - Nyquist stuff
    c3d -mcs $BDIR/canon/histology_to_canon.nii.gz \
      -foreach -smooth-fast 0.08mm -endfor \
      -oo $TMPDIR/histosm_%d.nii.gz \
      -thresh -inf inf 1 0 -o $TMPDIR/histomsk.nii.gz

    greedy -d 3 \
      -rf $WORK/mri_whole.nii.gz \
      -rm $TMPDIR/histosm_0.nii.gz $TMPDIR/histo_block${B}_0.nii.gz \
      -rm $TMPDIR/histosm_1.nii.gz $TMPDIR/histo_block${B}_1.nii.gz \
      -rm $TMPDIR/histosm_2.nii.gz $TMPDIR/histo_block${B}_2.nii.gz \
      -rm $TMPDIR/histomsk.nii.gz $TMPDIR/mask_block${B}.nii.gz \
      -r \
        $M_WHOLE_TO_B2W_AFFINE,-1 $INV_MRI_WARP $M_BLOCK_TO_WHOLE_MANUAL \
        $M_BLOCK_TO_CANON,-1
SKIP
    c3d $BLOCK_MRI -stretch 0.1% 99.9% 0 1000 -clip 0 1000 -o $TMPDIR/block${B}_remap.nii.gz

    greedy -d 3 \
      -rf $WORK/mri_whole.nii.gz \
      -rm $TMPDIR/block${B}_remap.nii.gz $TMPDIR/blockmri${B}.nii.gz \
      -r \
        $M_WHOLE_TO_B2W_AFFINE,-1 $INV_MRI_WARP $M_BLOCK_TO_WHOLE_MANUAL

  done

  # Combine these blocks into one
 
  c3d \
    $TMPDIR/mask_block*.nii.gz -mean -replace 0 1 -reciprocal -popas S \
    $TMPDIR/histo_block*_0.nii.gz -mean -popas R \
    $TMPDIR/histo_block*_1.nii.gz -mean -popas G \
    $TMPDIR/histo_block*_2.nii.gz -mean -popas B \
    -push R -push G -push B \
    -foreach -push S -times -endfor \
    -omc $WORK/histo_blocks_to_whole_mri.nii.gz

  c3d \
    $TMPDIR/mask_block*.nii.gz -mean -replace 0 1 -reciprocal -popas S \
    $TMPDIR/blockmri*.nii.gz -mean -push S -times \
    -o $WORK/mri_blocks_to_whole_mri.nii.gz

}

function main()
{
  echo hi
}

if [[ $1 ]]; then
  CMD=$1
  shift
  $CMD $@
else
  main
fi
