#!/bin/bash
set -x -e

### This script is used to align John Pluta's annotation image to the 
### original space histology and to the MRI. It parses information from
### the XML files in HistoloZee

### Parameter 1: Path to the HistoloZee project
PROJ=${1?}

### Index of the slide to process (letter + 2 digit number)
BLOCK=${2?}
INDEX=${3?}

### Work directory
WORK=${4?}

# Function to resolve path relative to a path
function resolve_path()
{
  pushd "$2" > /dev/null
  greadlink -f "$1"
  popd > /dev/null
}

# Create some directories
mkdir -p $WORK/mat $WORK/slide $WORK/ann

# Get a list of all slides in this project
cat $PROJ | grep "slide image" | sed -e "s/.*meta-data=\"\(.*\)\".*/\1/g" \
  > $WORK/slides.txt

# For each slide, compute its Z position
ZSUM=0
for ((i=0;i<$(cat $WORK/slides.txt | wc -l);i++)); do

  SLIDE=$(resolve_path "$(cat $WORK/slides.txt | awk -v r=$i 'NR==r+1 {print $0}')" "$(dirname $PROJ)")

  THICK=$(cat "$SLIDE" | grep thick | sed -e "s/.*>\(.*\)<.*/\1/g")
  BEFORE=$(cat "$SLIDE" | grep spacing | sed -e "s/.*before=\"\([-0-9\.]*\)\".*/\1/")
  AFTER=$(cat "$SLIDE" | grep spacing | sed -e "s/.*after=\"\([-0-9\.]*\)\".*/\1/")

  ZSUM=$(echo $ZSUM $BEFORE | awk '{print $1+$2}')
  ZPOS[$i]=$ZSUM

  ZSUM=$(echo $ZSUM $THICK $AFTER | awk '{print $1+$2+$3}')

done

# Report slide info
SLIDE_REL=$(cat $WORK/slides.txt | awk -v r=$INDEX 'NR==r+1 {print $0}')
SLIDE=$(resolve_path "$SLIDE_REL" "$(dirname $PROJ)")
ZSLIDE=${ZPOS[$INDEX]}

MRI_REL=$(cat $PROJ | grep -A 10 "<reference-images>" | head -n 11 \
  | grep "image data" | sed -e "s/.*image data=\"\(.*\)\".*/\1/")

MRI=$(resolve_path $MRI_REL $(dirname $PROJ))

echo "Slide: $SLIDE"
echo "Z-Pos: $ZSLIDE" 
echo "MRI:   $MRI"

# ----------------------------------------------
# Compute the matrices for the slide
# ----------------------------------------------

# Shift in z, so slide is centered on Z
M_ZSHIFT=$WORK/mat/zshift.mat
c3d_affine_tool -tran 0 0 $ZSLIDE -o $M_ZSHIFT

# Scale by the pixel size of the slide 
M_PHYS=$WORK/mat/phys.mat
PIX_I=$(cat "$SLIDE" | grep pixel-size | sed -e "s/.*i=\"\([-0-9\.]*\)\".*/\1/")
PIX_J=$(cat "$SLIDE" | grep pixel-size | sed -e "s/.*j=\"\([-0-9\.]*\)\".*/\1/")
c3d_affine_tool -scale $PIX_I $PIX_J 1.0 -o $M_PHYS

# Apply the slide affine transform
M_SLIDE=$WORK/mat/slide.mat
AFF=($(cat $PROJ | grep -A 20 "$SLIDE_REL" \
  | grep "affine matrix" | head -n 1 \
  | sed -e "s/.*\[\(.*\)\].*/\1/" -e "s/;//g"))

cat <<-MAT1 > $M_SLIDE
  ${AFF[0]} ${AFF[1]} 0 ${AFF[2]}
  ${AFF[3]} ${AFF[4]} 0 ${AFF[5]}
  0 0 1 0
  0 0 0 1
MAT1

# Stack orientation matrix - hardcoded
M_ORIENT=$WORK/mat/orient.mat
cat <<-MAT2 > $M_ORIENT
-1 0 0 0 
0 0 -1 0
0 -1 0 0 
0 0 0 1
MAT2

# MRI matrix
M_MRI=$WORK/mat/mri.mat
cat $PROJ | grep -A 4 "World-to-image" | head -n 5 | tail -n 4 > $M_MRI

# Compute the complete matrix (voxel to voxel)
M_SLIDE_VOX_TO_MRI_VOX=$WORK/mat/slide_vox_to_mri_vox.mat
c3d_affine_tool -sform $MRI -inv $M_MRI $M_ORIENT $M_SLIDE $M_PHYS $M_ZSHIFT \
  -mult -mult -mult -mult -mult \
  -o $M_SLIDE_VOX_TO_MRI_VOX

echo "Mat:   $(cat $M_SLIDE_VOX_TO_MRI_VOX)"

# ----------------------------------------------
# Generate a low-resolution version of the slide
# ----------------------------------------------
LEVEL2=$WORK/slide/slide_x16.png
LEVEL3=$WORK/slide/slide_x64.png
SLIDE_NII=$WORK/slide/slide_neg.nii.gz

# use VIPS to read BigTiff file
SVS=$(echo $SLIDE | sed -e "s/.xml/.svs/g")

if [[ ! -f $LEVEL2 || ! -f $LEVEL3 ]]; then
  vips openslideload --level 2 "$SVS" $LEVEL2
  vips im_shrink $LEVEL2 $LEVEL3 4 4
fi

# convert to inverted nifti without alpha channel
c2d -mcs $LEVEL3 -pop -foreach -stretch 0 255 255 0 -origin 50% -endfor \
    -omc $SLIDE_NII

# If there is an SVG file for this sluce, convert it too
SVGIN=$(echo $SLIDE | sed -e "s/.xml/.svg/g")
SVG=$WORK/slide/seg_transp.svg

if [[ -f "$SVGIN" ]]; then

  # Process SVG for transparency
  cat "$SVGIN" | sed -e 's/stroke-width/fill-opacity="0.5" stroke-width/g' > $SVG

  # Dimensions of the reduced image (1100x777, e.g.)
  DIM_x64=$(identify $LEVEL3 | awk '{print $3}')

  # Dimensions of the SVG file
  DIM_SVG=$(identify "$SVG" | sed -e "s/.* SVG //g" | awk '{print $1}')

  # Compute the density factor with high precision
  DENSITY=$(echo ${DIM_x64}x${DIM_SVG} | awk -F 'x' '{printf "%18.16f\n", $1 * 72.0 / $3}')

  # Convert to PNG using the density factor
  # This command seems to time out sometimes. We will loop it
  for ((q=0;q<5;q++)); do
    gtimeout 5 convert -density $DENSITY "$SVG" $WORK/slide/svg_temp.png | cat
    if [[ -f $WORK/slide/svg_temp.png ]]; then break; fi
  done

  # Make sure the size matches precisely
  convert $WORK/slide/svg_temp.png -resize ${DIM_x64}! $WORK/slide/svg.png
  rm -rf $WORK/slide/svg_temp.png

  # Convert to a NIFTI 
  c2d -mcs $WORK/slide/svg.png -type ushort -oo $WORK/slide/svg_%d.png

  SLIDE_SVG=$WORK/slide/slide_seg.nii.gz
  c2d $SLIDE_NII -popas R \
    $WORK/slide/svg_0.png $WORK/slide/svg_1.png $WORK/slide/svg_2.png \
    -foreach -info -popas X -push R -push X -copy-transform -endfor \
    -type ushort -info \
    -omc $WORK/slide/slide_seg.nii.gz

  rm -rf $WORK/slide/svg_?.png

# TEMPORARY - DELETE LATER
#  M_MRI_TO_SLIDE=$WORK/mat/mri_to_slide_x64.mat
#  CAN_IMG=$WORK/canon/canonical_space.mha
#  greedy -d 3 -rf $CAN_IMG -rm $SLIDE_SVG $WORK/canon/slide_seg_to_canon.mha \
#    -r $M_MRI $M_MRI_TO_SLIDE,-1
fi

# ----------------------------------------------
# Transform MRI into the space of the slide
# ----------------------------------------------
RESLICE_MRI_TO_SLIDE_HZEE=$WORK/slide/reslice_mri_to_slide_hzee.nii.gz

# Scaling by 1/64
scale_64=$(echo 64 | awk '{print 1.0 / $1}')

M_MRI_TO_SLIDE=$WORK/mat/mri_to_slide_x64.mat
c3d_affine_tool \
  -sform $MRI \
  $M_SLIDE_VOX_TO_MRI_VOX \
  -scale 64 64 1 \
  -sform $SLIDE_NII -inv \
  -mult -mult -mult -o $M_MRI_TO_SLIDE

c3d $SLIDE_NII $MRI -reslice-matrix $M_MRI_TO_SLIDE -o $RESLICE_MRI_TO_SLIDE_HZEE

# ----------------------------------------------
# Match up the slide and the annotation
# ----------------------------------------------

# Find annotation
ANNID="${BLOCK}_$(printf %02d $INDEX)"
ANN=$(ls $(dirname $PROJ)/*annotation/*${ANNID}.png | cat)
ANN_NII=$WORK/ann/ann_neg.nii.gz

if [[ ! -f $ANN ]]; then
  echo "Missing annotation $ANNID"
else

  # The annotation may also be on the ignore list
  echo "Annot:  $ANN"

  # We don't know if the annotation has 3 or 4 components. We only
  # want the top 3 components. 
  c2d -mcs $ANN -oo $WORK/ann/ann_comp_%02d.nii.gz

  # Invert and convert to NIFTI
  c2d $WORK/ann/ann_comp_00.nii.gz \
      $WORK/ann/ann_comp_01.nii.gz \
      $WORK/ann/ann_comp_02.nii.gz \
      -foreach -stretch 0 255 255 0 -origin 50% -endfor \
      -omc $ANN_NII

  # Delete the intermediates
  rm -rf $WORK/ann/ann_comp_??.nii.gz

  # Rotate the slide into the annotation space
  M_SLIDE_TO_ANN_INIT=$WORK/ann/slide_to_ann_init_noscale.mat
  M_SLIDE_TO_ANN_INIT_3D=$WORK/ann/slide_to_ann_init_noscale_3d.mat
  c3d_affine_tool $M_SLIDE $M_PHYS -scale 64 64 1 $M_ZSHIFT \
    -mult -mult -mult \
    -inv -scale 0.04 0.04 1.0 -mult -o $M_SLIDE_TO_ANN_INIT_3D \
    -info \
      | tail -n 4 | awk 'NR < 4 {print $1,$2,$3}' \
      > $M_SLIDE_TO_ANN_INIT
  # Match up the dimensions of the slides
  DIM_ANN=$(c2d $ANN_NII -as A -trim 0vox -info-full \
    | grep Dimensions | sed -e "s/.*\[//" -e "s/\].*//" -e "s/,//g")

  DIM_SLD=$(c2d $ANN_NII $SLIDE_NII -reslice-matrix $M_SLIDE_TO_ANN_INIT \
    -trim 0vox -info-full \
    | grep Dimensions | sed -e "s/.*\[//" -e "s/\].*//" -e "s/,//g")

  ANN_SCALE_X=$(echo $DIM_ANN $DIM_SLD | awk '{print $3 * 1.0 / $1}')
  ANN_SCALE_Y=$(echo $DIM_ANN $DIM_SLD | awk '{print $4 * 1.0 / $2}')

  # Create a better initialization
  M_SLIDE_TO_ANN_INIT_WSCALE=$WORK/ann/slide_to_ann_init.mat
  c3d_affine_tool $M_SLIDE_TO_ANN_INIT_3D -scale $ANN_SCALE_X $ANN_SCALE_Y 1 -mult \
    -info \
      | tail -n 4 | awk 'NR < 4 {print $1,$2,$3}' \
      > $M_SLIDE_TO_ANN_INIT_WSCALE

  # Peform affine registration between the two images
  M_SLIDE_TO_ANN=$WORK/ann/slide_to_ann_affine.mat
  greedy -d 2 \
    -o $M_SLIDE_TO_ANN -i $ANN_NII $SLIDE_NII -n 60x60x60x60 \
    -a -ia $M_SLIDE_TO_ANN_INIT_WSCALE

  # Create a corresponding 3D affine matrix
  M_SLIDE_TO_ANN_3D=$WORK/ann/slide_to_ann_affine_3d.mat
  cat $M_SLIDE_TO_ANN \
    | awk 'NR == 3 {print 0,0,1,0} {print $1,$2,0,$3}' \
    > $M_SLIDE_TO_ANN_3D 

  # Apply affine registration to the slide and to the MRI
  RESLICE_SLIDE_TO_ANN_AFFINE=$WORK/ann/reslice_slide_to_ann_affine.nii.gz
  RESLICE_MRI_TO_ANN_HZEE=$WORK/ann/reslice_mri_to_ann_hzee.nii.gz

  # Reslice the slice to ANN using interpolaiton - and subtract to see differnces
  greedy -d 2 \
    -rf $ANN_NII \
    -rm $SLIDE_NII $RESLICE_SLIDE_TO_ANN_AFFINE \
    -r $M_SLIDE_TO_ANN 

  c2d -mcs $ANN_NII $RESLICE_SLIDE_TO_ANN_AFFINE \
    -wsum 0.30 0.59 0.11 -0.30 -0.59 -0.11 \
    -o $WORK/ann/ann_markup.nii.gz

  # Reslice the block MRI to the annotation
  greedy -d 3 \
    -rf $ANN_NII \
    -rm $MRI $RESLICE_MRI_TO_ANN_HZEE \
    -r $M_SLIDE_TO_ANN_3D $M_MRI_TO_SLIDE

fi 

# ----------------------------------------------
# Generate the canonical space 
# ----------------------------------------------

# Determine the center of the MRI in canonical space
CTR=($(c3d $MRI -verbose -probe 50% | grep Phys | sed -e "s/.*\[//" -e "s/\].*//" -e "s/,//g"))
CTR_CAN=($(\
  c3d_affine_tool $M_MRI -inv -info | tail -n 4 | head -n 3 | \
    awk -v i=${CTR[0]} -v j=${CTR[1]} -v k=${CTR[2]} '{print $1 * i + $2 * j + $3 * k + $4}'))

# Generate the canonical space
CANDIM=1200

CANSPCX=$(echo $PIX_I | awk '{print 64 * $1}')
CANSPCY=$(echo $PIX_J | awk '{print 64 * $1}')

CANORX=$(echo $PIX_I $CANDIM ${CTR_CAN[0]} | awk '{print -$3 - $1 * 0.5 * 64 * $2}')
CANORY=$(echo $PIX_J $CANDIM ${CTR_CAN[2]} | awk '{print $1 * 0.5 * 64 * $2 + $3}')
CANORZ=$(echo $ZSLIDE | awk '{print $1 - 0.1}')

# Because of an ITK/NIFTI bug that does not allow 1 voxel thick images to be stored
# as multi-component 3D images, we use compressed MHA files here
mkdir -p $WORK/canon
CAN_IMG=$WORK/canon/canonical_space.mha

c3d -create ${CANDIM}x${CANDIM}x1 ${CANSPCX}x${CANSPCY}x0.2mm \
  -orient RSA -origin ${CANORX}x${CANORZ}x${CANORY}mm \
  -compress -o $CAN_IMG 

greedy -d 3 -rf $CAN_IMG -rm $MRI $WORK/canon/mri_block_to_canon.mha -r $M_MRI

greedy -d 3 -rf $CAN_IMG -rm $SLIDE_NII $WORK/canon/slide_to_canon.mha \
  -r $M_MRI $M_MRI_TO_SLIDE,-1

# Transform the annotation image into the canonical space
if [[ -f "$ANN" ]]; then
  greedy -d 3 -rf $CAN_IMG \
    -rm $ANN_NII $WORK/canon/ann_to_canon.mha \
    -r $M_MRI $M_MRI_TO_SLIDE,-1 $M_SLIDE_TO_ANN_3D,-1 
fi

# Transform the SVG segmentation to the cannonical space
if [[ -f "$SVG"  ]]; then

  greedy -d 3 -rf $CAN_IMG -rm $SLIDE_SVG $WORK/canon/slide_seg_to_canon.mha \
    -r $M_MRI $M_MRI_TO_SLIDE,-1

fi

