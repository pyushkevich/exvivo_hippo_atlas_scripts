#!/bin/bash
BLOCKS=$(cat /Volumes/Histology/py_which_proj.txt | grep zee | awk '{printf "%s:%s\n",$1,$2}')

for B in $BLOCKS; do

  bash process_block.sh $(echo $B | awk -F ':' '{printf "%s %s test\n",$1,$2}')

done
