#!/bin/bash
set -x -e

# Path
PATH=/Users/picsl/tk/lddmm/xc64rel:/Users/picsl/tk/bb3/Experimental/c3d/master/xc64rel:$PATH

### Parameter 1: sample ID
SAMPLE=${1?}

### Parameter 2: block ID
BLOCK=${2?}

### Parameter 3: outdir
OUTDIR=${3?}

# Work directory
WDIR=$OUTDIR/${SAMPLE}-block${BLOCK}
mkdir -p $WDIR

WMRI=$WDIR/mri
mkdir -p $WMRI

# Function to resolve path relative to a path
function resolve_path()
{
  pushd $2 > /dev/null
  greadlink -f $1
  popd > /dev/null
}

# ----------------------------------------------
# Find all the necessary files
# ----------------------------------------------
SERVER=/Volumes/Histology/

# Use the manually created file to find projects
PROJFILE=$(cat $SERVER/py_which_proj.txt | grep "^${SAMPLE} ${BLOCK}" | awk '{print $3}')
PROJ=$SERVER/$SAMPLE/$PROJFILE
MRIDIR=$SERVER/$SAMPLE/MRI

if [[ ! -f $PROJ ]]; then
  echo "Missing project file $PROJ"
  exit
fi

# Find the matrix : some lovely code here :)
M_BLOCK_TO_WHOLE_RIGID=$(\
  ls $MRIDIR/*whole*rigid.txt \
  | grep block | sed -e "s/\(.*\(...block.\).*\)/\1 \2/g" \
  | grep " .*${BLOCK}.*" | awk '{print $1}')

if [[ ! -f $M_BLOCK_TO_WHOLE_RIGID ]]; then
  echo "No rigid matrix found in $MRIDIR"
  exit
fi

# Find the whole-hippocampus NIFTI scan
IN_MRI_WHOLE=$MRIDIR/$(cat $SERVER/py_which_mri.txt | awk -v s=$SAMPLE '$1 == s {print $2}')
if [[ ! -f $IN_MRI_WHOLE ]]; then
  echo "Missing whole MRI $IN_MRI_WHOLE"
  exit
fi

# Find the segmentations
IN_SEG_HF=$SERVER/$(cat $SERVER/py_which_seg.txt | awk -v s=$SAMPLE '$1 == s {print $2}')
IN_SEG_DB=$SERVER/$(cat $SERVER/py_which_seg.txt | awk -v s=$SAMPLE '$1 == s {print $3}')

if [[ ! -f $IN_SEG_HF ]]; then
  echo "Missing segmentation $IN_SEG_HF"
  exit
fi

if [[ ! -f $IN_SEG_DB ]]; then
  echo "Missing segmentation $IN_SEG_DB"
  exit
fi

# Find the block MRI
IN_MRI_BLOCK_REL=$(cat $PROJ | grep "image data" | tail -n 1 | sed -e "s/.*data=\"\(.*\)\".*/\1/g")
IN_MRI_BLOCK=$(resolve_path $IN_MRI_BLOCK_REL $(dirname $PROJ))
if [[ ! -f $IN_MRI_BLOCK ]]; then
  echo "Missing block MRI $IN_MRI_BLOCK"
  exit
fi

MRI_WHOLE=$WMRI/mri_whole.nii.gz
MRI_BLOCK=$WMRI/mri_block.nii.gz
SEG_HF=$WMRI/seg_hf.nii.gz
SEG_DB=$WMRI/seg_db.nii.gz

cp -av $IN_MRI_BLOCK $MRI_BLOCK

# Whole MRI needs to be remapped to intensity range
c3d $IN_MRI_WHOLE -stretch 1% 99% 0 1000 -type short -o $MRI_WHOLE

# Segmentations need their headers fixed
c3d $MRI_WHOLE $IN_SEG_HF -copy-transform -o $SEG_HF
c3d $MRI_WHOLE $IN_SEG_DB -copy-transform -o $SEG_DB

# ----------------------------------------------
# List all the slices
# ----------------------------------------------
IDXLAST=$(cat $PROJ | grep index | tail -n 1 | sed -e "s/.* k = \([0-9][0-9]*\).*/\1/g")
NSLICES=$((IDXLAST+1))

# ----------------------------------------------
# Register the block MRI to the whole MRI
# ----------------------------------------------
MRI_B2W_RIGID=$WMRI/mri_block_to_whole_rigid.nii.gz
MRI_B2W_RIGID_MASK=$WMRI/mri_block_to_whole_rigid_mask.nii.gz
MRI_B2W_RIGID_MASK_HF=$WMRI/mri_block_to_whole_rigid_mask_hf.nii.gz

M_W2_B2W_AFFINE=$WMRI/whole_to_b2w_affine.mat
RES_W2_B2W_AFFINE=$WMRI/reslice_whole_to_b2w_affine.nii.gz

WARP_W2_B2W=$WMRI/whole_to_b2w_warp.nii.gz
RES_W2_B2W_DEFORM=$WMRI/reslice_whole_to_b2w_warp.nii.gz

# Skip this slow stuff if already exists
if [[ -f $RES_W2_B2W_DEFORM ]]; then
  echo "Skipping MRI registration"
else

  # Apply rigid transform to the block, mapping it to whole space
  # also create a mask for the registration of the block
  c3d $MRI_WHOLE -popas W \
      $MRI_BLOCK -as B -pim R -thresh 10% inf 1 0 \
      -pad 10x10x10 10x10x10 0 \
      -dilate 1 8x8x8 -dilate 0 10x10x10 -popas BM \
      -push W -push B -reslice-matrix $M_BLOCK_TO_WHOLE_RIGID -o $MRI_B2W_RIGID \
      -push W -push BM -int 0 -reslice-matrix $M_BLOCK_TO_WHOLE_RIGID \
      -o $MRI_B2W_RIGID_MASK -info \
      -dup $SEG_HF -copy-transform \
      -thresh 0.5 inf 1 0 -dilate 1 5x5x5 -info -times \
      -o $MRI_B2W_RIGID_MASK_HF

  # Affine registration between block and whole
  greedy -d 3 \
    -i $MRI_B2W_RIGID $MRI_WHOLE \
    -gm $MRI_B2W_RIGID_MASK \
    -o $M_W2_B2W_AFFINE \
    -n 100x100x0 -a -ia-identity -m NCC 4x4x4

  # Apply affine registration
  greedy -d 3 \
    -rf $MRI_B2W_RIGID -rm $MRI_WHOLE $RES_W2_B2W_AFFINE \
    -ri NN -r $M_W2_B2W_AFFINE

  # Perform deformable registration
  greedy -d 3 \
    -i $MRI_B2W_RIGID $MRI_WHOLE \
    -gm $MRI_B2W_RIGID_MASK \
    -o $WARP_W2_B2W \
    -n 100x100x0 -s 8vox 0.5vox -m NCC 4x4x4 -e 0.25 \
    -it $M_W2_B2W_AFFINE

  # Apply deformable and affine registration
  greedy -d 3 \
    -rf $MRI_B2W_RIGID -rm $MRI_WHOLE $RES_W2_B2W_DEFORM \
    -ri NN -r $WARP_W2_B2W $M_W2_B2W_AFFINE

fi

# Compute the NCC metric between the three approaches
NCC_SRC=($MRI_WHOLE $RES_W2_B2W_AFFINE $RES_W2_B2W_DEFORM)
NCC_HEADING=("Rigid" "Affine" "Deform")

echo "Mode     NCC_OUTER   NCC_HF" > $WMRI/ncc_stat.txt
for ((i=0;i<${#NCC_SRC[*]};i++)); do

echo ${NCC_HEADING[i]} $(\
    c3d $MRI_B2W_RIGID ${NCC_SRC[i]} \
    -ncc 4x4x4 -replace NaN 0 Inf 0 -Inf 0 \
    $MRI_B2W_RIGID_MASK $MRI_B2W_RIGID_MASK_HF -add \
    -lstat | tail -n 2 | awk '{print $2}') >> $WMRI/ncc_stat.txt

done



# ----------------------------------------------
# Process the histology slices and annotations,
# mapping block MRI into their space
# ----------------------------------------------
WHIST=$WDIR/hist

for((i=0;i<$NSLICES;i++)); do

  # Perform the slice tasks
  BID=$(printf "%s%02d" $BLOCK $i)

  # Check if this slice is on the ignore list
  CHECK=$(cat $SERVER/py_which_ignore.txt | grep $SAMPLE | grep $BID | cat)
  if [[ $CHECK ]]; then
    echo "Skipping slide $BID"
    continue
  fi

  # Whole MRI mapped to canonical space
  WHOLE_TO_CANON=$WHIST/$BID/canon/wholemri_to_canon.mha

  # Skip slice if it already has been done
  ALLOW_SKIP=0
  if [[ $ALLOW_SKIP -eq 1 && -f $WHOLE_TO_CANON ]]; then
    echo "Skipping slide $BID - already done"
  else

    echo "Processing slide $BID"

    # Create a dump directory
    mkdir -p $WHIST/$BID/dump

    # Map the slide and block MRI to the annotation
    sh ./mri_to_annot.sh $PROJ $BLOCK $i $WHIST/$BID \
      > $WHIST/$BID/dump/mri_to_annot.$$ 2>&1

    # Transform the full MRI into the canonical space
    greedy -d 3 \
      -rf $WHIST/$BID/canon/canonical_space.mha \
      -rm $MRI_WHOLE $WHOLE_TO_CANON \
      -ri LABEL 0.2vox \
      -rm $SEG_HF $WHIST/$BID/canon/seg_hf_to_canon.mha \
      -rm $SEG_DB $WHIST/$BID/canon/seg_db_to_canon.mha \
      -r $WHIST/$BID/mat/mri.mat \
	 $M_BLOCK_TO_WHOLE_RIGID,-1 \
	 $WARP_W2_B2W  $M_W2_B2W_AFFINE

    # Make sure the annotation exists
    if [[ ! -f $WHIST/$BID/ann/ann_neg.nii.gz ]]; then
      continue
    fi

  fi

done

# ----------------------------------------------
# Define a canonical space for the whole block
# ----------------------------------------------

# Define the 3D canonical space

# Find the first slice
CFIRST=$(ls $WDIR/hist/*/canon/canonical_space.mha | head -n 1)
CLAST=$(ls $WDIR/hist/*/canon/canonical_space.mha | tail -n 1)

# How many slices to pad by
NPAD=$(c3d $CFIRST $CLAST \
  -foreach -probe 50% -endfor \
  | awk '{k[NR]= $6 / -0.2} END {printf "%d\n",(k[2]-k[1]+0.5)}')

mkdir -p $WDIR/canon

CAN3D=$WDIR/canon/canonical_space.nii.gz

c3d $CFIRST \
  -pad 0x0x0 0x0x${NPAD} 0 \
  -type uchar -o $CAN3D

# Transform the full MRI into the canonical space
MFIRST=$(ls $WDIR/hist/*/mat/mri.mat | head -n 1)
greedy -d 3 \
  -rf $CAN3D \
  -rm $MRI_WHOLE $WDIR/canon/wholemri_to_canon.nii.gz \
  -ri LABEL 0.2vox \
  -rm $SEG_HF $WDIR/canon/seg_hf_to_canon.nii.gz \
  -rm $SEG_DB $WDIR/canon/seg_db_to_canon.nii.gz \
  -r $MFIRST \
     $M_BLOCK_TO_WHOLE_RIGID,-1 \
     $WARP_W2_B2W  $M_W2_B2W_AFFINE

# Copy this important matrix to the Canon directory for future use
cp -av $MFIRST $WDIR/canon/block_to_canon.mat
cp -av $M_BLOCK_TO_WHOLE_RIGID $WDIR/mri/block_to_whole_rigid_manual.mat












