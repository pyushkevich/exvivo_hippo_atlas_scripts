#!/bin/bash
# This script is to quickly apply mri_to_annot to all previously processed histology slides
for fn in $(ls test/*/hist/*/slide/slide_neg.nii.gz); do

  BLOCK=$(echo $fn | awk -F '/' '{print $2}')
  SLIDE=$(echo $fn | awk -F '/' '{print $4}')

  ID=$(echo $BLOCK | sed -e "s/-block.*//")
  LETTR=$(echo $BLOCK | sed -e "s/.*block//")
  NUMBR=$(echo $SLIDE | sed -e "s/^${LETTR}//")

  PROJ=$(cat /Volumes/Histology/py_which_proj.txt | grep "^${ID} ${LETTR}" | awk '{print $3}')

  echo bash mri_to_annot.sh /Volumes/Histology/${ID}/${PROJ} $LETTR $NUMBR test/$BLOCK/hist/$SLIDE

  if [[ ! -f test/$BLOCK/hist/$SLIDE/slide/slide_seg.nii.gz || ! -f test/$BLOCK/hist/$SLIDE/canon/slide_seg_to_canon.mha ]]; then
    bash mri_to_annot.sh /Volumes/Histology/${ID}/${PROJ} $LETTR $NUMBR test/$BLOCK/hist/$SLIDE > /dev/null
  fi

done
