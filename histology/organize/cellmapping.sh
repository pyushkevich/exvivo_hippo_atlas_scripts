#!/bin/bash

# Script: cell segmentation and density estimation

### Parameter 1: Path to the HistoloZee project
PROJ=${1?}

### Index of the slide to process (letter + 2 digit number)
BLOCK=${2?}
INDEX=${3?}

### Work directory
WORK=${4?}

# Environment
PATH=/Users/picsl/tk/lddmm/xc64rel:/Users/picsl/tk/bb3/Experimental/c3d/master/xc64rel:$PATH

### The slides.txt file must exist
if [[ ! -f $WORK/slides.txt ]]; then
  echo "Missing slides.txt"
  exit -1
fi

# Function to resolve path relative to a path
function resolve_path()
{
  pushd "$2" > /dev/null
  greadlink -f "$1"
  popd > /dev/null
}

# Function that prints the origin and size of a 2d image
function orig_size()
{
  ORIG=$(c2d $1 -probe 0x0vox | awk '{print $5,$6}')
  SIZE=$(c2d $1 -info-full | grep Dimensions | sed -e "s/.*\[//" -e "s/\]//" -e "s/,//")
  echo $ORIG $SIZE
}

# Get the corresponding SVS file
SLIDE_REL=$(cat $WORK/slides.txt | awk -v r=$INDEX 'NR==r+1 {print $0}')
SLIDE=$(resolve_path "$SLIDE_REL" "$(dirname $PROJ)")

# Get the to the svs file
SVS=$(echo $SLIDE | sed -e "s/.xml/.svs/g")

# We will extract little tiles from the SVS file. We determine which tiles to
# use based on the histology segmentation. In the future we might want to use 
# MRI segmentation or something else in case segmentation is missing

# This is the reference space
SEGNII=$WORK/slide/slide_seg.nii.gz
if [[ ! -f $SEGNII ]]; then
  echo "Missing segmentation $SEGNII"
  exit -1
fi

# Create the cell mapping directory
TDEFDIR=$WORK/cells/tiledef
mkdir -p $TDEFDIR

# Tile size: 50x50 (64x) voxels, i.e., 3200x3200
c2d -mcs $SEGNII \
  -accum -min -endaccum -thresh 0 65534 1 0 -dilate 1 25x25 -as A \
  -int 0 -resample-mm 50x50mm -as B \
  -cmv -foreach -push B -times -insert A 1 -reslice-identity -endfor \
  -scale 100 -add \
  -split -foreach -trim 4x4vox -info -endfor \
  -oo $TDEFDIR/tdef_%04d.nii.gz

# Remove the zero tile - it's background
rm -rf $TDEFDIR/tdef_0000.nii.gz

# Get the origin of the overall segmentation image
OD_SEGNII=($(orig_size $SEGNII))
echo ${OD_SEGNII[*]}

# Repeat for each little tile
for tdef in $(ls $TDEFDIR | grep "^tdef_" | grep 0013); do

  # Get the origin and size of the tile in SEGNII space
  OD_TDEF=($(orig_size $TDEFDIR/$tdef))
	echo ${OD_TDEF[*]}

  # Create a directory for this tile
	TILEID=$(echo $tdef | sed -e "s/tdef_//" -e "s/.nii.gz//")
	TILEDIR=$WORK/cells/tiles/tile_${TILEID}

	mkdir -p $TILEDIR

  # Now that we have tiles, we will use python to extract the image. But for
  # this we need to map each tile into proper coordinate system
  python2 - <<____PROGRAM

import openslide;

S=openslide.OpenSlide("$SVS")

# Compute scaling factors
fx = S.dimensions[0] * 1.0 / ${OD_SEGNII[2]};
fy = S.dimensions[1] * 1.0 / ${OD_SEGNII[3]};

# Compute the staring index of the block
locx=int(round((-${OD_TDEF[0]}+${OD_SEGNII[0]}) * fx));
locy=int(round((-${OD_TDEF[1]}+${OD_SEGNII[1]}) * fy));

print "loc = %d, %d" % (locx,locy);

# Compute the size of the block
szx=int(round(${OD_TDEF[2]} * fx));
szy=int(round(${OD_TDEF[3]} * fy));

# Read the region of the image
I = S.read_region((locx, locy), 0, (szx, szy));

# Write the region as a PNG file
I.save("$TILEDIR/tile_${TILEID}.png")

____PROGRAM

  # Take the PNG image and turn it into a NIFTI with a real header
	c2d -verbose $TDEFDIR/$tdef -popas R \
	  -mcs $TILEDIR/tile_${TILEID}.png -pop \
		-foreach -insert R 1 -mbb -endfor \
		-type ushort -omc $TILEDIR/tile_${TILEID}.nii.gz

  # Delete the PNG
	rm -rf $TILEDIR/tile_${TILEID}.png 


exit
done
