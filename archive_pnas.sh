#!/bin/bash
# This creates an archive of analysis outputs for the PNAS paper
ROOT=/data/picsl/pauly/dan2015/atlas2016

for id in $(cat subj.txt); do

  # This is the output directory for this specimen
  WORK=/data/picsl/pauly/dan2015/pnas_data/organized/specimens/${id}

  # Create the input subdirectories
  OUTMRI=$WORK/input/mri
  mkdir -p $OUTMRI/native $OUTMRI/manseg $OUTMRI/distcorr

  # This is my preproc directory
  MYPP=$ROOT/preproc/${id}

  # Copy the raw MRI
  IN_RAW=$(readlink -f $MYPP/${id}_raw.nii.gz)
  IN_RAW_IMG=$MYPP/${id}_raw_n4clip.nii.gz

  cp -Luv $IN_RAW $OUTMRI/native/${id}_mri_native_raw.nii
  cp -Luv $IN_RAW_IMG $OUTMRI/native/${id}_mri_native_n4clip.nii.gz

  # Copy the segmentation space images and unwarp images
  for what in dbseg hfseg img; do
    cp -Luv $MYPP/${id}_danseg_${what}.nii.gz $OUTMRI/manseg/${id}_manseg_${what}.nii.gz
    cp -Luv $MYPP/unwarp/${id}_unwarp_${what}.nii.gz $OUTMRI/distcorr/${id}_distcorr_${what}.nii.gz
  done
  cp -Luv $MYPP/${id}_danseg_to_raw.mat $OUTMRI/manseg/${id}_manseg_to_native_affine.mat
  cp -Luv $MYPP/unwarp/${id}_unwarp.mat $OUTMRI/distcorr/${id}_native_to_distcorr_affine.mat

  # Copy the images resliced into the MST template
  OUTMST=$WORK/template_build/mst/
  mkdir -p $OUTMST

  for what in db hf img; do
    cp -Luv $ROOT/mst/paths/${id}/final/reslice_${id}_to*_${what}.nii.gz \
      $OUTMST/reslice_${id}_to_mstroot_${what}.nii.gz
  done

  # Create a warp between unwarped space and mst root space
  COMBOWARP=$OUTMST/${id}_distcorr_to_mstroot_warp.nii.gz
  if [[ ! -f $COMBOWARP ]]; then
    greedy -d 3 -rf $ROOT/mst/paths/${id}/final/reslice_${id}_to*_db.nii.gz \
      -rc $COMBOWARP -r $(cat $ROOT/mst/paths/${id}/final/chain_unwarp_to_final.txt) \
      -wp 0.001
  fi

  # Copy images resliced to the geodesic shooting template
  OUTSHOOT=$WORK/template_build/gshoot
  mkdir -p $OUTSHOOT

  for what in db hf img; do
    cp -Luv $ROOT/mst/gshoot/${id}/iter_4/reslice_${id}_shooting_to_template_${what}.nii.gz \
      $OUTSHOOT/reslice_${id}_to_gshoot_template_${what}.nii.gz
  done

  # Create warp from distortion correction space to gshoot template
  GFULLWARP=$OUTSHOOT/${id}_distcorr_to_gshoot_template_warp.nii.gz
  if [[ ! -f $GFULLWARP ]]; then
    greedy -d 3 -rf $ROOT/mst/gshoot/${id}/refspace.nii.gz \
      -rc $GFULLWARP \
      -r $ROOT/mst/gshoot/${id}/iter_4/shooting_warp.nii.gz \
         $ROOT/mst/gshoot/${id}/iter_4/target_to_root_procrustes.mat,-1 \
      -wp 0.001
  fi

  # Copy images resliced to NCC template
  OUTNCC=$WORK/template_build/ncc
  mkdir -p $OUTNCC

  for what in db hf img; do
    cp -Luv $ROOT/mst/template_ncc/iter5/reslice_to_template_${id}_${what}.nii.gz \
      $OUTNCC/reslice_${id}_to_ncc_template_${what}.nii.gz
  done

  # Create warp from distortion correction space to ncc template
  NCCFULLWARP=$OUTNCC/${id}_distcorr_to_ncc_template_warp.nii.gz
  if [[ ! -f $NCCFULLWARP ]]; then
    greedy -d 3 -rf $ROOT/mst/template_ncc/iter5/template_hf.nii.gz \
      -rc $NCCFULLWARP \
      -r $ROOT/mst/template_ncc/iter5/warp_${id}.nii.gz \
         $ROOT/mst/gshoot/${id}/iter_4/shooting_warp.nii.gz \
         $ROOT/mst/gshoot/${id}/iter_4/target_to_root_procrustes.mat,-1 \
      -wp 0.001
  fi

  # Copy images resliced based on affine transform only
  OUTFOILAFFINE=$WORK/template_build/foil_affine_only
  mkdir -p $OUTFOILAFFINE

  for what in db hf img; do
    cp -Luv \
      $ROOT/mst/template_aff_ncc/input/${id}/reslice_to_template_${id}_${what}.nii.gz \
      $OUTFOILAFFINE/reslice_${id}_to_affine_template_${what}.nii.gz
  done
  cp -Luv $ROOT/mst/template_aff_ncc/input/${id}/proc_affine_${id}.mat \
    $OUTFOILAFFINE/${id}_distcorr_to_affine_template_affine.mat

  # Copy images resliced based on affine and ncc
  OUTFOILNCC=$WORK/template_build/foil_affine_ncc
  mkdir -p $OUTFOILNCC

  for what in db hf img; do
    cp -Luv \
      $ROOT/mst/template_aff_ncc/iter5/reslice_to_template_${id}_${what}.nii.gz \
      $OUTFOILNCC/reslice_${id}_to_affine_ncc_template_${what}.nii.gz
  done

  # Create warp from distortion correction space to ncc template
  FOILNCCFULLWARP=$OUTFOILNCC/${id}_distcorr_to_affine_ncc_template_warp.nii.gz
  if [[ ! -f $FOILNCCFULLWARP ]]; then
    greedy -d 3 -rf $ROOT/mst/template_aff_ncc/iter5/template_hf.nii.gz \
      -rc $FOILNCCFULLWARP \
      -r $ROOT/mst/template_aff_ncc/iter5/warp_${id}.nii.gz \
         $ROOT/mst/template_aff_ncc/input/${id}/proc_affine_${id}.mat \
      -wp 0.001
  fi

  # Axis aligned images
  for kind in rigid similarity; do
    AADIR=$WORK/axis_align/$kind
    AASRCDIR=$ROOT/histolabels/axisalign/${id}/${kind}/
    mkdir -p $AADIR

    cp -Luv $AASRCDIR/*.nii.gz $AADIR/

  done

  if [[ -f $ROOT/histolabels/${id}_lwseg_to_template.nii.gz ]]; then

    # Histo to template
    H2TDIR=$WORK/histo_to_template
    mkdir -p $H2TDIR

    cp -Luv $ROOT/histolabels/${id}_lwseg_to_template.nii.gz $H2TDIR/${id}_histoseg_to_template.nii.gz
    cp -Luv $ROOT/histolabels/${id}_lwseg_to_template_mask.nii.gz $H2TDIR/${id}_histoseg_mask_to_template.nii.gz

  fi

  THSDIR=$WORK/template_histo_seg
  mkdir -p $THSDIR

  cp -Luv $ROOT/histolabels/tosubj/${id}/${id}_histoseg.nii.gz    $THSDIR/${id}_histoseg_distcorr_space.nii.gz
  cp -Luv $ROOT/histolabels/tosubj/${id}/${id}_histoseg_ap.nii.gz $THSDIR/${id}_histoseg_ap_distcorr_space.nii.gz

  # Copy jacobian
  mkdir -p $WORK/jacobian
  cp -Luv $ROOT/histolabels/jacobian/subj/jacobian_${id}.nii.gz $WORK/jacobian/${id}_jacobian_template_to_distcorr_space.nii.gz


done

# Copy the template stuff
TWORK=/data/picsl/pauly/dan2015/pnas_data/organized/templates

mkdir -p $TWORK/template_build/mst
cp -Luv $ROOT/mst/template_mst/*.nii.gz $TWORK/template_build/mst/

mkdir -p $TWORK/template_build/gshoot
cp -Luv $ROOT/mst/template_gshoot/*.nii.gz $TWORK/template_build/gshoot/

mkdir -p $TWORK/template_build/ncc
mkdir -p $TWORK/template_build/foil_affine_only
mkdir -p $TWORK/template_build/foil_affine_ncc
mkdir -p $TWORK/final_template

for what in img hf db; do
  cp -Luv $ROOT/mst/template_ncc/iter5/template_${what}.nii.gz $TWORK/template_build/ncc/template_ncc_${what}.nii.gz
  cp -Luv $ROOT/mst/template_ncc/iter5/template_${what}.nii.gz $TWORK/final_template/template_${what}.nii.gz
  cp -Luv $ROOT/mst/template_aff_ncc/iter5/template_${what}.nii.gz $TWORK/template_build/foil_affine_ncc/template_foil_affine_ncc_${what}.nii.gz
  cp -Luv $ROOT/mst/template_aff_ncc/iter0/template_${what}.nii.gz $TWORK/template_build/foil_affine_only/template_foil_affine_only_${what}.nii.gz
done

cp -Luv $ROOT/histolabels/template_histo_seg_final.nii.gz $TWORK/final_template/template_histo_seg.nii.gz
cp -Luv $ROOT/histolabels/template_histo_seg_ap_final.nii.gz $TWORK/final_template/template_histo_seg_ap.nii.gz
cp -Luv $ROOT/histolabels/entropy_masked.nii.gz $TWORK/final_template/template_histo_entropy_map.nii.gz

mkdir -p $TWORK/thickness/subj $TWORK/thickness/lmer
for id in $(ls $ROOT/histolabels/thickness/subj); do
  mkdir -p $TWORK/thickness/subj/${id}
  cp -Luv $ROOT/histolabels/thickness/subj/$id/*.vtk $TWORK/thickness/subj/${id}/
done
cp -LuvR $ROOT/histolabels/thickness/lmer/*.vtk $TWORK/thickness/lmer/
for what in dg db gm; do
  cp -Luv $ROOT/histolabels/thickness/template_${what}_tsm_nopd.vtk $TWORK/thickness/template_${what}_surface_smoothed.vtk
done

mkdir -p $TWORK/shape/momenta 
for what in mode1 mode2 mode3 mode4 mode5 svm; do
  cp -Luv $ROOT/histolabels/shape/${what}_vector_*.vtk $TWORK/shape/momenta/
  mkdir -p $TWORK/shape/image_movies/movie_${what}
  cp -Luv $ROOT/histolabels/shape/template/movie_${what}/*.nii.gz $TWORK/shape/image_movies/movie_${what}/
  mkdir -p $TWORK/shape/mesh_movies/movie_${what}
  cp -Luv $ROOT/histolabels/shape/labels/movie_${what}/*.vtk $TWORK/shape/mesh_movies/movie_${what}/
  mkdir -p $TWORK/shape/warp/movie_${what}
  cp -Luv $ROOT/histolabels/shape/warp/movie_${what}/*.nii.gz $TWORK/shape/warp/movie_${what}/
done

# Histology copy
HWORK=/data/picsl/pauly/dan2015/pnas_data/organized/histology
HROOT=/data/picsl/pauly/dan2015/histology
mkdir -p $HWORK/blocks/histo_seg_block $HWORK/whole

for block in $(ls $HROOT/data | grep block); do

  # This block and all its histology was mislabeled (65231-L is really 64823-L)
  # Let's be consistent with INDD ids
  block_rename=$(echo $block | sed -e "s/65231-L/64823-L/g" -e "s/IND1/INDD1/g")

  mkdir -p $HWORK/blocks/${block_rename}/histo_seg_block

  # Copy the data for this block (there are no filenames to rename)
  cp -LuvR $HROOT/data/$block/* $HWORK/blocks/${block_rename}/

  if [[ -d $HROOT/lwseg_atlas/work/$block ]]; then
    for fn in $(ls $HROOT/lwseg_atlas/work/$block); do

      fn_rename=$(echo $fn | sed -e "s/65231-L/64823-L/g" -e "s/IND1/INDD1/g")
      cp -Luv $HROOT/lwseg_atlas/work/$block/$fn $HWORK/blocks/${block_rename}/histo_seg_block/${fn_rename}

    done
  fi

done

# Copy laura's segmentations
for id in $(cat subj.txt); do
  
  LWEDITDIR=/data/picsl/lwisse/IND/histo_segmentations/Whole_MRI/
  LWSEG=$LWEDITDIR/wholemri_$id/${id}_lwseg_interp_masked_wholemri_LW.nii.gz

  if [[ -f $LWSEG ]]; then

    id_rename=$(echo $id | sed -e "s/65231-L/64823-L/g" -e "s/IND1/INDD1/g")

    mkdir -p $HWORK/whole/wholemri_${id_rename}

    for fn in $(ls $HROOT/lwseg_atlas/work/wholemri_${id}); do
      fn_rename=$(echo $fn | sed -e "s/65231-L/64823-L/g" -e "s/IND1/INDD1/g")
      cp -Luv $HROOT/lwseg_atlas/work/wholemri_${id}/$fn $HWORK/whole/wholemri_${id_rename}/$fn_rename
    done
    cp -Luv $LWSEG $HWORK/whole/wholemri_${id_rename}/${id_rename}_lwseg_interp_masked_wholemri_touchup.nii.gz
  fi

done





