---
title: "PCA and SVM"
author: "Paul Yushkevich"
date: "4/18/2017"
output: html_document
---

```{r}
require(dplyr);
require(MASS);
require(ggplot2);
```

Linear Discriminant Analysis on Momentum
======

Load the PMC datafile

```{r}
# Download the PMC spreadsheet
download.file("https://docs.google.com/spreadsheets/d/1wLQ2Se9zT85ISMb-kbupkT6mim8ZhaELn5fnA_Qa6Qc/export?format=csv", destfile="/tmp/exvivo_vols.csv", method="wget");

X<-read.csv("/tmp/exvivo_vols.csv",header = T, na.strings = "");
```

Load the velocity data and associate with group/age
```{r}
X.mom.raw<-read.table("../../histolabels/shape/initial_momenta.txt", header = FALSE);
names(X.mom.raw)<-c("ID","SIDE",paste('M',1:(dim(X.mom.raw)[2]-2),sep=''));
```

We are reading momenta, but we need to do the PCA/LDA on velocities
```{r}
# Read the mesh points
X.q <- read.table("../../histolabels/shape/template_points.txt", header = FALSE);

# Compute the kernel matrix
dmat = as.matrix(dist(X.q));
sigma.gs = 2.0;
G.dmat = exp(-dmat^2 / (2 * sigma.gs^2))

# Function to map velocities to momenta
momentum_to_velocity<-function (p)
{
  # Split into columns for x, y, z momentum
  pmat = matrix(as.double(p), ncol=3, byrow=TRUE);
  
  # Multiply by the G matrix
  vmat = cbind(G.dmat %*% as.matrix(pmat[,1]), 
               G.dmat %*% as.matrix(pmat[,2]), 
               G.dmat %*% as.matrix(pmat[,3]));
  
  # Flatten into a vector
  matrix(t(vmat), nrow=1)
}

# Function to map momenta to velocities
velocity_to_momentum<-function (v)
{
  # Split into columns for x, y, z momentum
  vmat = matrix(as.double(v), ncol=3, byrow=TRUE);
  
  # Multiply by the G matrix
  pmat = cbind(solve(G.dmat,as.matrix(vmat[,1])), 
               solve(G.dmat,as.matrix(vmat[,2])), 
               solve(G.dmat,as.matrix(vmat[,3])));
  
  # Flatten into a vector
  matrix(t(pmat), nrow=1)
}
```

Generate velocity arrays

```{r}
# This maps momentum to velocity
X.vel.raw = cbind(X.mom.raw[,1:2],
                  t(apply(X.mom.raw[,3:dim(X.mom.raw)[2]],1,momentum_to_velocity)));

# Add the other important columns
X.vel = merge(X[,c('ID','SIDE','STATUS','AGE')],X.vel.raw)
i.vel = 5:dim(X.vel)[2]

pca.vel = prcomp(X.vel[,i.vel], retx = T, center = F, scale. = F);
X.pca.vel = cbind(X.vel[,1:4], pca.vel$x);

pca.var = data.frame(Mode=1:length(pca.vel$sdev),
                     CumVar = cumsum(pca.vel$sdev^2) / sum(pca.vel$sdev^2));
ggplot(data=pca.var, aes(x=Mode,y=CumVar)) +
  geom_point() + theme_classic() + 
  geom_hline(aes(yintercept=0.95)) +
  labs(title="Variance explained by PCA modes")

ggplot(data=X.pca.vel, aes(x=PC1/pca.vel$sdev[1], y=PC2/pca.vel$sdev[2], color=STATUS)) +
  geom_point() + theme_classic() +
  labs(x="PCA Mode 1", y = "PCA Mode 2")
```
Perform LDA on first 10 PCA modes

```{r}
require(MASS)

k.modes=20;
c.modes = paste('PC',1:k.modes,sep='')
i.adctl = X.pca.vel$STATUS != 'OTHER';

X.disc = X.pca.vel[i.adctl,c.modes];
Y.disc = droplevels(X.pca.vel$STATUS[i.adctl]);

lda.ad = lda(x=X.disc, grouping = Y.disc);

# Cross-validation table
lda.ad.cv = lda(x=X.disc, grouping = Y.disc, CV=T);
table(Y.disc, lda.ad.cv$class)

```

Plot the PCA and linear discriminant
```{r}

p12.slope=lda.ad$scaling[2]/lda.ad$scaling[1];
p12.int = colMeans(lda.ad$means)[2] - colMeans(lda.ad$means)[1] * p12.slope;

ggplot(data=X.pca.vel, aes(x=PC1, y=PC2, color=STATUS)) +
  geom_point() + theme_classic() +
  geom_abline(slope = p12.slope, intercept = p12.int) + 
  labs(x="PCA Mode 1", y = "PCA Mode 2")
```

Support Vector Machine
```{r}
require(e1071)

# Train SVM
my.svm = svm(x=X.disc, y=Y.disc, cross=length(Y.disc),kernel='linear',scale=FALSE)
my.svm$tot.accuracy

# Hyperplane w
w = t(t(my.svm$coefs) %*% my.svm$SV)

```


Map the discriminant direction back into the original space
```{r}

vnorm=function(x)
{
  sqrt(sum((x)^2))
}

# This is the direction back in velocity space
Q=t(as.matrix(w)) %*% t(pca.vel$rotation[,1:k.modes])

# Scale it so that 1*Q = distance between means
mu.ad = colMeans(X.vel[X$STATUS=='AD',i.vel])
mu.ctl = colMeans(X.vel[X$STATUS=='CTL',i.vel]);
mu.diff = as.matrix(0.5 * (mu.ctl - mu.ad));
Q=Q * (vnorm(mu.diff) / vnorm(Q));

# Find the angle between the two
acos(sum((mu.diff / vnorm(mu.diff)) * t((Q / vnorm(Q)) ))) * (180/3.1415)

# Compare the two
plot(mu.ctl-mu.ad, Q)

# Create the momentum corresponding to Q
p.Q = velocity_to_momentum(Q)
```


Write out a VTK model
```{r}
pqm = matrix(p.Q, ncol=3, byrow = TRUE);

mm.ad = matrix(velocity_to_momentum(mu.ad), ncol=3, byrow = TRUE);
mm.ctl = matrix(velocity_to_momentum(mu.ctl), ncol=3, byrow = TRUE);

write.vtk<-function(p, file)
{
  sink(file)
  
  # Write VTK header
  cat("# vtk DataFile Version 4.0\n")
  cat("vtk output\n")
  cat("ASCII\n")
  cat("DATASET POLYDATA\n")
  cat(paste("POINTS", dim(p)[1], "float\n"))
  write.table(as.matrix(X.q),row.names=F,col.names=F);
  cat(paste("\nPOINT_DATA",dim(p)[1],"\n"));
  cat("FIELD FieldData 1\n");
  cat(paste("InitialMomentum 3",dim(p)[1],"double\n"));
  write.table(p,row.names=F,col.names=F);
  sink(NULL)
}

write.vtk(pqm, "../../histolabels/shape/svm_vector_pos.vtk")
write.vtk(-pqm, "../../histolabels/shape/svm_vector_neg.vtk")
write.vtk(mm.ad, "../../histolabels/shape/mean_ad.vtk")
write.vtk(mm.ctl, "../../histolabels/shape/mean_ctl.vtk")
```

Also write out the first couple PCA modes

```{r}
mom.mode1 = matrix(velocity_to_momentum(pca.vel$rotation[,1] * pca.vel$sdev[1]), 
                   ncol=3, byrow = TRUE);

mom.mode2 = matrix(velocity_to_momentum(pca.vel$rotation[,2] * pca.vel$sdev[2]), 
                   ncol=3, byrow = TRUE);

mom.mode3 = matrix(velocity_to_momentum(pca.vel$rotation[,3] * pca.vel$sdev[3]), 
                   ncol=3, byrow = TRUE);

mom.mode4 = matrix(velocity_to_momentum(pca.vel$rotation[,4] * pca.vel$sdev[4]), 
                   ncol=3, byrow = TRUE);

mom.mode5 = matrix(velocity_to_momentum(pca.vel$rotation[,5] * pca.vel$sdev[5]), 
                   ncol=3, byrow = TRUE);

write.vtk(mom.mode1, "../../histolabels/shape/mode1_vector_pos.vtk")
write.vtk(-mom.mode1, "../../histolabels/shape/mode1_vector_neg.vtk")

write.vtk(mom.mode2, "../../histolabels/shape/mode2_vector_pos.vtk")
write.vtk(-mom.mode2, "../../histolabels/shape/mode2_vector_neg.vtk")

write.vtk(mom.mode3, "../../histolabels/shape/mode3_vector_pos.vtk")
write.vtk(-mom.mode3, "../../histolabels/shape/mode3_vector_neg.vtk")

write.vtk(mom.mode4, "../../histolabels/shape/mode4_vector_pos.vtk")
write.vtk(-mom.mode4, "../../histolabels/shape/mode4_vector_neg.vtk")

write.vtk(mom.mode5, "../../histolabels/shape/mode5_vector_pos.vtk")
write.vtk(-mom.mode5, "../../histolabels/shape/mode5_vector_neg.vtk")
```
