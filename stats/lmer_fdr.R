# This script performs FDR correction on thickness p-values from LMER analysis

# Read the command line options
opts = commandArgs(trailingOnly = TRUE);

# Read the input table
data = read.table(opts[1], header=F, 
                  col.names=c("chi.ad","t.ad","p.ad","chi.age","t.age","p.age"));

# Perform FDR correction
data$padj.ad = p.adjust(data$p.ad, method="fdr");
data$padj.age = p.adjust(data$p.age, method="fdr");

# Save the data
write.table(data,opts[2],row.names=FALSE,col.names=TRUE)
