#!/bin/bash
set -x -e
ROOT=/data/picsl/pauly/dan2015
HDIR=$ROOT/histology/lwseg_atlas/work
ADIR=$ROOT/atlas2016
WORK=$ADIR/histolabels

SF_NAMES=("NULL" "CA1" "CA2" "DG" "CA3" "HATA" "SUB" "CYST" "MISC" "SRLM")

source $ADIR/scripts/common.sh

LWEDITDIR=/data/picsl/lwisse/IND/histo_segmentations/Whole_MRI/

TEMPLATEDIR=$ADIR/mst/template_ncc/iter5
TEMPLATE_AFF_DIR=$ADIR/mst/template_aff_ncc/iter5

mkdir -p $WORK

# Get Laura's histology segmentation for an ID from subj.txt
function get_lwseg()
{
  local id=${1?}

  # THIS is the most bizarre thing. The histology labeled as 65231-L actually 
  # correponds to the MRI scan 64823-L!!!
  local id_lw=$(echo $id | sed -e "s/NDRI64823/NDRI65231/g")

  # Laura used IND not INDD
  id_lw=$(echo $id_lw | sed -e "s/INDD/IND/");

  # Return Laura's segmentation
  local LWSEG=$LWEDITDIR/wholemri_$id_lw/${id_lw}_lwseg_interp_masked_wholemri_LW.nii.gz

  echo $LWSEG
}

function process_seg()
{
  # This is the subject ID
  id=${1?}

  # Registration mode for which this is being generated
  mode=${2?}

  # Get the histology segmentation filename
  LWSEG=$(get_lwseg $id)

  # Does Laura's edited segmentation exist?
  if [[ -f $LWSEG ]]; then
    SEG=$LWSEG
  else
    echo "MISSING SEGMENTATION!"
    exit -1
  fi

  # The reference images are from the input directory
  IMG=$ADIR/input/$id/${id}_img.nii.gz

  # Check that we have all files
  if [[ ! -f $IMG ]]; then
    echo "Missing segmentation"
    exit
  fi

  # Make sure we have the correct header 
  c3d $IMG $SEG -copy-transform -o $TMPDIR/${id}_seg.nii.gz

  # We also need to create a mask of the segmentation
  c3d $TMPDIR/${id}_seg.nii.gz -thresh 1 inf 1 0 -o $TMPDIR/${id}_mask.nii.gz

  # Set up the warpchains and working directories for the different
  # modes that are available
  local WDIR WARPCHAIN
  case "$mode" in 
    ncc)
      WDIR=$WORK
      WARPCHAIN="\
        $TEMPLATEDIR/warp_${id}.nii.gz \
        $ADIR/mst/gshoot/${id}/iter_4/shooting_warp.nii.gz \
        $ADIR/mst/gshoot/${id}/iter_4/target_to_root_procrustes.mat,-1"
      ;;
    aff_ncc)
      WDIR=$WORK/template_hist/$mode
      WARPCHAIN="\
        $TEMPLATE_AFF_DIR/warp_${id}.nii.gz \
        $ADIR/mst/template_aff_ncc/input/$id/proc_affine_${id}.mat"
      ;;
    mst)
      WDIR=$WORK/template_hist/$mode
      WARPCHAIN=$(cat $ADIR/mst/paths/$id/final/chain_unwarp_to_final.txt)
      ;;
    gshoot)
      WDIR=$WORK/template_hist/$mode
      WARPCHAIN="\
        $ADIR/mst/gshoot/${id}/iter_4/shooting_warp.nii.gz \
        $ADIR/mst/gshoot/${id}/iter_4/target_to_root_procrustes.mat,-1"
      ;;
  esac

  # Create the working directory
  mkdir -p $WDIR

  # Transform the segmentation into the template space
  greedy -d 3 \
    -rf $TEMPLATEDIR/template_img.nii.gz \
    -rm $IMG $WDIR/${id}_wholemri_to_template.nii.gz \
    -ri LABEL 0.24vox \
    -rm $TMPDIR/${id}_mask.nii.gz $WDIR/${id}_lwseg_to_template_mask.nii.gz \
    -rm $TMPDIR/${id}_seg.nii.gz $WDIR/${id}_lwseg_to_template.nii.gz \
    -r $WARPCHAIN \
      $ADIR/preproc/${id}/unwarp/${id}_unwarp.mat \
      $ADIR/preproc/${id}/${id}_danseg_to_raw.mat
}

function all_pair_histo_dice()
{
  mode=${1?}
  local WDIR

  # Work directory
  if [[ $mode == "ncc" ]]; then
    WDIR=$WORK
  else
    WDIR=$WORK/template_hist/$mode
  fi

  # List the available segmentations
  local IDS=($(for i in $(ls $WDIR/*_lwseg_to_template.nii.gz); do basename $i _lwseg_to_template.nii.gz; done))

  # Create a Dice directory
  mkdir -p $WDIR/overlap
  rm -rf $WDIR/gdsc.txt

  for ((i=0;i<${#IDS[*]};i++)); do
    for ((j=$((i+1));j<${#IDS[*]};j++)); do

      local OVLFILE=$WDIR/overlap/overlap_${IDS[i]}_${IDS[j]}.txt

      c3d \
        $WDIR/vote_hfdb.nii.gz -thresh 1 1 1 0 -popas M \
        $WDIR/${IDS[i]}_lwseg_to_template.nii.gz -push M -times -as A \
        $WDIR/${IDS[j]}_lwseg_to_template.nii.gz -push M -times -as B \
        -foreach -thresh 1 inf 1 0 -endfor -times -popas X \
        -push A -push X -times \
        -push B -push X -times \
        -label-overlap > $OVLFILE

      echo "$mode ${IDS[i]} ${IDS[j]} $(cat $OVLFILE | awk 'NR==3 {print $3}')" >> $WDIR/gdsc.txt

    done
  done
}


function combine()
{
  mode=${1?}

  local WDIR

  # Work directory
  if [[ $mode == "ncc" ]]; then
    WDIR=$WORK
  else
    WDIR=$WORK/template_hist/$mode
  fi


  c3d $WDIR/*lwseg_to_template.nii.gz -foreach -thresh 1 inf 1 0 -endfor -accum -add -endaccum -o $TMPDIR/sum.nii.gz

  for i in 1 2 3 4 5 6 7 8; do
    c3d $WDIR/*lwseg_to_template.nii.gz \
      -foreach -thresh $i $i 1 0 -endfor \
      -accum -add -endaccum \
      -o $WDIR/hitsum_${i}.nii.gz \
      $TMPDIR/sum.nii.gz -reciprocal -times \
      -o $WDIR/probmap_${i}.nii.gz
  done

  # Vote - no masking for now
  c3d $TMPDIR/sum.nii.gz -thresh 0 inf 0.1 0 \
    $WDIR/probmap_*.nii.gz -vote \
    -o $WDIR/vote_raw.nii.gz

  DBSEG_PROB=$TEMPLATEDIR/template_db.nii.gz
  HFSEG_PROB=$TEMPLATEDIR/template_hf.nii.gz

  c3d $DBSEG_PROB -thresh 0.5 inf 1 0 $HFSEG_PROB -thresh 0.5 inf 1 0 -add -o $WDIR/vote_hfdb.nii.gz

  c3d $WDIR/vote_hfdb.nii.gz -as A -thresh 1 1 1 0 $WDIR/vote_raw.nii.gz -times -push A -thresh 2 2 9 0 -add -o $WDIR/vote_masked.nii.gz

  # Generate an entropy map and a masked entropy map
  c3d $WDIR/probmap_*.nii.gz \
    -foreach -dup -shift 0 -log -scale 1.442695 -clip -1000 1000 -times -endfor \
    -accum -add -endaccum -scale -1 -replace nan 0 \
    -o $WDIR/entropy.nii.gz 

  c3d $WDIR/entropy.nii.gz $WDIR/vote_hfdb.nii.gz -thresh 1 1 1 0 -times \
    $WDIR/vote_hfdb.nii.gz -thresh 1 1 0 -1 -add \
    -o $WDIR/entropy_masked.nii.gz
}

function mrf()
{
  # The regularization weight
  WEIGHT=${1?}

  # The weight converted to something that can be in a directory name
  DIRNAME=$(echo $WEIGHT | awk '{printf "%06d\n", $1 * 100000}')
  WDIR=$WORK/mrf_vote/mrf_vote_$DIRNAME

  # Scaling
  SCALE=$(ls $WORK/*_lwseg_to_template.nii.gz | wc -l | awk '{print 1.0 / $1}')

  # Create the directory
  mkdir -p $WDIR

  # Perform the graph cut optimization
  c3d \
    $WORK/hitsum_*.nii.gz \
    $WORK/vote_hfdb.nii.gz -thresh 1 2 1 0 -popas M \
    -foreach -scale $SCALE -push M -thresh 0 0 NaN 1 -times -endfor \
    -verbose -vote-mrf VA $WEIGHT \
    -o $WDIR/mrf_seg_masked.nii.gz

  # Compute Dice with individual segmentations
  rm -rf $WDIR/gdsc.txt
  for seg in $(ls $WORK/*_lwseg_to_template.nii.gz); do
    ID=$(basename $seg _lwseg_to_template.nii.gz)

    c3d $seg -as S -thresh 1 inf 1 0 \
      $WORK/vote_hfdb.nii.gz -thresh 1 1 1 0 -times -popas M \
      -push S $WDIR/mrf_seg_masked.nii.gz \
      -foreach -push M -times -endfor -label-overlap \
      > $WDIR/dice_report_$ID.txt

    echo "$WEIGHT $ID $(cat $WDIR/dice_report_$ID.txt| awk 'NR==3 {print $3}')" \
      >> $WDIR/gdsc.txt
  done

  # Combine the subfields and the DB mask
  c3d $WDIR/mrf_seg_masked.nii.gz $WORK/vote_hfdb.nii.gz \
    -thresh 2 2 1 0 -as DB \
    -thresh 1 1 0 1 -times \
    -push DB -scale 9 -add \
    -o $WDIR/mrf_seg_withdb.nii.gz
}

function map_seg_to_subject()
{
  id=${1?}

  # Map the atlas segmentation back into subject space
  WDIR=$WORK/tosubj/${id}
  SDIR=$ADIR/mst/final/${id}

  # Unwarped image
  IMGUW=$SDIR/${id}_unwarp_img.nii.gz 

  # The segmentation we are going to use
  SEG=$WORK/template_histo_seg_final.nii.gz
  SEG_AP=$WORK/template_histo_seg_ap_final.nii.gz

  mkdir -p $WDIR
  ln -sf $IMGUW $WDIR/

  greedy -d 3 \
    -ri LABEL 0.24vox \
    -rf $SDIR/${id}_unwarp_img.nii.gz \
    -rm $SEG $WDIR/${id}_histoseg.nii.gz \
    -rm $SEG_AP $WDIR/${id}_histoseg_ap.nii.gz \
    -r $(cat $SDIR/chain_template_to_unwarped.txt)

  c3d $IMGUW $WDIR/${id}_histoseg.nii.gz \
    -lstat > $WDIR/${id}_lstat.txt

  c3d $IMGUW $WDIR/${id}_histoseg_ap.nii.gz \
    -lstat > $WDIR/${id}_lstat_ap.txt

}

function measure_long_axis_extents()
{
  id=${1?}
  mode=${2?}

  if [[ $mode = 'rigid' ]]; then
    PROCOPT="-r"
  elif [[ $mode = 'similarity' ]]; then 
    PROCOPT=""
  else
    echo "Error: wrong mode"
    return -1
  fi

  # Working directory
  WDIR=$WORK/axisalign/$id/$mode

  # Get landmarks in the space of the template
  LM_TEMPLATE=$ADIR/mst/gshoot/shape_avg/iter_4/shavg_landmarks.vtk

  # Landmarks mapped into the space of the main-axis aligned image
  LM_AXISSPACE=$TMPDIR/axis_landmarks.vtk

  # Landmarks mapped into the subject unwarped space
  LM_SUBJ=$TMPDIR/subj_landmarks.vtk

  # Subject's final directory
  SDIR=$ADIR/mst/final/${id}

  # Determine the side of the template (L or R)
  TEMPLATE_SIDE=L

  # Subject side
  SUBJ_SIDE=$(echo $id | awk -F '-' '{print $2}')

  # Procrustes matrix
  MAT_PROC=$WDIR/procrustes.mat

  # Segmentation used for this
  SEG=$WORK/template_histo_seg_final.nii.gz

  # Segmentation with anterior/posterior labels
  SEG_AP=$WORK/template_histo_seg_ap_final.nii.gz

  # Manual segmentation of the uncal apex
  APEX=$ADIR/scripts/manual/uncal_apex.nii.gz

  # Manual segmentation of the tail landmarks
  TAIL=$ADIR/scripts/manual/tail_landmarks.nii.gz

  # Flip flag
  FLIPFLAG=""
  if [[ $TEMPLATE_SIDE != $SUBJ_SIDE ]]; then
    FLIPFLAG="-f"
  fi

  mkdir -p $WDIR

  # Generate the landmarks in axis space
  greedy -d 3 -rf $TEMPLATEDIR/template_img.nii.gz -rs $LM_TEMPLATE $LM_AXISSPACE \
    -r $ADIR/scripts/manual/manual_template_hippoaxis.mat,-1

  # Generate the landmarks in the unwarped subject space
  greedy -d 3 \
    -rf $TEMPLATEDIR/template_img.nii.gz \
    -rs $LM_TEMPLATE $LM_SUBJ \
    -r $(cat $SDIR/chain_unwarped_to_template.txt)

  # Perform procrustes alignment between the two sets of landmarks
  vtkprocrustes $FLIPFLAG $PROCOPT $LM_AXISSPACE $LM_SUBJ $MAT_PROC

  # From the apex segmentation, generate the anterior/posterior masks
  # label everything posterior of the uncal apex as posterior, everything else as anterior
  local APCOORD=$(c3d $APEX -dup -centroid | grep _MM | sed -e "s/,//g" | awk '{print $3}')
  c3d $APEX -cmp -pop -thresh $APCOORD inf 2 1 -scale 100 \
    $SEG -add -replace 100 0 200 0 -o $SEG_AP
  
  # Apply the procrustes alignment to the segmentation
  greedy -d 3 \
    -ri LABEL 0.24vox \
    -rf $TEMPLATEDIR/template_img.nii.gz \
    -rm $SEG $WDIR/${id}_axisalign_histoseg.nii.gz \
    -rm $APEX $WDIR/${id}_uncalapex.nii.gz \
    -rm $TAIL $WDIR/${id}_tail_landmarks.nii.gz \
    -r $MAT_PROC $(cat $SDIR/chain_template_to_unwarped.txt)

  # Compute the angle between tail landmarks
  c3d $WDIR/${id}_tail_landmarks.nii.gz \
    -split -foreach -centroid -endfor \
    | grep CENTROID_MM | tail -n 3 \
    | sed -e "s/.*\[//" -e "s/\].*//" \
    | $ADIR/scripts/angle_three_points.R \
    > $WDIR/${id}_tail_angle.txt

  # Also send the intensity from its raw space into this aligned space
  # so we can compare images in this common orientation
  greedy -d 3 \
    -rf $TEMPLATEDIR/template_img.nii.gz \
    -rm $ADIR/preproc/${id}/${id}_raw_n4clip.nii.gz $WDIR/${id}_axisalign_img.nii.gz \
    -r $MAT_PROC \
      $ADIR/preproc/${id}/unwarp/${id}_unwarp.mat \

  # While we are here, also take this subject's histology segmentation
  # (if she has one) and bring it into the aligned space
  LWSEG=$(get_lwseg $id)
  if [[ -f $LWSEG ]]; then

    # We need to fix up the header of the segmentation
    c3d $ADIR/input/$id/${id}_img.nii.gz $LWSEG -copy-transform -o $TMPDIR/${id}_seg.nii.gz

    # Apply the transform to Laura's segmentation. This means that we
    # need to take it from its input space to current space
    greedy -d 3 \
      -ri LABEL 0.24vox \
      -rf $TEMPLATEDIR/template_img.nii.gz \
      -rm $TMPDIR/${id}_seg.nii.gz $WDIR/${id}_axisalign_lwseg.nii.gz \
      -r $MAT_PROC \
        $ADIR/preproc/${id}/unwarp/${id}_unwarp.mat \
        $ADIR/preproc/${id}/${id}_danseg_to_raw.mat

  fi


  # Measure the extents of the whole hippocampus
  EXTENTS=$( \
      c3d $WDIR/${id}_axisalign_histoseg.nii.gz \
        -thresh 1 inf 1 0 -trim 0mm \
        -probe 0x50x50% -probe 100x50x50% | awk '{print $6}')

  # Read these values into two variables
  read HF_TAIL HF_HEAD <<< "$(echo $EXTENTS)"

  # Write the start and end of the hippocampus
  echo "$id HF $(echo $EXTENTS)" > $WDIR/${id}_extent_stats.txt

  # Generate statistics on the extents of various subfields
  for SF in 1 2 3 4 6 9; do

    # Compute the extents of the different labels along the main axis
    EXTENTS=$( \
      c3d $WDIR/${id}_axisalign_histoseg.nii.gz \
        -thresh $SF $SF 1 0 -trim 0mm \
        -probe 0x50x50% -probe 100x50x50% | awk '{print $6}')

    # Print out
    echo "$id ${SF_NAMES[$SF]} $(echo $EXTENTS)"

  done >> $WDIR/${id}_extent_stats.txt

  # Generate similar statistics for the uncal apex
  EXTENTS=$( \
      c3d $WDIR/${id}_uncalapex.nii.gz \
        -thresh 1 1 1 0 -trim 0mm \
        -probe 50x50x50% -probe 50x50x50% | awk '{print $7}')

  echo "$id UA $(echo $EXTENTS)" >> $WDIR/${id}_extent_stats.txt

  # Generate a separate file where all units are mm from the start of the hippocampal head
  cat $WDIR/${id}_extent_stats.txt \
    | awk -v z=${HF_HEAD} '{print $1,$2,z-$4,z-$3}' > $WDIR/${id}_dist_from_head.txt
}

function glm_fdr_threshold()
{
  local mesh alpha M
  mesh=${1?}
  alpha=${2?}

  dumpmeshattr ${mesh} "P-value (uncorr)" | sort -g > $TMPDIR/psort.txt
  M=$(cat $TMPDIR/psort.txt | wc -l)

  cat $TMPDIR/psort.txt | awk -v m=$M -v alpha=$alpha '$1 <= alpha*NR/m {print $1}' | tail -n 1
}

# Common steps for the thickness analysis
function thickness_templates()
{
  # Extract the surface of the dark band for thickness analyses
  mkdir -p $WORK/thickness
  vtklevelset $TEMPLATEDIR/template_db.nii.gz $WORK/thickness/template_db.vtk 0.5

  # Extract the SUB-CA surface (i.e. everything excluding DG and SRLM)
  c3d $WORK/template_histo_seg_final.nii.gz \
    -replace 3 0 -replace 9 0 -thresh 1 inf 1 0 \
    -o $TMPDIR/graystrip.nii.gz

  # Extract the DG  surface 
  c3d $WORK/template_histo_seg_final.nii.gz \
    -thresh 3 3 1 0 \
    -o $TMPDIR/dg.nii.gz

  vtklevelset $TMPDIR/graystrip.nii.gz $WORK/thickness/template_gm.vtk 0.5
  vtklevelset $TMPDIR/dg.nii.gz $WORK/thickness/template_dg.vtk 0.5

  # Apply Taubin smoothing to the meshes
  for what in dg db gm; do

    mesh_smooth_curv -mu -0.51 -lambda 0.5 -iter 1000 -r 2.0 \
      $WORK/thickness/template_${what}.vtk \
      $WORK/thickness/template_${what}_tsm.vtk

    # Strip point data that messes Greedy up
    cat $WORK/thickness/template_${what}_tsm.vtk \
      | awk '$1 == "POINT_DATA" {stop=1} { if (stop!=1) print $0  }' \
      > $WORK/thickness/template_${what}_tsm_nopd.vtk

  done
}

# Subject-specific steps for the thickness analysis
function thickness_persubj()
{
  id=${1?}
  what=${2?}

  # Segmentation used for this
  SEG=$WORK/template_histo_seg_final.nii.gz

  # Subject's final directory
  SDIR=$ADIR/mst/final/${id}

  # Output directory
  WDIR=$WORK/thickness/subj/${id}

  # The subject's segmentations
  SUBJSEG=$WORK/tosubj/${id}/${id}_histoseg.nii.gz

  # The output skeleton mesh
  local SKEL=$WDIR/${id}_${what}_skeleton.vtk
  local THKMESH=$WDIR/${id}_${what}_thickness.vtk
  local LTHK=$TMPDIR/labthick.txt

  # Make the directory
  mkdir -p $WDIR

<<'skip674'  
  # Map the dark band and gray matter surfaces into subject space
  greedy -d 3 \
    -rf $SEG \
    -rs $WORK/thickness/template_${what}_tsm_nopd.vtk $TMPDIR/${what}_subj.vtk \
    -r $(cat $SDIR/chain_unwarped_to_template.txt)

  # Extract the skeleton and compute thickness map
  cmrep_vskel -Q /data/picsl/pauly/bin/qvoronoi \
    -e 5 -p 1.8 -c 1 -T $THKMESH \
    $TMPDIR/${what}_subj.vtk $SKEL \
    | tee $WDIR/${id}_${what}_thickness_out.txt

skip674

  # Take the segmentation, retain only the labels pertaining to the list of interest
  # expand these labels and use them to assign a label to each vertex on the thickness
  # mesh. This really only does something for the GM mesh.
  local LABLIST
  
  if [[ $what == 'gm' ]]; then
    LABLIST="1 2 4 6 8"
  elif [[ $what == 'db' ]]; then
    LABLIST="9"
  else
    LABLIST="3"
  fi

  # Expand the segmentation
  c3d $SUBJSEG -retain-labels ${LABLIST} -trim 5vox -replace 0 1000 \
    -split \
    -foreach -sdt -dup -times -sqrt -reciprocal -endfor \
    -scale 0 -merge \
    -replace 1000 0 -o $TMPDIR/seg_expanded.nii.gz

  # Sample the mesh by the segmentation
  mesh_image_sample -i 0 $THKMESH $TMPDIR/seg_expanded.nii.gz $THKMESH LABEL

  # Compute the area element per voxel
  mesh_area_element $THKMESH $THKMESH

  # Write all the labels into a file
  dumpmeshattr $THKMESH Thickness > $WDIR/${id}_${what}_dumpthick.txt
  dumpmeshattr $THKMESH LABEL > $WDIR/${id}_${what}_dumplabel.txt
  dumpmeshattr $THKMESH AreaElement > $WDIR/${id}_${what}_dumparea.txt
  paste \
    $WDIR/${id}_${what}_dumpthick.txt \
    $WDIR/${id}_${what}_dumplabel.txt \
    $WDIR/${id}_${what}_dumparea.txt \
    > $LTHK
  
  # Aggregate the thickness
  for label in $LABLIST; do
    cat $LTHK | awk -v L=$label -v id=$id '$2 == L {w+=$3;x+=$1*$3} END {print id,L,x/w}'
  done > $WDIR/${id}_${what}_thickness_by_label.txt
}

function thickness_glm()
{
  # This function performs GLM on dark band thickness
  what=${1?}

  # The side where this takes place
  side=${2?}

  # The group membership file
  GRPFILE=$ADIR/scripts/groups.txt

  # The template thickness mesh
  REFMESH=$WORK/thickness/template_${what}_tsm_nopd.vtk

  # Work directory for this analysis
  WDIR=$WORK/thickness/glm/${what}_thickness_ad_ctl_${side}

  # The input mesh for GLM
  GLMINPUT=$WDIR/glm_input.vtk
  GLMRESULT=$WDIR/glm_result.vtk

  # Create the directory
  mkdir -p $WDIR

  # List of meshes to average
  local MERGE_LIST=""

  # Clear the design matrix
  rm -rf $WDIR/design.txt

  # Iterate over all subjects in the group file
  for subj in $(cat $GRPFILE | grep -v 'OTHER' | awk '{print $1}'); do

    # Get the design matrix info
    local group age dummy
    read dummy group age <<< $(cat $GRPFILE | grep $subj) 

    # Select which file to include
    if [[ $side == 'A' ]]; then

      # Average between the left and right meshes for this subject
      local MERGE_ENTRY=$TMPDIR/arravg_${subj}.vtk
      local SUBJ_MESH=$(ls $WORK/thickness/subj/${subj}*/*${what}_thickness.vtk)
      avgmesharr ${SUBJ_MESH} Thickness $REFMESH $MERGE_ENTRY

    else

      local MERGE_ENTRY=$(ls $WORK/thickness/subj/${subj}-${side}/*${what}_thickness.vtk)

    fi

    # Append to the merge list
    MERGE_LIST="$MERGE_LIST $MERGE_ENTRY"

    # Generate the design matrix value
    echo $group $age | sed -e "s/AD/0 1/g" | sed -e "s/CTL/1 0/g" >> $WDIR/design.txt

  done

  # Combine meshes into a single VTK mesh for analysis
  mesh_merge_arrays -r $REFMESH $GLMINPUT Thickness $MERGE_LIST

  # Generate the contrast 
  echo "1 -1 0" > $WDIR/contrast.txt

  # Perform GLM without permutation (we can use FDR)
  meshglm -m $GLMINPUT $GLMRESULT -a Thickness -d 4 \
    -g $WDIR/design.txt $WDIR/contrast.txt \
    -p 1000 -s P -t 0.01 -e > $WDIR/glmoutput.txt

  # Compute FDR threshold
  glm_fdr_threshold $GLMRESULT 0.05 > $WDIR/fdr_0.05_threshold.txt
}

function thickness_lmer_chunk()
{
  local i=${1?}
  local n=${2?}

  local WDIR=$WORK/thickness/lmer
  local TALL=$WDIR/allthick.txt

  # Get a subset of thickness columns for this i and n
  cat $TALL | cut -d ' ' -f 1,2,$((3+i))-$((2+i+n)) > $TMPDIR/thick.txt

  # Run the R script with LMER
  $RHOME/bin/Rscript $ADIR/scripts/stats/lmer_pw.R \
    $TMPDIR/thick.txt $ADIR/scripts/stats/demog.csv $WDIR/chunks/chunk_$(printf %07d $i).txt
}

function thickness_lmer()
{
  # Dump the mesh arrays
  local WDIR=$WORK/thickness/lmer
  local TFILE=$TMPDIR/thick.txt
  local TALL=$WDIR/allthick.txt

  mkdir -p $WDIR/chunks

  # Dump the mesh arrays for each subject
  rm -rf $TALL
  for id in $(cat $ADIR/scripts/subj.txt); do

    rm -rf $TFILE

    for what in dg db gm; do

      local mesh=$(ls $WORK/thickness/subj/$id/*${what}_thickness.vtk)

      mesh3d ${mesh} -diffuse 4 Thickness -da $TMPDIR/dump.txt Thickness 

      cat $TMPDIR/dump.txt >> $TFILE


      # Keep track of where each mesh starts
      if [[ ! -f $TMPDIR/pos_${what}.txt ]]; then
        cat $TFILE | wc -l > $TMPDIR/pos_${what}.txt
      fi

    done

    echo $(echo $id | sed -e "s/-/ /g") $(cat $TFILE) >> $TALL

  done

  # Count the number of thickness entries altogether
  local N=$(cat $TFILE | wc -l)

  # Split into 1000-piece chunks for parallel R analysis
  for ((i=0; i<N; i+=1000)); do 
    qsub -j y -cwd -V -o $WORK/dump -N "lmer_$i" \
      $0 thickness_lmer_chunk $i 1000
  done

  # Wait for the chunks to finish
  qsub -cwd -o $WORK/dump -j y -hold_jid "lmer_*" -sync y -b y sleep 1

  # Perform FDR on the p-values
  cat $WDIR/chunks/chunk_*.txt > $TMPDIR/praw.txt
  $RHOME/bin/Rscript $ADIR/scripts/stats/lmer_fdr.R $TMPDIR/praw.txt $WDIR/pcorr.txt

  local RSTART=0
  for what in dg db gm; do

    # rows 2 through K+1
    local REND=$(cat $TMPDIR/pos_${what}.txt)
    cat $WDIR/pcorr.txt | awk "NR-1 > $RSTART && NR-1 <= $REND {print \$0}" > $WDIR/lmer_${what}.txt

    # Dump columns
    cat $WDIR/lmer_${what}.txt | cut -d ' '  -f 1 > $TMPDIR/col_${what}_ad_chi.txt
    cat $WDIR/lmer_${what}.txt | cut -d ' '  -f 2 > $TMPDIR/col_${what}_ad_tval.txt
    cat $WDIR/lmer_${what}.txt | cut -d ' '  -f 4 > $TMPDIR/col_${what}_age_chi.txt
    cat $WDIR/lmer_${what}.txt | cut -d ' '  -f 5 > $TMPDIR/col_${what}_age_tval.txt
    cat $WDIR/lmer_${what}.txt | cut -d ' '  -f 7 > $TMPDIR/col_${what}_ad_padj.txt
    cat $WDIR/lmer_${what}.txt | cut -d ' '  -f 8 > $TMPDIR/col_${what}_age_padj.txt

    # Add to the mesh
    mesh3d $WORK/thickness/template_${what}_tsm.vtk \
      -aa $TMPDIR/col_${what}_ad_chi.txt "AD-ChiSq" \
      -aa $TMPDIR/col_${what}_age_chi.txt "Age-ChiSq" \
      -aa $TMPDIR/col_${what}_ad_tval.txt "AD-TVal" \
      -aa $TMPDIR/col_${what}_age_tval.txt "Age-TVal" \
      -aa $TMPDIR/col_${what}_ad_padj.txt "AD-PAdj" \
      -aa $TMPDIR/col_${what}_age_padj.txt "Age-PAdj" \
      -o $WDIR/lmer_${what}.vtk

    # Update the start
    RSTART=$REND

  done
}


function thickness_glm_age()
{
  # This function performs GLM on dark band thickness
  what=${1?}

  # The side where this takes place
  side=${2?}

  # The group membership file
  GRPFILE=$ADIR/scripts/groups.txt

  # The template thickness mesh
  REFMESH=$WORK/thickness/template_${what}_tsm_nopd.vtk

  # Work directory for this analysis
  WDIR=$WORK/thickness/glm_age/${what}_thickness_ad_ctl_${side}

  # The input mesh for GLM
  GLMINPUT=$WDIR/glm_input.vtk
  GLMRESULT=$WDIR/glm_result.vtk

  # Create the directory
  mkdir -p $WDIR

  # List of meshes to average
  local MERGE_LIST=""

  # Clear the design matrix
  rm -rf $WDIR/design.txt

  # Iterate over all subjects in the group file
  for subj in $(cat $GRPFILE | grep 'CTL' | awk '{print $1}'); do

    # Get the design matrix info
    local group age dummy
    read dummy group age <<< $(cat $GRPFILE | grep $subj) 

    # Select which file to include
    if [[ $side == 'A' ]]; then

      # Average between the left and right meshes for this subject
      local MERGE_ENTRY=$TMPDIR/arravg_${subj}.vtk
      local SUBJ_MESH=$(ls $WORK/thickness/subj/${subj}*/*${what}_thickness.vtk)
      avgmesharr ${SUBJ_MESH} Thickness $REFMESH $MERGE_ENTRY

    else

      local MERGE_ENTRY=$(ls $WORK/thickness/subj/${subj}-${side}/*${what}_thickness.vtk)

    fi

    # Append to the merge list
    MERGE_LIST="$MERGE_LIST $MERGE_ENTRY"

    # Generate the design matrix value
    echo 1 $age >> $WDIR/design.txt

  done

  # Combine meshes into a single VTK mesh for analysis
  mesh_merge_arrays -r $REFMESH $GLMINPUT Thickness $MERGE_LIST

  # Generate the contrast 
  echo "0 -1" > $WDIR/contrast.txt

  # Perform GLM without permutation (we can use FDR)
  meshglm -m $GLMINPUT $GLMRESULT -a Thickness -d 4 \
    -g $WDIR/design.txt $WDIR/contrast.txt \
    -p 1000 -s P -t 0.01 -e > $WDIR/glmoutput.txt

  # Compute FDR threshold
  glm_fdr_threshold $GLMRESULT 0.05 > $WDIR/fdr_0.05_threshold.txt
}


function map_histo_segs()
{
  mode=${1?}

  # Reslice individual segs
  mkdir -p $WORK/dump
  for subj in $(cat $ADIR/scripts/subj.txt); do
    qsubp2 -cwd -o $WORK/dump -j y -N "hproc_${subj}" $0 process_seg $subj $mode
  done

  # Wait for completion
  qsub -cwd -o $WORK/dump -j y -hold_jid "hproc_*" -sync y -b y sleep 1

  # Combine the segs
  qsubp4 -cwd -o $WORK/dump -j y -sync y -N "comb_${fn}" $0 combine $mode

  # Compute the GDSC
  qsubp4 -cwd -o $WORK/dump -j y -sync y -N "gdsc_${mode}" $0 all_pair_histo_dice $mode

  if [[ $mode == "ncc" ]]; then

    # Determine best MRF weight
    for weight in $(echo 1 | awk '{ for (i=0.01; i<=0.101; i+=0.01) print i }'); do
      qsubp2 -cwd -o $WORK/dump -j y -N "mrf_${weight}" $0 mrf $weight
    done
    qsub -cwd -o $WORK/dump -j y -hold_jid "mrf_*" -sync y -b y sleep 1

    # Link the chosen template
    CHOSEN_WEIGHT=005000
    ln -sf $WORK/mrf_vote/mrf_vote_${CHOSEN_WEIGHT}/mrf_seg_withdb.nii.gz \
      $WORK/template_histo_seg_final.nii.gz

    ln -sf $TEMPLATEDIR/template_img.nii.gz $WORK/

  fi
}

# Generate the A/P segmentation
function make_ap_segmentation()
{
  # Segmentation used for this
  local SEG=$WORK/template_histo_seg_final.nii.gz

  # Segmentation with anterior/posterior labels
  local SEG_AP=$WORK/template_histo_seg_ap_final.nii.gz

  # Manual segmentation of the uncal apex
  local APEX=$ADIR/scripts/manual/uncal_apex.nii.gz

  # From the apex segmentation, generate the anterior/posterior masks
  # label everything posterior of the uncal apex as posterior, everything else as anterior
  local APCOORD=$(c3d $APEX -dup -centroid | grep _MM | sed -e "s/,//g" | awk '{print $3}')
  c3d $APEX -cmp -pop -thresh $APCOORD inf 2 1 -scale 100 \
    $SEG -add -replace 100 0 200 0 -o $SEG_AP
}

function vols_and_stats()
{
  # Map the atlas segmentation into the individual subject spaces
<<'SKIPzz'
  for subj in $(cat $ADIR/scripts/subj.txt); do

    qsubp2 -cwd -o $WORK/dump -j y -N "hproc_${subj}" \
      $0 map_seg_to_subject $subj

  done
  qsub -cwd -o $WORK/dump -j y -hold_jid "hproc_*" -sync y -b y sleep 1

  # Compute the volume statistics in subject unwarped space
  mkdir -p $WORK/stats
  for subj in $(cat $ADIR/scripts/subj.txt); do
    cat $WORK/tosubj/$subj/${subj}_lstat.txt \
      | awk -v id=$subj 'NR > 2 {print id,$1,$7}' 
  done > $WORK/stats/all_vols.txt

  for subj in $(cat $ADIR/scripts/subj.txt); do
    echo $subj $(cat $WORK/stats/all_vols.txt | grep $subj | awk '{print $3}')
  done > $WORK/stats/all_vols_wide.txt

  # Same for the AP statistics
  for subj in $(cat $ADIR/scripts/subj.txt); do
    cat $WORK/tosubj/$subj/${subj}_lstat_ap.txt \
      | awk -v id=$subj 'NR > 2 {print id,$1,$7}' 
  done > $WORK/stats/all_vols_ap.txt

  for subj in $(cat $ADIR/scripts/subj.txt); do
    echo $subj $(cat $WORK/stats/all_vols_ap.txt | grep $subj | awk '{print $3}')
  done > $WORK/stats/all_vols_ap_wide.txt
SKIPzz

  # Compute summary A/P volumes
  for subj in $(cat $ADIR/scripts/subj.txt); do
    echo $subj \
      $(cat $WORK/tosubj/$subj/${subj}_lstat_ap.txt | grep '^  1.. ' | awk '{k+=$7} END {print k}') \
      $(cat $WORK/tosubj/$subj/${subj}_lstat_ap.txt | grep '^  2.. ' | awk '{k+=$7} END {print k}')
  done > $WORK/stats/hippo_ap_vols_wide.txt

}

function main_axis_lengths()
{
  for subj in $(cat $ADIR/scripts/subj.txt); do
    for mode in rigid similarity; do

      qsubp2 -cwd -o $WORK/dump -j y -N "laxis_${subj}_${mode}" \
        $0 measure_long_axis_extents $subj $mode

    done
  done
  qsub -cwd -o $WORK/dump -j y -hold_jid "laxis_*" -sync y -b y sleep 1

  # Collect the angle statistics into a usable file
  for subj in $(cat $ADIR/scripts/subj.txt); do
    echo $subj $(cat $WORK/axisalign/$subj/rigid/${subj}_tail_angle.txt) 
  done | sed -e "s/\[1\] //" -e "s/-/ /" > $WORK/stats/all_tail_angles.txt
  
}

function thickness_summary()
{
  for id in $(cat $ADIR/scripts/subj.txt); do

    local WDIR=$WORK/thickness/subj/${id}

    cat \
      $WDIR/${id}_gm_thickness_by_label.txt \
      $WDIR/${id}_dg_thickness_by_label.txt \
      $WDIR/${id}_db_thickness_by_label.txt \
      > $WDIR/${id}_all_thickness_by_label.txt

    # Note the multiplication by two here
    echo ${id} $(for label in 1 2 4 3 6 9; \
      do cat $WDIR/${id}_all_thickness_by_label.txt \
        | grep "^${id} ${label} " | awk '{print $3 * 2.0}'; done) \
        > $WDIR/${id}_all_thickness_by_label_wide.txt

  done
}

function thickness_stats()
{
<<'SKIP03'
  # THICKNESS ANALYSIS
  # extract meshes for thickness analysis
  qsub -cwd -o $WORK/dump -j y -N "thk" -sync y $0 thickness_templates

  # compute thickness in each subject's space
  for what in dg db gm; do
    for subj in $(cat $ADIR/scripts/subj.txt); do

      qsubp2 -cwd -o $WORK/dump -j y -N "thk_${what}_${subj}" \
        $0 thickness_persubj $subj $what

    done
  done
  qsub -cwd -o $WORK/dump -j y -hold_jid "thk_*" -sync y -b y sleep 1

  thickness_summary

  # Perform thickness GLM
  for what in dg db gm; do
    for side in L R A; do
      qsub -cwd -o $WORK/dump -j y -N "glm_${what}_${side}" $0 thickness_glm ${what} ${side}
      qsub -cwd -o $WORK/dump -j y -N "glm_age_${what}_${side}" $0 thickness_glm_age ${what} ${side}
    done
  done
  qsub -cwd -o $WORK/dump -j y -hold_jid "glm_*" -sync y -b y sleep 1

SKIP03

  # Perform thickness mixed effects linear model using R
  qsub -cwd -o $WORK/dump -j y -sync y -N "thicklmer" $0 thickness_lmer

  # Compute summary thickness statistics
  for subj in $(cat $ADIR/scripts/subj.txt); do

    read MEANSURF MEANTHK <<< $(cat $WORK/thickness/subj/$subj/*_db_thickness_out.txt | tail -n 2 | awk '{print $NF}')
    echo $(echo $subj | sed -e "s/-/ /g") $MEANTHK $MEANSURF

  done > $WORK/stats/all_db_thickness.txt
}

function shape_stats_init()
{
  # The geodesic shooting directory
  local GDIR=$ADIR/mst/gshoot_ncc
  local ITER=iter_4

  # Work directory
  local WDIR=$WORK/shape/

  mkdir -p $WDIR $WDIR/labels
<<'SKIP'


  # Generate the momentum data in a format that is readable by R script
  for id in $(cat $ADIR/scripts/subj.txt); do 
    local MOMENTS=$GDIR/$id/$ITER/shooting_momenta.vtk
    echo $(echo $id | sed -e "s/-/ /g") $(dumpmeshattr $MOMENTS InitialMomentum) 
  done > $WDIR/initial_momenta.txt

  # Generate an array of mesh coordinates for R
  dumpmeshpoints $GDIR/shape_avg/$ITER/average_momenta.vtk | tail -n +3 > $WDIR/template_points.txt


  # Run the R statistics notebook
  pushd $ADIR/scripts/stats
  $RHOME/bin/Rscript run_pca_svm.R
  popd

SKIP

  # Exctract surface meshes for each label
  local LABELS=$(c3d $WORK/template_histo_seg_final.nii.gz -dup -lstat | tail -n +3 | awk '{print $1}')

  # Build movies for SVM, PCA modes
  for what in svm mode1 mode2 mode3 mode4 mode5; do

    # Animate the label mesh
    for label in $LABELS; do
      
      # For some reason this dies in the GPU queue... 
      qsub -cwd -o $WORK/dump -j y -N "anim_label_${what}_${label}" -q all.q \
        $0 shape_stats_animate_label $label $what

    done

    # Animate the template image
<<DIDIT2    
    qsub -cwd -o $WORK/dump -j y -N "anim_image_${what}" $0 shape_stats_animate_template $what
DIDIT2

  done

  # Wait for completion
  qsub -cwd -o $WORK/dump -j y -hold_jid "anim_*" -sync y -b y sleep 1

<<'DIDIT3'

  # The warps generated for each flow direction need to be inverted and applied
  # to the template and segmentation. 
  for what in svm mode1 mode2 mode3 mode4 mode5; do

    # Animate the label mesh
    for ((i=0;i<=80;i+=4)); do
      qsub -cwd -o $WORK/dump -j y -N "anim_winv_${what}_${i}" $0 shape_stats_anim_warp_template $what $i
    done
  done

  # Wait for completion
  qsub -cwd -o $WORK/dump -j y -hold_jid "anim_winv*" -sync y -b y sleep 1
DIDIT3
}

function merge_neg_pos_movie()
{
  local OUT_PATTERN=${1?}
  local CENTER=${2?}

  for ((i=4;i<=40;i+=4)); do
    local kneg=$((40-i))
    local kpos=$((40+i))
    cp $TMPDIR/neg_flow_${i}.vtk $(printf $OUT_PATTERN $kneg)
    cp $TMPDIR/pos_flow_${i}.vtk $(printf $OUT_PATTERN $kpos)
  done

  cp $CENTER $(printf $OUT_PATTERN 40)
}

function merge_neg_pos_warp_movie()
{
  local OUT_PATTERN=${1?}
  local TEMPLATE=${2?}

  for ((i=4;i<=40;i+=4)); do
    local kneg=$((40-i))
    local kpos=$((40+i))
    cp $TMPDIR/neg_warp_${i}.nii.gz $(printf $OUT_PATTERN $kneg)
    cp $TMPDIR/pos_warp_${i}.nii.gz $(printf $OUT_PATTERN $kpos)
  done

  # Empty warp for the zero
  c3d $TEMPLATE -scale 0 -dup -dup -omc $(printf $OUT_PATTERN 40)
}

function shape_stats_animate_template()
{
  # The label
  local what=${1?}

  # Work directory
  local WDIR=$WORK/shape/

  # Template
  local TEMPLATE=$TEMPLATEDIR/template_img.nii.gz
  local TEMP_SEG=$WORK/template_histo_seg_final.nii.gz

  # Make the output directory
  mkdir -p $WDIR/warp/movie_${what}

  # Flow the SVM vector forward and backward
  /data/picsl-build/pauly/cmrep/gcc64rel/lmtowarp \
    -m $WDIR/${what}_vector_neg.vtk -s 2.0 -n 40 -d 3 -a 4 \
    -r $TEMPLATEDIR/template_img.nii.gz -o $TMPDIR/neg_warp_%d.nii.gz

  /data/picsl-build/pauly/cmrep/gcc64rel/lmtowarp \
    -m $WDIR/${what}_vector_pos.vtk -s 2.0 -n 40 -d 3 -a 4 \
    -r $TEMPLATEDIR/template_img.nii.gz -o $TMPDIR/pos_warp_%d.nii.gz

  ls -l $TMPDIR

  # Combine the flows into a single movie
  merge_neg_pos_warp_movie $WDIR/warp/movie_${what}/movie_${what}_%02d.nii.gz $TEMPLATEDIR/template_img.nii.gz
}

function template_slice_pngs()
{
  # The image to slice
  local image=${1?}

  # Intensity max
  local imax=${2?}

  # Output prefix
  local png=${3?}

  # Slice along the y axis
  c3d -mcs $image \
    -foreach -slice y 80 -stretch 0 $imax 0 255 -clip 0 255 -flip x -flip y -endfor \
    -type uchar -omc $TMPDIR/slice_y.png

  # Slice along the x axis
  for ((j=244;j>=102;j-=44)); do

    # Extract slice
    c3d -mcs $image \
      -foreach -slice x $j -stretch 0 $imax 0 255 -clip 0 255 -flip x -flip y -info -endfor \
      -type uchar -omc $TMPDIR/slice_x_${j}.png

    # Add a line to the y slice
    convert $TMPDIR/slice_y.png -flop -fill white \
      -draw "fill-opacity 0.4 line $j,5 $j,165" -flop \
      $TMPDIR/slice_y.png

  done

  # Montage
  montage $(ls -r $TMPDIR/slice_x_*.png) -tile x1 -geometry +0+0 $TMPDIR/mont_x.png

  # Draw vertical lines on the sagittal slice
  montage $TMPDIR/slice_y.png $TMPDIR/mont_x.png -geometry +0+0 $png
}

function template_slice_img_seg_pngs()
{
  local image=${1?}
  local seg=${2?}
  local imax=${3?}
  local opacity=${4?}
  local png=${5?}

  # Slice the image
  template_slice_pngs $image $imax $TMPDIR/slice_img.png

  # Create segmentation OLI image
  c3d $image -stretch 0 $imax 0 255 $seg \
    -oli $ADIR/scripts/snaplabels.txt $opacity \
    -omc $TMPDIR/segcolor.nii.gz

  # Slice the segmentation image
  template_slice_pngs $TMPDIR/segcolor.nii.gz 255 $TMPDIR/slice_seg.png

  # Montage them
  montage $TMPDIR/slice_img.png $TMPDIR/slice_seg.png -tile 1x -geometry +0+0 $png
}

function shape_stats_anim_warp_template()
{
  # The label
  local what=${1?}

  # The index in the movie
  local i=${2?}

  # Work directory
  local WDIR=$WORK/shape/

  # Template
  local TEMPLATE=$TEMPLATEDIR/template_img.nii.gz
  local TEMP_SEG=$WORK/template_histo_seg_final.nii.gz

  # The warp
  local WARP=$(printf $WDIR/warp/movie_${what}/movie_${what}_%02d.nii.gz $i)

  # The results
  local TEMPLATE_FRAME=$(printf $WDIR/template/movie_${what}/movie_${what}_img_%02d.nii.gz $i)
  local TEMP_SEG_FRAME=$(printf $WDIR/template/movie_${what}/movie_${what}_seg_%02d.nii.gz $i)

  mkdir -p $WDIR/template/movie_${what}/png

  # Invert the geodesic shooting warp
  $GREEDY_HOME/greedy -d 3 \
    -iw $WARP $TMPDIR/invwarp.nii.gz \
    -invexp 4 -wp 0.0001

  # Apply the inverse warp to the template and segmentation
  $GREEDY_HOME/greedy -d 3 \
    -rf $TEMPLATE \
    -rm $TEMPLATE $TEMPLATE_FRAME \
    -ri LABEL 0.24vox \
    -rm $TEMP_SEG $TEMP_SEG_FRAME \
    -r $TMPDIR/invwarp.nii.gz

  # Generate a PNG movie frame
  template_slice_img_seg_pngs $TEMPLATE_FRAME $TEMP_SEG_FRAME 600 0.75 \
    $(printf $WDIR/template/movie_${what}/png/movie_${what}_%02d.png $i)
}

function shape_stats_animate_label()
{
  # The label
  local label=${1?}

  # What direction we are animating (svm, etc)
  local what=${2?}

  # Work directory
  local WDIR=$WORK/shape

  # The central mesh
  local TEMPMESH=$TMPDIR/surf_${label}.vtk

  # Make the output directory
  local WORK=$WDIR/labels/movie_${what}/png
  mkdir -p $WORK

<<'DIDIT'  

  # Extract a mesh for this label
  c3d $WORK/template_histo_seg_final.nii.gz -thresh $label $label 1 0 -o $TMPDIR/label_${label}.nii.gz
  vtklevelset $TMPDIR/label_${label}.nii.gz $TEMPMESH 0.5

  # Flow the SVM vector forward and backward
  /data/picsl-build/pauly/cmrep/gcc64rel/lmtowarp \
    -m $WDIR/${what}_vector_neg.vtk -s 2.0 -n 40 -d 3 \
    -M $TEMPMESH  $TMPDIR/neg_flow_%d.vtk -a 4

  /data/picsl-build/pauly/cmrep/gcc64rel/lmtowarp \
    -m $WDIR/${what}_vector_pos.vtk -s 2.0 -n 40 -d 3 \
    -M $TEMPMESH  $TMPDIR/pos_flow_%d.vtk -a 4

  # Combine the flows into a single movie
  merge_neg_pos_movie $WDIR/labels/movie_${what}/movie_${what}_label${label}_%02d.vtk $TEMPMESH

DIDIT

  # Generate screenshots of the movie
  for ((i=0;i<=80;i+=4)); do

    # 04
    local ind=$(printf %02d $i)

    # Loop over viewpoints and label sets 
    local TOMONTAGE=""
    for view in superior inferior; do

      for labels in all db; do

        local labelset=$(echo $labels | sed -e "s/db/9/" -e "s/all/1-6,8-9/")

        $ADIR/scripts/vtkrender/mesh_to_png.sh \
          $WDIR/labels/movie_${what}/movie_${what}_label%d_${ind}.vtk \
          $ADIR/scripts/vtkrender/$view.pvcc 1360 720 \
          $TMPDIR/large.png $labelset

        local OUT=$WORK/movie_${what}_${labels}_${view}_${ind}.png

        convert $TMPDIR/large.png -filter Gaussian -resize 340x $OUT
        TOMONTAGE="$TOMONTAGE $OUT"
      done
    done

    # Combine the 4 pngs into a montage
    montage $TOMONTAGE -tile x1 -geometry +0+0 $WORK/movie_${what}_montage_${ind}.png

  done
}

function shape_stats()
{
  # extract meshes for thickness analysis
  qsub -cwd -o $WORK/dump -j y -N "shape_init" -sync y $0 shape_stats_init


}

function compute_jacobian()
{
  local id=${1?}
  local SDIR=$ADIR/mst/final/${id}
  local WDIR=$WORK/jacobian/subj

  # Compute the Jacobian
  $GREEDY_HOME/greedy -d 3 \
    -rf $TEMPLATEDIR/template_img.nii.gz \
    -rj $WDIR/jacobian_${id}.nii.gz \
    -r $(cat $SDIR/chain_unwarped_to_template.txt)

  # Take the log
  c3d $WDIR/jacobian_${id}.nii.gz -log -o $WDIR/logjac_${id}.nii.gz
}

function jacobian_stats()
{
  # Create a directory for jacobians
  mkdir -p $WORK/jacobian/subj

  # Compute each jacobian
  mkdir -p $WORK/dump
  for subj in $(cat $ADIR/scripts/subj.txt); do
    qsub -cwd -o $WORK/dump -j y -N "jac_${subj}" $0 compute_jacobian $subj
  done
  qsub -cwd -o $WORK/dump -j y -hold_jid "jac_*" -sync y -b y sleep 1


}

function main()
{
  # Map histology labels into atlas space
  ### for mode in ncc aff_ncc mst gshoot; do
  ###   map_histo_segs $mode
  ### done
  ###make_ap_segmentation

  # Compute volumes and statistics
  ### vols_and_stats

  # Slice along the hippocampal main axis to get length stats
  ### main_axis_lengths

  # Thickness statistical analysis 
  thickness_stats

  # Compute Jacobian and perform analysis
  ### jacobian_stats

  # PCA and LDA
  ### shape_stats
}

if [[ $1 ]]; then

  command=$1
  shift
	$command $@

else

  main
	
fi
