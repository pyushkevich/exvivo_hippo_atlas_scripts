#!/bin/bash
#$ -S /bin/bash
set -x -e

IDS=($(cat subj.txt))

function compute_stats()
{
  DIR=${1?}
  ID1=${2?}
  ID2=${3?}

  mkdir -p $DIR/stats2016/raw

  IMG1=$(ls $DIR/*reslice*${ID1}*img.nii.gz)
  IMG2=$(ls $DIR/*reslice*${ID2}*img.nii.gz)

  c3d $IMG1 $IMG2 -ncc 4x4x4 -replace -NaN 0 NaN 0 -Inf 0 Inf 0 \
    $DIR/template_pot.nii.gz -thresh 0 2 1 0 -lstat | awk 'NR==3 {print $2}' \
    > $DIR/stats2016/raw/ncc_${ID1}_${ID2}.txt
}

function main()
{
  DIR=${1?}
  mkdir -p $DIR/stats2016/dump

  for ((i=0;i<${#IDS[*]};i++)); do
    for ((j=$((i+1));j<${#IDS[*]};j++)); do

      qsub -cwd -o $DIR/stats2016/dump -j y -N "stats_${i}_${j}" \
          $0 compute_stats $DIR ${IDS[i]} ${IDS[j]}

    done
  done

  qsub -cwd -o $DIR/stats2016/dump -j y -hold_jid "stats_*" -sync y -b y sleep 1
}


if [[ $1 ]]; then

  command=$1
  shift
	$command $@

else

  main $1
	
fi

