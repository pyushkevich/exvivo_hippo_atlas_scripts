#!/bin/bash
#$ -S /bin/bash
set -x -e

ROOT=/data/picsl/pauly/dan2015/atlas2016
RAWDIR=/data/tesla-data/pauly/studies/pmatlas/ndri/import/nii

ANTS_PATH=/share/apps/ANTs/2014-06-23/build/bin/

# The directory where the preprocessing is done
WDIR=$ROOT/preproc

# This function performs several tasks to organize the data for each subject
#
# 1. Match the image in the input directory to the RAW image in the scanner output
#    directory. 
function organize_subj()
{
  # This is the subject ID
  id=${1?}

  # Output for this work
  WORK=$WDIR/${id}
  mkdir -p $WORK/misc

  # List all of the potential matches
  RAW_MATCHES=$(cat $ROOT/scripts/id_to_raw.txt \
    | awk -v id="$id" '$1 == id {print $2,$3,$4,$5,$6}')

  # Clear the match list
  MLIST=$WORK/misc/match_list.txt

  # The input image
  INPUT_IMG=$ROOT/input/$id/${id}_img.nii.gz

  # Input image dimensions
  INPUT_DIM=$(c3d $INPUT_IMG -info-full | grep "Image Dimensions" | awk -F ':' '{print $2}')
  
<<'SKIP'  

  # Create a listing o match metrics
  rm -rf $MLIST

  # Repeat this for each potential match
  for rawimg in $RAW_MATCHES; do

    # Check the image dimensions - must match the raw image
    RAW_DIM=$(c3d $RAWDIR/$rawimg -info-full | grep "Image Dimensions" | awk -F ':' '{print $2}')

    rawbase=$(basename $rawimg .nii)
    PRECLIP=$TMPDIR/preclip_${rawbase}.nii.gz
    N4=$TMPDIR/n4_${rawbase}.nii.gz
    CLIP=$WORK/misc/clip_${rawbase}.nii.gz
    MRIGID=$WORK/misc/rigid_${rawbase}.mat

    # Perform N4
    c3d $RAWDIR/$rawimg -stretch 0.1% 99.9% 0 1000 -clip 0 1000 -o $PRECLIP
    $ANTS_PATH/N4BiasFieldCorrection -d 3 -i $PRECLIP -o $N4

    # Rescale the raw image to 0 - 1000 range (as input images already have)
    c3d $N4 -stretch 0.1% 99.9% 0 1000 -clip 0 1000 -o $CLIP


    # If the image dimensions are different, continue to next
    if [[ "$INPUT_DIM" == "$RAW_DIM" ]]; then

      # Line up the headers of the two images and check for overlap
      METRIC=$(c3d $INPUT_IMG -dup $CLIP -copy-transform -ncc 4x4x4 \
        -replace -nan 0 nan 0 -inf 0 inf 0 \
        -dup -thresh -inf inf 1 0 -lstat \
        | awk 'NR==2 {print $2}')

      # Create a matrix from headers
      c3d_affine_tool -sform $CLIP -sform $INPUT_IMG -inv -mult -o $MRIGID 

    else

      # Dimensions are different, so we have to perform registration and hope it works
      MMOMENTS=$WORK/misc/moments_${rawbase}.mat
      RESLICE=$WORK/misc/reslice_${rawbase}.nii.gz

      # Perform moments of intertia matching
      greedy -d 3 -o $MMOMENTS \
        -i $INPUT_IMG $CLIP -moments -det 1 \
        | tee $WORK/misc/momdump_${rawimg}.txt 

      # Perform rigid registration
      greedy -d 3 -o $MRIGID \
        -i $INPUT_IMG $CLIP -a -dof 6 -n 100x100x40x0 \
        -ia $MMOMENTS -m NCC 2x2x2 \
        | tee $WORK/misc/affdump_${rawimg}.txt

      # Apply rigid registration (reslice) 
      greedy -d 3 -rf $INPUT_IMG -rm $CLIP $RESLICE -r $MRIGID
        
      # Compute the average NCC metric 
      METRIC=$(c3d $INPUT_IMG $RESLICE -ncc 4x4x4 \
        -replace -nan 0 nan 0 -inf 0 inf 0 \
        -dup -thresh -inf inf 1 0 -lstat \
        | awk 'NR==2 {print $2}')

    fi

    # Write the image and metric to the file
    echo "$rawbase $METRIC" >> $MLIST

  done

SKIP

  # Take the best matching image as the raw image
  bestraw=$(cat $MLIST | sort -k 2 -g | tail -n 1 | awk '{print $1}')

  # Produce a link to the raw data
  ln -sf $RAWDIR/${bestraw}.nii $WORK/${id}_raw.nii.gz

  # Copy the processed (N4'd) raw data
  cp $WORK/misc/clip_${bestraw}.nii.gz $WORK/${id}_raw_n4clip.nii.gz

  # Produce a link to the input data - i.e. the data segmented by Dan
  ln -sf $INPUT_IMG $WORK/${id}_danseg_img.nii.gz
  ln -sf $ROOT/input/${id}/${id}_hfseg.nii.gz $WORK/${id}_danseg_hfseg.nii.gz
  ln -sf $ROOT/input/${id}/${id}_dbseg.nii.gz $WORK/${id}_danseg_dbseg.nii.gz

  # Store the matrix from the input image to the raw image
  cp $WORK/misc/rigid_${bestraw}.mat $WORK/${id}_raw_to_danseg.mat
  c3d_affine_tool $WORK/${id}_raw_to_danseg.mat -inv -o $WORK/${id}_danseg_to_raw.mat

  # Determine which gradwarp factors should be applied to the data
  # - based on 11/24/16 cutoff date
  OLD_SCAN=$(echo $bestraw | awk -F '[_ ]' '{print ($5 < 20161124)}')

  if [[ $OLD_SCAN -eq 1 ]]; then
    UNWARP=/data/picsl/lwisse/IND/phantom_unwarp/phantom_unwarp_affine.mat 
  else
    UNWARP=/data/picsl/lwisse/IND/phantom_unwarp_new/phantom_unwarp_affine_newparam.mat
  fi

  # Create the unwarp dir
  mkdir -p $WORK/unwarp

  cp -av $UNWARP $WORK/unwarp/${id}_unwarp.mat

  # Transform everything into gradwarp-corrected space
  greedy -d 3 \
    -rf $WORK/${id}_raw_n4clip.nii.gz \
    -rm $WORK/${id}_raw_n4clip.nii.gz $WORK/unwarp/${id}_unwarp_img.nii.gz \
    -r $WORK/unwarp/${id}_unwarp.mat 

  greedy -d 3 \
    -rf $WORK/unwarp/${id}_unwarp_img.nii.gz \
    -ri LABEL 0.24vox \
    -rm $WORK/${id}_danseg_hfseg.nii.gz $WORK/unwarp/${id}_unwarp_hfseg.nii.gz \
    -rm $WORK/${id}_danseg_dbseg.nii.gz $WORK/unwarp/${id}_unwarp_dbseg.nii.gz \
    -r $WORK/unwarp/${id}_unwarp.mat $WORK/${id}_danseg_to_raw.mat
  
}

function main()
{
  mkdir -p $WDIR/dump
  for id in $(cat $ROOT/scripts/id_to_raw.txt | awk '{print $1}'); do
    qsub -o $WDIR/dump -j y -cwd -V -N "prep_$id" \
      $0 organize_subj $id
  done
}

if [[ $1 ]]; then

  command=$1
  shift
  $command $@

else

  main

fi

